﻿// AUTHOR: Benjamin Chaves

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SaveFile
{
    public class SaveFileDataLoadWrapper
    {
        /// <summary>
        /// The possible results from attempting to load the data from disk.
        /// </summary>
        public enum LoadResults
        {
            Successful,
            FileIncomplete,
            FileNotFound,
        }

        public LoadResults LoadResult { get; private set; }
        public SaveFileData SaveFileData { get; private set; }

        public SaveFileDataLoadWrapper(LoadResults inputLoadResult, SaveFileData inputSaveFileData)
        {
            LoadResult = inputLoadResult;

            SaveFileData = inputSaveFileData;
        }
    }
}