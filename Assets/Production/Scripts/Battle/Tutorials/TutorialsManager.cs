﻿// AUTHOR: Benjamin Chaves

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Battle
{
    public class TutorialsManager : MonoBehaviour
    {
        private AppManager appManager;

        [Header("Override Items")]

        public GameObject TutorialCursorParent;

        [Header("Prefab Items")]

        public GameObject TutorialCursorPrefab;

        [Tooltip("Defines the seconds for which the tutorial cursor's position is held.")]
        public float CursorHoldSeconds;

        [Tooltip("Defines the speed with which the tutorial cursor moves position.")]
        public float CursorMovementSpeed;

        [Tooltip("Defines the seconds for which the tutorial prompt will wait before highlighting itself.")]
        public int TutorialsPromptTimerSeconds;
        private Coroutine tutorialsPromptTimerCoroutine;
        private WaitForSeconds waitForPromptTimerSeconds;

        [Tooltip("The GameObject which holds the Drain Enemy Tutorial as a component.")]
        public GameObject GO_DrainEnemyTutorial;

        [Tooltip("The GameObject which holds the Strike Enemy Tutorial as a component.")]
        public GameObject GO_StrikeEnemyTurorial;

        [Tooltip("The GameObject which holds the Card Selection Tutorial as a component.")]
        public GameObject GO_CardSelectionTutorial;

        [Tooltip("The GameObject which holds the Heal Character Tutorial as a component.")]
        public GameObject GO_HealCharacterTutorial;

        [Tooltip("The GameObject which holds the Tutorials Prompt Manager as a component.")]
        public GameObject GO_TutorialsPromptManager;

        private DrainEnemyTutorial drainEnemyTutorial;
        private StrikeEnemyTutorial strikeEnemyTutorial;
        private CardSelectionTutorial cardSelectionTutorial;
        private HealCharacterTutorial healCharacterTutorial;
        private UI_TutorialsPromptManager tutorialsPromptManager;

        /// <summary>
        /// Whether the user has learned how to use the Drain action.
        /// </summary>
        private bool drainEnemyLearned;

        /// <summary>
        /// Whether the user has learned how to use the Strike action.
        /// </summary>
        private bool strikeEnemyLearned;

        /// <summary>
        /// Whether the user learned how to use the Heal action.
        /// </summary>
        private bool healCharacterLearned;

        /// <summary>
        /// The tutorials which are available.
        /// </summary>
        public enum Tutorials
        {
            CardSelection,
            StrikeEnemy,
            HealCharacter,
            DrainEnemy,
        }

        public void Initialise(AppManager inputAppManager)
        {
            Debug.Assert(inputAppManager != null);
            appManager = inputAppManager;

            AssertInspectorInputs();

            CacheSubordinates();
            InitialiseSubordinates();

            waitForPromptTimerSeconds = new WaitForSeconds(TutorialsPromptTimerSeconds);
        }

        /// <summary>
        /// Assert that all Inspector fields have been set or filled.
        /// </summary>
        private void AssertInspectorInputs()
        {
            Debug.Assert(TutorialCursorParent != null);
            Debug.Assert(TutorialCursorPrefab != null);

            Debug.Assert(CursorHoldSeconds > 0f);
            Debug.Assert(CursorMovementSpeed > 0f);

            Debug.Assert(GO_TutorialsPromptManager != null);
            Debug.Assert(TutorialsPromptTimerSeconds > 0);

            Debug.Assert(GO_DrainEnemyTutorial != null);
            Debug.Assert(GO_StrikeEnemyTurorial != null);
            Debug.Assert(GO_CardSelectionTutorial != null);
            Debug.Assert(GO_HealCharacterTutorial != null);
        }

        private void CacheSubordinates()
        {
            Debug.Assert(GO_DrainEnemyTutorial != null);
            Debug.Assert(GO_StrikeEnemyTurorial != null);
            Debug.Assert(GO_CardSelectionTutorial != null);
            Debug.Assert(GO_HealCharacterTutorial != null);
            Debug.Assert(GO_TutorialsPromptManager != null);

            drainEnemyTutorial = GO_DrainEnemyTutorial.GetComponent<DrainEnemyTutorial>();
            Debug.Assert(drainEnemyTutorial != null); // Assert that the component was found.

            strikeEnemyTutorial = GO_StrikeEnemyTurorial.GetComponent<StrikeEnemyTutorial>();
            Debug.Assert(strikeEnemyTutorial != null); // Assert that the component was found.

            cardSelectionTutorial = GO_CardSelectionTutorial.GetComponent<CardSelectionTutorial>();
            Debug.Assert(cardSelectionTutorial != null); // Assert that the component was found.

            healCharacterTutorial = GO_HealCharacterTutorial.GetComponent<HealCharacterTutorial>();
            Debug.Assert(healCharacterTutorial != null); // Assert that the component was found.

            tutorialsPromptManager = GO_TutorialsPromptManager.GetComponent<UI_TutorialsPromptManager>();
            Debug.Assert(tutorialsPromptManager != null); // Assert that the component was found.
        }

        private void InitialiseSubordinates()
        {
            Debug.Assert(tutorialsPromptManager != null);
            Debug.Assert(drainEnemyTutorial != null);
            Debug.Assert(strikeEnemyTutorial != null);
            Debug.Assert(healCharacterTutorial != null);
            Debug.Assert(cardSelectionTutorial != null);

            tutorialsPromptManager.Initialise(this);

            drainEnemyTutorial.Initialise(this, TutorialCursorPrefab, TutorialCursorParent);

            strikeEnemyTutorial.Initialise(this, TutorialCursorPrefab, TutorialCursorParent);

            healCharacterTutorial.Initialise(this, TutorialCursorPrefab, TutorialCursorParent);

            cardSelectionTutorial.Initialise(this, TutorialCursorPrefab, TutorialCursorParent, appManager.UIManager);
        }

        /// <summary>
        /// Notifies the manager that the battle has started.
        /// </summary>
        public void NotifyBattleStarted()
        {
            Debug.Assert(tutorialsPromptManager != null);
            Debug.Assert(appManager != null);
            Debug.Assert(appManager.BattleManager != null);

            tutorialsPromptManager.SetMenuActive(true);

            if (appManager.BattleManager.GetBattleIndex() == 1) // The current battle is the first battle.
            {
                StartTutorial(Tutorials.CardSelection);
            }
            else
            {
                SetAllTutorialsLearned();
            }
        }

        /// <summary>
        /// Activates the appropriate tutorial, depending on the player's current context.
        /// </summary>
        public void ActivateContextualTutorial()
        {
            Debug.Assert(appManager != null);
            Debug.Assert(appManager.MouseActionsManager != null);
            Debug.Assert(appManager.MouseActionsManager.ActiveSymbol != MouseActionsManager.Symbols.None);

            EndAllTutorials();

            switch(appManager.MouseActionsManager.ActiveSymbol)
            {
                case MouseActionsManager.Symbols.Sword:

                    StartTutorial(Tutorials.StrikeEnemy); // Start the Strike Enemy tutorial.
                    break;

                case MouseActionsManager.Symbols.Potion:

                    StartTutorial(Tutorials.HealCharacter); // Start the Heal Character tutorial.
                    break;

                case MouseActionsManager.Symbols.Drain:

                    StartTutorial(Tutorials.DrainEnemy); // Start the Drain Enemy tutorial.
                    break;

                case MouseActionsManager.Symbols.None:

                    StartTutorial(Tutorials.CardSelection); // Start the Card Selection tutorial.
                    break;

                default:

                    Debug.LogError(appManager.MouseActionsManager.ActiveSymbol);
                    break;
            }
        }

        public void SetAllTutorialsLearned()
        {
            drainEnemyLearned = true;
            strikeEnemyLearned = true;
        }

        /// <summary>
        /// Notifies the manager that the player's turn has started.
        /// </summary>
        public void NotifyPlayerTurnStarted()
        {
            Debug.Assert(appManager != null);
            Debug.Assert(appManager.BattleManager != null);

            if(!GetAnyTutorialActive() && appManager.BattleManager.GetBattleIndex() == 1) // No tutorial is active, and the current battle is the first battle.
            {
                RestartTutorialsPromptTimer();
            }
        }

        /// <summary>
        /// Notifies the manager that the player has selected
        /// to play a card with the given mouse action.
        /// </summary>
        /// <param name="selectedCardMouseAction">The mouse action which the player will use.</param>
        public void NotifyHandCardSelected(MouseActionsManager.Symbols selectedCardMouseAction)
        {
            Debug.Assert(appManager != null);
            Debug.Assert(appManager.BattleManager != null);
            Debug.Assert(tutorialsPromptManager != null);

            EndAllTutorials();

            if(appManager.BattleManager.GetBattleIndex() == 1) // The current battle is the first battle.
            {
                tutorialsPromptManager.SetPromptHighlighted(false);
                RestartTutorialsPromptTimer();
            }

            switch (selectedCardMouseAction) // Select the mouse-action the user will be using.
            {
                case MouseActionsManager.Symbols.Sword:

                    if(!strikeEnemyLearned) // The user has not learned how to strike the enemy.
                    {
                        StartTutorial(Tutorials.StrikeEnemy);
                    }
                    break;

                case MouseActionsManager.Symbols.Potion:

                    if(!healCharacterLearned) // The user has not learned how to heal the character.
                    {
                        StartTutorial(Tutorials.HealCharacter);
                    }
                    break;

                case MouseActionsManager.Symbols.Drain:

                    if(!drainEnemyLearned) // The user has not learned how to drain the enemy.
                    {
                        StartTutorial(Tutorials.DrainEnemy);
                    }
                    break;

                default:

                    Debug.LogError(selectedCardMouseAction);
                    break;
            }
        }

        /// <summary>
        /// Notifies the manager that the player has initiated a mouse action.
        /// </summary>
        public void NotifyMouseActionInitiated()
        {
            Debug.Assert(tutorialsPromptManager != null);

            tutorialsPromptManager.SetPromptHighlighted(false);

            EndAllTutorials();
        }

        /// <summary>
        /// Notifies the manager that the player has completed the
        /// given mouse action, with the given success result.
        /// </summary>
        /// <param name="actionInitiated">Which action was initiated.</param>
        /// <param name="actionSuccessful">Whether the player completed the action successfully.</param>
        public void NotifyActionResolved(MouseActionsManager.Symbols actionInitiated, bool actionSuccessful)
        {
            EndAllTutorials();

            if (actionSuccessful) // The player's action was successful.
            {
                switch (actionInitiated)
                {
                    case MouseActionsManager.Symbols.Sword:

                        strikeEnemyLearned = true; // Mark the Strike Enemy action as learned by the user.
                        break;

                    case MouseActionsManager.Symbols.Potion:

                        healCharacterLearned = true; // Mark the Heal Character action as learned by the user.
                        break;

                    case MouseActionsManager.Symbols.Drain:

                        drainEnemyLearned = true; // Mark the Drain Enemy action as learned by the user.
                        break;

                    default:

                        Debug.LogError(actionInitiated);
                        break;
                }
            }
        }

        /// <summary>
        /// Starts the given tutorial.
        /// </summary>
        /// <param name="tutorialToStart">The tutorial which should be started.</param>
        private void StartTutorial(Tutorials tutorialToStart)
        {
            Debug.Assert(cardSelectionTutorial != null);
            Debug.Assert(strikeEnemyTutorial != null);
            Debug.Assert(healCharacterTutorial != null);
            Debug.Assert(drainEnemyTutorial != null);

            switch (tutorialToStart)
            {
                case Tutorials.CardSelection:

                    cardSelectionTutorial.StartTutorial();
                    break;

                case Tutorials.StrikeEnemy:

                    strikeEnemyTutorial.StartTutorial();
                    break;

                case Tutorials.HealCharacter:

                    healCharacterTutorial.StartTutorial();
                    break;

                case Tutorials.DrainEnemy:

                    drainEnemyTutorial.StartTutorial();
                    break;

                default:

                    Debug.LogError(tutorialToStart);
                    break;
            }
        }

        /// <summary>
        /// Returns whether at least 1 tutorial is currently active.
        /// </summary>
        private bool GetAnyTutorialActive()
        {
            Debug.Assert(cardSelectionTutorial != null);
            Debug.Assert(strikeEnemyTutorial != null);
            Debug.Assert(healCharacterTutorial != null);
            Debug.Assert(drainEnemyTutorial != null);

            return cardSelectionTutorial.TutorialActive || strikeEnemyTutorial.TutorialActive || healCharacterTutorial.TutorialActive || drainEnemyTutorial.TutorialActive;
        }

        /// <summary>
        /// Ends any tutorials which may be active.
        /// </summary>
        public void EndAllTutorials()
        {
            Debug.Assert(cardSelectionTutorial != null);
            Debug.Assert(strikeEnemyTutorial != null);
            Debug.Assert(healCharacterTutorial != null);
            Debug.Assert(drainEnemyTutorial != null);

            if (cardSelectionTutorial.TutorialActive) // The Card Selection tutorial is active.
            {
                cardSelectionTutorial.EndTutorial();
            }

            if(strikeEnemyTutorial.TutorialActive) // The Strike Enemy tutorial is active.
            {
                strikeEnemyTutorial.EndTutorial();
            }

            if(healCharacterTutorial.TutorialActive) // The Heal Character tutorial is active.
            {
                healCharacterTutorial.EndTutorial();
            }

            if(drainEnemyTutorial.TutorialActive) // The Drain Enemy tutorial is active.
            {
                drainEnemyTutorial.EndTutorial();
            }
        }

        /// <summary>
        /// Returns whether each corresponding float component of each given vector are approximately equal.
        /// </summary>
        public static bool VectorsApproximatelyEqual(Vector2 a, Vector2 b)
        {
            return Mathf.Approximately(a.x, b.x) && Mathf.Approximately(a.y, b.y);
        }

        /// <summary>
        /// Stops the tutorials prompt timer coroutine if it is active,
        /// and then starts the coroutine.
        /// </summary>
        private void RestartTutorialsPromptTimer()
        {
            StopTutorialsPromptCoroutine();

            tutorialsPromptTimerCoroutine = StartCoroutine(TutorialsPromptTimer());
        }

        /// <summary>
        /// Checks if the tutorials prompt timer coroutine is active,
        /// and if it is, stops the coroutine.
        /// </summary>
        public void StopTutorialsPromptCoroutine()
        {
            if(tutorialsPromptTimerCoroutine != null)
            {
                StopCoroutine(tutorialsPromptTimerCoroutine);

                tutorialsPromptTimerCoroutine = null;
            }
        }

        /// <summary>
        /// Waits for the set number of seconds, and then sets the tutorial prompt as highlighted.
        /// </summary>
        /// <returns></returns>
        private IEnumerator TutorialsPromptTimer()
        {
            Debug.Assert(tutorialsPromptManager != null);
            Debug.Assert(tutorialsPromptTimerCoroutine == null); // Assert that the coroutine is not already running.

            yield return waitForPromptTimerSeconds;

            tutorialsPromptManager.SetPromptHighlighted(true);
        }

        /// <summary>
        /// Returns whether the player's cursor is currently over the tutorial prompt button.
        /// </summary>
        public bool MouseIsOverTutorialPrompt()
        {
            Debug.Assert(tutorialsPromptManager != null);

            return tutorialsPromptManager.MouseIsOver;
        }
    }
}