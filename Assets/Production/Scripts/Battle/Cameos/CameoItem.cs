﻿// AUTHOR: Benjamin Chaves

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class CameoItem
{
    [Tooltip("The character speaking this cameo.")]
    public CameosManager.Characters Character;
    
    [Tooltip("The speech being spoken in this cameo.")]
    public string SpeechText;
}