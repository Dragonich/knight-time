﻿// AUTHOR: Benjamin Chaves

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundEffectsManager : MonoBehaviour
{
    [Header("Prefab Items")]

    [Tooltip("The GameObject which contains Menu Sound Effects as a component.")]
    public GameObject GO_MenuSoundEffects;

    [Tooltip("The GameObject which contains Cards Sound Effects as a component.")]
    public GameObject GO_CardsSoundEffects;

    [Tooltip("The GameObject which contains Characters Sound Effects as a component.")]
    public GameObject GO_CharacterSoundEffects;

    public MenuSoundEffects MenuSoundEffects { get; private set; }
    public CardsSoundEffects CardsSoundEffects { get; private set; }
    public CharacterSoundEffects CharacterSoundEffects { get; private set; }

    public void Initialise()
    {
        CacheSubordinates();

        InitialiseSubordinates();
    }

    private void CacheSubordinates()
    {
        Debug.Assert(GO_MenuSoundEffects != null);
        Debug.Assert(GO_CardsSoundEffects != null);
        Debug.Assert(GO_CharacterSoundEffects != null);

        MenuSoundEffects = GO_MenuSoundEffects.GetComponent<MenuSoundEffects>();
        Debug.Assert(MenuSoundEffects != null); // Assert that the component was found.

        CardsSoundEffects = GO_CardsSoundEffects.GetComponent<CardsSoundEffects>();
        Debug.Assert(CardsSoundEffects != null); // Assert that the component was found.

        CharacterSoundEffects = GO_CharacterSoundEffects.GetComponent<CharacterSoundEffects>();
        Debug.Assert(CharacterSoundEffects != null); // Assert that the component was found.
    }

    private void InitialiseSubordinates()
    {
        Debug.Assert(MenuSoundEffects != null);
        Debug.Assert(CardsSoundEffects != null);
        Debug.Assert(CharacterSoundEffects != null);

        MenuSoundEffects.Initialise();
        CardsSoundEffects.Initialise();
        CharacterSoundEffects.Initialise();
    }
}