﻿// Author: Luke Saliba
using SaveFile;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Menu
{
    public class MainMenu : MonoBehaviour
    {
        public GameObject openMenu;
        public GameObject SaveFileManager;
        public GameObject AreYouSurePanel;
        static public bool menuOpen;
        public float Delay;
        public string BattleSceneName;

        void Start()
        {
            openMenu.SetActive(true);
            AreYouSurePanel.SetActive(false);
        }

        // Update is called once per frame
        void Update()
        {

        }
        public void Level1()
        {

            if (SaveFileManager.GetComponent<SaveFileManager>().SaveFileExists() == true)
            {
                
                StartCoroutine(ButtonDelay());
            }
            
        
        }
        private IEnumerator ButtonDelay()
        {
            yield return new WaitForSeconds(Delay);
            SceneManager.LoadScene(BattleSceneName);
            openMenu.SetActive(false);
        }
        public void CloseMenu()
        {
            openMenu.SetActive(false);
        }
        public void QuitGame()
        {
            Application.Quit();
        }
        public void OpenMenu()
        {
            openMenu.SetActive(true);
        }
        public void AreYouSure()
        {
            if (SaveFileManager.GetComponent<SaveFileManager>().SaveFileExists() == true)
            {
                AreYouSurePanel.SetActive(true);
            }
            else
                NewGame();
        }
        public void NoImNotSure()
        {
            AreYouSurePanel.SetActive(false);
        }
        public void NewGame()
        {

            SaveFileManager.GetComponent<SaveFileManager>().DeleteSaveFile();
            SceneManager.LoadScene(BattleSceneName);


        }
    }
}
