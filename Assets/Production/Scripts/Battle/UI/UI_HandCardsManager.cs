﻿// AUTHOR: Benjamin Chaves

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Pause;

namespace Battle
{
    public class UI_HandCardsManager : MonoBehaviour
    {
        private CardsManager cardsManager;
        private BattleManager battleManager;
        private TutorialsManager tutorialsManager;
        private CharactersManager charactersManager;
        private CanvasConversionManager canvasConversionManager;

        [Header("Override Items")]

        public GameObject HandCardsParent;
        
        [Header("Prefab Items")]
        
        public GameObject HandCardSetTemplate;

        /// <summary>
        /// Array of the card graphics objects.
        /// </summary>
        private GameObject[] HandCardSets;

        [Tooltip("The horizontal distance between each card graphic.")]
        public float HandCardSeparation;

        public void Initialise(CardsManager inputCardsManager, BattleManager inputBattleManager, TutorialsManager inputTutorialsManager, CharactersManager inputCharactersManager, CanvasConversionManager inputCanvasConversionManager)
        {
            Debug.Assert(inputCardsManager != null);
            Debug.Assert(inputBattleManager != null);
            Debug.Assert(inputTutorialsManager != null);
            Debug.Assert(inputCharactersManager != null);
            Debug.Assert(inputCanvasConversionManager != null);

            AssertInspectorInputs();

            cardsManager = inputCardsManager;
            battleManager = inputBattleManager;
            tutorialsManager = inputTutorialsManager;
            charactersManager = inputCharactersManager;
            canvasConversionManager = inputCanvasConversionManager;

            int handCardSetsQuantity = cardsManager.GetCardsQuantity();
            HandCardSets = new GameObject[handCardSetsQuantity];

            InstantiateHandCards();

            UpdateHandCardsInformation();
        }

        /// <summary>
        /// Assert that all Inspector fields have been set or filled.
        /// </summary>
        private void AssertInspectorInputs()
        {
            Debug.Assert(HandCardsParent != null);

            Debug.Assert(HandCardSetTemplate != null);

            Debug.Assert(HandCardSeparation > 0f);
        }

        /// <summary>
        /// Instantiates the card graphics for the player's hand.
        /// </summary>
        private void InstantiateHandCards()
        {
            Debug.Assert(cardsManager != null);
            Debug.Assert(HandCardsParent != null);
            Debug.Assert(charactersManager != null);
            Debug.Assert(HandCardSetTemplate != null);
            Debug.Assert(canvasConversionManager != null);

            HandCardSetTemplate.SetActive(true);

            Vector2 newCardPositionSetter = canvasConversionManager.GetMultipliedCanvasPosition(-0.17f, 0f);
            newCardPositionSetter.y = -450f;

            for (int handSetIndex = 0; handSetIndex < HandCardSets.Length; handSetIndex++) // Iterate through each hand card.
            {
                if (charactersManager.GetPlayerHasCard(handSetIndex)) // The player has the current card.
                {
                    Card currentCard = cardsManager.GetDatabaseCardByID(handSetIndex);

                    GameObject newHandCardSet = Instantiate(HandCardSetTemplate);
                    HandCardSets[handSetIndex] = newHandCardSet;

                    newHandCardSet.name = currentCard.CardName + " Hand Set";
                    newHandCardSet.transform.SetParent(HandCardsParent.transform);

                    RectTransform currentCardRectTrans = newHandCardSet.GetComponent<RectTransform>();
                    Debug.Assert(currentCardRectTrans != null); // Assert that the component was found.

                    currentCardRectTrans.anchoredPosition = newCardPositionSetter;
                    newCardPositionSetter.x += HandCardSeparation;

                    UI_HandCardAttributesManager currentCardAttributesManager = newHandCardSet.GetComponent<UI_HandCardAttributesManager>();
                    Debug.Assert(currentCardAttributesManager != null); // Assert that the component was found.

                    currentCardAttributesManager.Initialise(currentCard, this);
                }
            }

            HandCardSetTemplate.SetActive(false);
        }

        private void Update()
        {
            Debug.Assert(battleManager != null);

            if(battleManager.GetAcceptingPlayerCardSelection() && !PauseMenu.isPaused) // This turn is the player's component, and the pause menu is not active.
            {
                ApplyPlayerInput();
            }
        }

        /// <summary>
        /// Checks for and applies any card keyboard number selection.
        /// </summary>
        private void ApplyPlayerInput()
        {
            if (Input.GetKeyDown(KeyCode.Alpha1))
            {
                SelectSetByNumber(1);
            }
            else if (Input.GetKeyDown(KeyCode.Alpha2))
            {
                SelectSetByNumber(2);
            }
            else if (Input.GetKeyDown(KeyCode.Alpha3))
            {
                SelectSetByNumber(3);
            }
            else if (Input.GetKeyDown(KeyCode.Alpha4))
            {
                SelectSetByNumber(4);
            }
            else if (Input.GetKeyDown(KeyCode.Alpha5))
            {
                SelectSetByNumber(5);
            }
            else if (Input.GetKeyDown(KeyCode.Alpha6))
            {
                SelectSetByNumber(6);
            }
        }

        /// <summary>
        /// Selects the given card set by keyboard number.
        /// </summary>
        /// <param name="numberToSelect">The keyboard number pressed by the user.</param>
        private void SelectSetByNumber(int numberToSelect)
        {
            Debug.Assert(numberToSelect >= 1, numberToSelect);
            Debug.Assert(numberToSelect <= 9, numberToSelect);

            int selectionCounter = 0;

            for (int index = 0; index < HandCardSets.Length; index++) // Iterate through each hand card.
            {
                GameObject currentHandCard = HandCardSets[index];

                if (currentHandCard != null) // The current hand card is valid.
                {
                    selectionCounter++;

                    if(selectionCounter == numberToSelect) // The player is selecting the current hand card.
                    {
                        UI_HandCardAttributesManager cardManagerToSelect = currentHandCard.GetComponent<UI_HandCardAttributesManager>();
                        Debug.Assert(cardManagerToSelect != null); // Assert that the component was found.

                        SelectCard(cardManagerToSelect);

                        break;
                    }
                }
            }
        }

        /// <summary>
        /// Updates the visual information on each hand card.
        /// </summary>
        public void UpdateHandCardsInformation()
        {
            Debug.Assert(cardsManager != null);
            Debug.Assert(charactersManager != null);

            for (int handSetIndex = 0; handSetIndex < HandCardSets.Length; handSetIndex++) // Iterate through each card set.
            {
                if(charactersManager.GetPlayerHasCard(handSetIndex)) // The player has the current card.
                {
                    GameObject currentCardObject = HandCardSets[handSetIndex];
                    Debug.Assert(currentCardObject != null);

                    UI_HandCardAttributesManager currentCardAttributesManager = currentCardObject.GetComponent<UI_HandCardAttributesManager>();
                    Debug.Assert(currentCardAttributesManager != null); // Assert that the component was found.

                    Card currentCard = cardsManager.GetDatabaseCardByID(currentCardAttributesManager.HandCard.CardID); // Caches the current card from its Card ID.
                    Debug.Assert(currentCard != null);

                    if (currentCard.IsSkillCard) // The current card is a skill card.
                    {
                        int cooldownsRemaining = charactersManager.GetPlayerCardCooldowns(handSetIndex);
                        currentCardAttributesManager.SetCardCooldowns(cooldownsRemaining);
                    }
                    else // The current card is not a skill card.
                    {
                        int cardQuantity = charactersManager.GetPlayerCardsQuantity(currentCard.CardID);
                        currentCardAttributesManager.SetCardQuantity(cardQuantity);
                    }
                }
            }
        }

        /// <summary>
        /// Triggers a card selection from the given card manager.
        /// </summary>
        /// <param name="cardManagerToSelect">The card which should be selected.</param>
        public void ButtonClickedHandCard(UI_HandCardAttributesManager cardManagerToSelect)
        {
            Debug.Assert(cardManagerToSelect != null);

            SelectCard(cardManagerToSelect);
        }

        /// <summary>
        /// Selects a card from its given manager.
        /// </summary>
        /// <param name="cardManagerToSelect">The card which should be selected.</param>
        private void SelectCard(UI_HandCardAttributesManager cardManagerToSelect)
        {
            Debug.Assert(cardManagerToSelect != null);

            Debug.Assert(cardsManager != null);
            Debug.Assert(battleManager != null);
            Debug.Assert(tutorialsManager != null);
            Debug.Assert(charactersManager != null);

            int cardToSelect = cardManagerToSelect.HandCard.CardID;

            if (charactersManager.GetPlayerHasCardAvailable(cardToSelect)) // The player has the given card available.
            {
                Card cardToTable = cardsManager.GetDatabaseCardByID(cardToSelect);
                Debug.Assert(cardToTable != null);

                if(cardToTable != cardsManager.GetPlayerTabledCard()) // The player has not already tabled the given card.
                {
                    tutorialsManager.NotifyHandCardSelected(cardToTable.MouseSymbol);

                    battleManager.PlayPlayerCard(cardToTable);
                }
            }
        }

        /// <summary>
        /// Returns whether the user's mouse is over at least 1 card.
        /// </summary>
        public bool PointerOverAnyCard()
        {
            for (int index = 0; index < HandCardSets.Length; index++) // Iterate through each card.
            {
                GameObject currentCardSet = HandCardSets[index];

                if (currentCardSet != null) // The user has the current card.
                {
                    UI_HandCardAttributesManager currentCardAttributesManager = currentCardSet.GetComponent<UI_HandCardAttributesManager>();
                    Debug.Assert(currentCardAttributesManager != null); // Assert that the component was found.

                    if (currentCardAttributesManager.MouseIsOver) // The user's mouse is over the current card.
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        /// <summary>
        /// Returns the Canvas position of the given hand card.
        /// </summary>
        /// <param name="handSetIndex">The card whose Canvas position should be returned.</param>
        public Vector2 GetHandCardPosition(int handSetIndex)
        {
            Debug.Assert(handSetIndex >= 0);
            Debug.Assert(handSetIndex < HandCardSets.Length);

            GameObject handCardSet = HandCardSets[handSetIndex];
            Debug.Assert(handCardSet != null);

            RectTransform handCardSetRectTrans = handCardSet.GetComponent<RectTransform>();
            Debug.Assert(handCardSetRectTrans != null); // Assert that the component was found.

            return handCardSetRectTrans.anchoredPosition;
        }

        /// <summary>
        /// Sets whether this UI is active, depending on the given boolean.
        /// </summary>
        public void SetHandCardsActive(bool newIsActive)
        {
            Debug.Assert(HandCardsParent != null);

            HandCardsParent.SetActive(newIsActive);
        }
    }
}