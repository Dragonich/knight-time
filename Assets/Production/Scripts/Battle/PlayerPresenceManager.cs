﻿// AUTHOR: Benjamin Chaves

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Pause;

public class PlayerPresenceManager : MonoBehaviour
{
    [Header("Prefab Items")]

    public int PlayerAbsentSeconds;

    [Header("Override Items")]

    public GameObject GO_PauseMenu;
    private PauseMenu pauseMenuManager;

    private Vector3 mouseCachedPosition;

    private Coroutine playerAbsentTimerCoroutine;
    private WaitForSeconds waitForPlayerAbsentSeconds;

    private void Start()
    {
        AssertInspectorInputs();

        CacheSubordinates();

        waitForPlayerAbsentSeconds = new WaitForSeconds(PlayerAbsentSeconds);
    }

    /// <summary>
    /// Assert that all Inspector fields have been set or filled.
    /// </summary>
    private void AssertInspectorInputs()
    {
        Debug.Assert(PlayerAbsentSeconds > 0);

        Debug.Assert(GO_PauseMenu != null);
    }

    private void CacheSubordinates()
    {
        Debug.Assert(GO_PauseMenu != null);

        pauseMenuManager = GO_PauseMenu.GetComponent<PauseMenu>();
        Debug.Assert(pauseMenuManager != null); // Assert that the component was successfuly found.
    }

    private void Update()
    {
        CheckUserInput();
    }

    /// <summary>
    /// Checks for any user input, and stops or resets the timer based on the result.
    /// </summary>
    private void CheckUserInput()
    {
        if (GetMouseMoved() || GetMouseClicked() || GetKeyboardActive()) // The user has applied input this cycle.
        {
            if (playerAbsentTimerCoroutine != null) // The absence timer is currenty active.
            {
                StopCoroutine(playerAbsentTimerCoroutine); // Stop the absence timer.

                playerAbsentTimerCoroutine = null;
            }
        }
        else // The user has not applied input this cycle.
        {
            if (playerAbsentTimerCoroutine == null) // The absence timer is not currently active.
            {
                ResetAbsenceTimer();
            }
        }
    }

    /// <summary>
    /// Returns whether the mouse position has moved since the last check.
    /// </summary>
    private bool GetMouseMoved()
    {
        bool mouseMoved = !VectorsApproximatelyEqual(mouseCachedPosition, Input.mousePosition);

        mouseCachedPosition = Input.mousePosition; // Update the cached position for a future check.

        return mouseMoved;
    }

    /// <summary>
    /// Returns whether the user has currently pressed either left-click or right-click.
    /// </summary>
    private bool GetMouseClicked()
    {
        return Input.GetMouseButton(0) || Input.GetMouseButton(1);
    }

    /// <summary>
    /// Returns whether any key has been pressed, or is held down.
    /// </summary>
    private bool GetKeyboardActive()
    {
        return Input.anyKey || Input.anyKeyDown;
    }

    /// <summary>
    /// Returns whether each corresponding float component of each given vector are approximately equal.
    /// </summary>
    private bool VectorsApproximatelyEqual(Vector3 vectorA, Vector3 vectorB)
    {
        return Mathf.Approximately(vectorA.x, vectorB.x) && Mathf.Approximately(vectorA.y, vectorB.y) && Mathf.Approximately(vectorA.z, vectorB.z);
    }

    private IEnumerator PlayerAbsentTimer()
    {
        yield return waitForPlayerAbsentSeconds;

        playerAbsentTimerCoroutine = null;
        pauseMenuManager.PauseGame(false);
    }

    /// <summary>
    /// Restarts the player absence timer coroutine.
    /// </summary>
    public void ResetAbsenceTimer()
    {
        if(playerAbsentTimerCoroutine != null) // The absence timer is currently active.
        {
            StopCoroutine(playerAbsentTimerCoroutine); // Stop the absence timer.

            playerAbsentTimerCoroutine = null;
        }
        
        playerAbsentTimerCoroutine = StartCoroutine(PlayerAbsentTimer()); // Start the absence timer, and cache its coroutine.
    }
}