﻿// AUTHOR: Benjamin Chaves

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using SaveFile;

namespace Battle
{
    public class BattleManager : MonoBehaviour
    {
        private AppManager appManager;

        [Header("Prefab Items")]

        [Tooltip("Defines whether the details of each turn should be written to the debug log.")]
        public bool WriteTurnDetailsToLog;

        [Tooltip("The seconds between the player concluding the battle, and the battle debrief being shown.")]
        public float BattleEndDelaySeconds;
        private Coroutine battleEndDelayCoroutine;
        private WaitForSeconds waitForBattleEndDelaySeconds;

        [Tooltip("The name of the map Unity Scene.")]
        public string MapSceneName;

        [Tooltip("The GameObject which holds the Turn Manager as a component.")]
        public GameObject GO_TurnManager;

        [Tooltip("The GameObject which holds the Battle Manifest as a component.")]
        public GameObject GO_BattleManifest;

        [Tooltip("The GameObject which holds the UI Battle Debrief Manager as a component.")]
        public GameObject GO_UI_BattleDebriefManager;

        private TurnManager turnManager;
        private BattleManifest battleManifest;
        private UI_BattleDebriefManager UI_BattleDebriefManager;

        /// <summary>
        /// Array representing the status of each battle.
        /// </summary>
        private SaveFileData.BattleStatus[] battlesStatus = new SaveFileData.BattleStatus[4];

        /// <summary>
        /// The conditions on which the player can win the battle.
        /// </summary>
        public enum WinConditions
        {
            EnemiesKilled,
            EnemiesCardsUnavailable,
        }

        /// <summary>
        /// The conditions on which the player can fail the battle.
        /// </summary>
        public enum FailConditions
        {
            PlayerKilled,
            PlayerCardsUnavailable,
            AllCardsUnavailableDraw,
        }

        public void InitialiseStaticSubordinates(AppManager inputAppManager)
        {
            Debug.Assert(inputAppManager != null);
            appManager = inputAppManager;

            AssertInspectorInputs();

            battleManifest = GO_BattleManifest.GetComponent<BattleManifest>();
            Debug.Assert(battleManifest != null); // Assert that the component was successfuly found.

            waitForBattleEndDelaySeconds = new WaitForSeconds(BattleEndDelaySeconds);

            battleManifest.Initialise();
        }

        public void InitialiseDynamicSubordinates()
        {
            turnManager = GO_TurnManager.GetComponent<TurnManager>();
            Debug.Assert(turnManager != null); // Assert that the component was found.

            UI_BattleDebriefManager = GO_UI_BattleDebriefManager.GetComponent<UI_BattleDebriefManager>();
            Debug.Assert(UI_BattleDebriefManager != null); // Assert that the component was found.

            UI_BattleDebriefManager.Initialise(appManager.CardsManager, this, appManager.SoundEffectsManager);
            turnManager.Initialise(appManager.UIManager, appManager.CardsManager, this, appManager.TutorialsManager, appManager.CharactersManager, appManager.SoundEffectsManager);

            LoadSaveFile();
        }

        /// <summary>
        /// Assert that all Inspector fields have been set or filled.
        /// </summary>
        private void AssertInspectorInputs()
        {
            Debug.Assert(MapSceneName != "");

            Debug.Assert(BattleEndDelaySeconds > 0f);

            Debug.Assert(GO_TurnManager != null);
            Debug.Assert(GO_BattleManifest != null);
            Debug.Assert(GO_UI_BattleDebriefManager != null);
        }

        /// <summary>
        /// Returns the number of characters in the battle.
        /// </summary>
        public int GetBattleCharactersQuantity()
        {
            Debug.Assert(battleManifest != null);

            return battleManifest.GetEnemiesQuantity() + 1; // Return the number of enemies plus the player.
        }

        /// <summary>
        /// Returns the enum value of the given enemy character index.
        /// </summary>
        /// <param name="enemyCharacterIndex">The enemy character index to look up.</param>
        public CharactersDatabase.Characters GetEnemyCharacter(int enemyCharacterIndex)
        {
            Debug.Assert(enemyCharacterIndex >= 0, enemyCharacterIndex);
            Debug.Assert(enemyCharacterIndex < battleManifest.Enemies.Length, enemyCharacterIndex);

            Debug.Assert(battleManifest != null);

            return battleManifest.Enemies[enemyCharacterIndex];
        }

        /// <summary>
        /// Resolves the player component of the current turn,
        /// with the given action result boolean.
        /// </summary>
        /// <param name="actionSuccessful">Whether the player's action was successful.</param>
        public void ResolvePlayerTurnComponent(bool actionSuccessful)
        {
            Debug.Assert(turnManager != null);

            turnManager.ResolvePlayerComponent(actionSuccessful);
        }

        /// <summary>
        /// Starts the cameo sequence at the start of a battle.
        /// </summary>
        public void StartCameo()
        {
            Debug.Assert(appManager != null);
            Debug.Assert(appManager.UIManager != null);
            Debug.Assert(appManager.CameosManager != null);

            appManager.UIManager.SetBattleUIActive(false);

            appManager.CameosManager.ActivateCameoSeries();
        }

        /// <summary>
        /// Starts the battle after the conclusion of the cameos.
        /// </summary>
        public void StartBattle()
        {
            Debug.Assert(turnManager != null);
            Debug.Assert(appManager != null);
            Debug.Assert(appManager.UIManager != null);
            Debug.Assert(appManager.CharactersManager != null);
            Debug.Assert(appManager.TutorialsManager != null);

            appManager.UIManager.SetBattleUIActive(true);

            appManager.CharactersManager.UnfreezeCharacters();

            appManager.TutorialsManager.NotifyBattleStarted();

            turnManager.StartTurn();
        }

        /// <summary>
        /// Plays the given player's card to the table.
        /// </summary>
        /// <param name="cardToTable">The card which should be played to the table.</param>
        public void PlayPlayerCard(Card cardToTable)
        {
            Debug.Assert(cardToTable != null);

            Debug.Assert(appManager != null);
            Debug.Assert(turnManager != null);
            Debug.Assert(appManager.CardsManager != null);

            if(turnManager.AcceptingPlayerCardSelection) // The user can currently select a card to be played to the table.
            {
                appManager.CardsManager.ClearTabledCards(); // Clear the table's cards, as this occurs as the start of a round.

                appManager.CardsManager.PlayCardToTable(0, cardToTable, 1);

                turnManager.ResolveTabledCards(); // Resolve the card which the player played to the table.
            }
        }

        /// <summary>
        /// Triggers the battle to be ended via victory, with the given win condition.
        /// </summary>
        /// <param name="winCondition">The condition on which the player won the battle.</param>
        public void WinBattle(WinConditions winCondition)
        {
            Debug.Assert(appManager != null);
            Debug.Assert(appManager.CharactersManager != null);
            Debug.Assert(appManager.ParticleEffectsManager != null);

            Debug.Assert(battleEndDelayCoroutine == null);

            appManager.CharactersManager.TriggerCharacterAnimation(0, RigAnimator.CharacterAnimations.Win); // Trigger the Win animation to be played on the player character.
            appManager.CharactersManager.TriggerCharacterAnimation(1, RigAnimator.CharacterAnimations.Death); // Trigger the Death animation to be played on the enemy character.

            if(GetBattleIndex() == 3) // The current battle is the game's final battle.
            {
                appManager.ParticleEffectsManager.PlayFireworksEffect();
            }

            battleEndDelayCoroutine = StartCoroutine(BattleEndDelay(true));
        }

        /// <summary>
        /// Triggers the battle to be ended via failure, with the given failure condition.
        /// </summary>
        /// <param name="failCondition">The condition on which the player failed the battle.</param>
        public void FailBattle(FailConditions failCondition)
        {
            Debug.Assert(appManager != null);
            Debug.Assert(appManager.CharactersManager != null);

            Debug.Assert(battleEndDelayCoroutine == null);

            appManager.CharactersManager.TriggerCharacterAnimation(0, RigAnimator.CharacterAnimations.Death); // Trigger the Death animation to be played on the player character.
            appManager.CharactersManager.TriggerCharacterAnimation(1, RigAnimator.CharacterAnimations.Win); // Trigger the Win animation to be played on the enemy character.

            battleEndDelayCoroutine = StartCoroutine(BattleEndDelay(false));
        }

        /// <summary>
        /// Waits for the set number of seconds before activating the appropriate menu or Scene.
        /// </summary>
        /// <param name="battleWon">Whether the player won the battle.</param>
        private IEnumerator BattleEndDelay(bool battleWon)
        {
            Debug.Assert(turnManager != null);
            Debug.Assert(battleManifest != null);
            Debug.Assert(UI_BattleDebriefManager != null);
            Debug.Assert(appManager.TutorialsManager != null);

            turnManager.SetAcceptingPlayerCardSelection(false);

            appManager.TutorialsManager.StopTutorialsPromptCoroutine();

            yield return waitForBattleEndDelaySeconds;

            if(battleWon)
            {
                if (battleManifest.BattleIndex == 3) // The current battle is the final battle.
                {
                    UnityEngine.SceneManagement.SceneManager.LoadScene("Win Scene");
                }
                else
                {
                    UI_BattleDebriefManager.ActivateVictoryDebrief();
                }
            }
            else // The player has not won the battle.
            {
                UI_BattleDebriefManager.ActivateFailureDebrief();
            }

            battleEndDelayCoroutine = null;
        }

        /// <summary>
        /// Returns the ID of the current turn, where the first turn is 1.
        /// </summary>
        public int GetTurnID()
        {
            Debug.Assert(turnManager != null);

            return turnManager.TurnID;
        }

        /// <summary>
        /// Returns the quantity of attack cards given
        /// to each character at the start of each battle.
        /// </summary>
        public uint GetInitialAttackCardsQuantity()
        {
            Debug.Assert(battleManifest != null);

            return battleManifest.InitialAttackCards;
        }

        /// <summary>
        /// Restarts the current battle by reloading the current Scene.
        /// </summary>
        public void RestartBattle()
        {
            int currentSceneIndex = UnityEngine.SceneManagement.SceneManager.GetActiveScene().buildIndex;
            UnityEngine.SceneManagement.SceneManager.LoadScene(currentSceneIndex);
        }

        /// <summary>
        /// Sets the current battle as attempted, and writes the save-file.
        /// </summary>
        public void SaveCurrentBattleAttempted()
        {
            Debug.Assert(battleManifest != null);

            uint currentBattleIndex = battleManifest.BattleIndex;
            Debug.Assert(currentBattleIndex < battlesStatus.Length, currentBattleIndex);

            battlesStatus[currentBattleIndex] = SaveFileData.BattleStatus.Attempted; // Set the current battle as attempted.
            WriteSaveFile();
        }

        /// <summary>
        /// Triggered when the user has chosen to
        /// upgrade their HP at the end of the battle.
        /// </summary>
        public void BattleEndUpgradeHP()
        {
            Debug.Assert(appManager != null);
            Debug.Assert(appManager.CharactersManager != null);

            appManager.CharactersManager.IncreaseFlavoMaxHP(battleManifest.MaxHPUpgrade);

            FinaliseBattle();
        }

        /// <summary>
        /// Triggered when the user has chosen to steal
        /// the enemy's skill card at the end of the battle.
        /// </summary>
        public void BattleEndStealCard()
        {
            Debug.Assert(appManager != null);
            Debug.Assert(appManager.CharactersManager != null);

            appManager.CharactersManager.SetPlayerHasCard(battleManifest.CardToSteal, true);

            FinaliseBattle();
        }

        /// <summary>
        /// Finalises the battle by marking it as completed, writing the save-file, and loading the map scene.
        /// </summary>
        private void FinaliseBattle()
        {
            Debug.Assert(battleManifest != null);

            uint currentBattleIndex = battleManifest.BattleIndex;
            Debug.Assert(currentBattleIndex < battlesStatus.Length, currentBattleIndex);

            battlesStatus[currentBattleIndex] = SaveFileData.BattleStatus.Completed; // Set the current battle as completed.

            WriteSaveFile();

            UnityEngine.SceneManagement.SceneManager.LoadScene(MapSceneName);
        }

        /// <summary>
        /// Collates the data from the game systems, and saves it to disk.
        /// </summary>
        private void WriteSaveFile()
        {
            Debug.Assert(appManager != null);
            Debug.Assert(appManager.SaveFileManager != null);
            Debug.Assert(appManager.CharactersManager != null);

            CharacterAttributes flavoAttributes = appManager.CharactersManager.GetCharacterAttributes(CharactersDatabase.Characters.Flavo);

            SaveFileData dataToSave = new SaveFileData(battlesStatus[1], battlesStatus[2], battlesStatus[3], (int)flavoAttributes.MaxHealth, flavoAttributes.HasDoubleAttack, flavoAttributes.HasFastAttack, flavoAttributes.HasPotion, flavoAttributes.HasDrainAttack);

            appManager.SaveFileManager.WriteSaveFile(dataToSave);
        }

        /// <summary>
        /// Loads the save-file from disk if it exists and is complete,
        /// and applies its content to the game systems.
        /// </summary>
        public void LoadSaveFile()
        {
            Debug.Assert(battleManifest != null);
            Debug.Assert(appManager != null);
            Debug.Assert(appManager.CameosManager != null);
            Debug.Assert(appManager.SaveFileManager != null);
            Debug.Assert(appManager.CharactersManager != null);

            SaveFileDataLoadWrapper saveFileDataLoadWrapper = appManager.SaveFileManager.LoadSaveFile();
            Debug.Assert(saveFileDataLoadWrapper != null);

            if(saveFileDataLoadWrapper.LoadResult == SaveFileDataLoadWrapper.LoadResults.Successful)
            {
                SaveFileData saveFileData = saveFileDataLoadWrapper.SaveFileData;
                CharactersManager charactersManager = appManager.CharactersManager;

                battlesStatus[1] = saveFileData.Battle1Status;
                battlesStatus[2] = saveFileData.Battle2Status;
                battlesStatus[3] = saveFileData.Battle3Status;

                charactersManager.SetPlayerMaxHP(saveFileData.PlayerMaxHealth);

                charactersManager.SetPlayerHasCard(CardsDatabase.CardTypes.DoubleAttack, saveFileData.HasDoubleAttack);
                charactersManager.SetPlayerHasCard(CardsDatabase.CardTypes.FastAttack, saveFileData.HasFastAttack);
                charactersManager.SetPlayerHasCard(CardsDatabase.CardTypes.Potion, saveFileData.HasPotion);
                charactersManager.SetPlayerHasCard(CardsDatabase.CardTypes.DrainAttack, saveFileData.HasDrainAttack);

                uint currentBattleIndex = battleManifest.BattleIndex;
                Debug.Assert(currentBattleIndex < battlesStatus.Length, currentBattleIndex);

                bool currentBattleAttempted = battlesStatus[currentBattleIndex] == SaveFileData.BattleStatus.Attempted;
                appManager.CameosManager.SetCurrentBattleAttempted(currentBattleAttempted);
            }
        }

        /// <summary>
        /// Returns the card which is offerred at the end of this battle.
        /// </summary>
        /// <returns></returns>
        public CardsDatabase.CardTypes GetCardToSteal()
        {
            Debug.Assert(battleManifest != null);

            return battleManifest.CardToSteal;
        }

        /// <summary>
        /// Returns the max HP increase which is offerred at the end of this battle.
        /// </summary>
        public uint GetHPUpgrade()
        {
            Debug.Assert(battleManifest != null);

            return battleManifest.MaxHPUpgrade;
        }

        /// <summary>
        /// Returns whether the Turn Manager is currently accepting
        /// input from the user to select a hand from their hand.
        /// </summary>
        public bool GetAcceptingPlayerCardSelection()
        {
            Debug.Assert(turnManager != null);

            return turnManager.AcceptingPlayerCardSelection;
        }

        /// <summary>
        /// Sets whether the Turn Manager is currently accepting
        /// input from the user to select a card from their hand.
        /// </summary>
        /// <param name="newAccepting">Whether the system is accepting the player's card selection.</param>
        public void SetAcceptingPlayerCardSelection(bool newAccepting)
        {
            Debug.Assert(turnManager != null);

            turnManager.SetAcceptingPlayerCardSelection(newAccepting);
        }

        /// <summary>
        /// Returns the index of this battle, where the first battle is 1.
        /// </summary>
        public uint GetBattleIndex()
        {
            Debug.Assert(battleManifest != null);

            return battleManifest.BattleIndex;
        }
    }
}