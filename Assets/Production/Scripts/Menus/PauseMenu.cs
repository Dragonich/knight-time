﻿// Author: Luke Saliba
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Pause
{
    public class PauseMenu : MonoBehaviour
    {
        public GameObject pauseMenu;
        public GameObject failureElements;
        public GameObject PlayerPresenceManager;
        static public bool isPaused;
        // Start is called before the first frame update
        void Start()
        {
            pauseMenu.SetActive(false);


        }

        // Update is called once per frame
        void Update()
        {
            
            if (isPaused == false && failureElements.activeSelf == false)
            {
                if (Input.GetKeyDown(KeyCode.Escape))
                {
                    PauseGame(true);
                }
            }
            else if (isPaused == true && failureElements.activeSelf == false)
            {
                if (Input.GetKeyDown(KeyCode.Escape))
                {
                    ResumeGame();

                }
            }
           

        }



        public void PauseGame(bool escapePressed)
        {
            if (LevelSelection.LevelSelecting.mapOpen == false)
            {
                pauseMenu.SetActive(true);
                Time.timeScale = 0f;
                isPaused = true;
            }
        }
        public void ResumeGame()
        {
            if (LevelSelection.LevelSelecting.mapOpen == false)
            {
                pauseMenu.SetActive(false);
                Time.timeScale = 1f;
                isPaused = false;
                PlayerPresenceManager.GetComponent<PlayerPresenceManager>().ResetAbsenceTimer();
            }
        }

        public void QuitGame()
        {
            Application.Quit();
        }
    }
}

