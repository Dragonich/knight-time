﻿// AUTHOR: Benjamin Chaves

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Battle
{
    public class BattleCharactersManager : MonoBehaviour
    {
        private AppManager appManager;

        public const string EnemyName = "Enemy";
        public const string CharacterName = "Flavo";

        [Header("Override Items")]

        [Tooltip("The GameObject which is the parent of all world objects, as opposed to Canvas objects or script objects.")]
        public GameObject WorldObjectsParent;

        [Tooltip("The Canvas parent for the characters' health-potion graphics.")]
        public GameObject HealthGraphicsParent;

        [Header("Prefab Items")]

        [Tooltip("The prefab for a character's health-potion graphics.")]
        public GameObject HealthGraphicsPrefab;

        private GameObject DuckFlavo;

        [Tooltip("The prefab for the character's duck form.")]
        public GameObject DuckFlavoPrefab;

        [Tooltip("The Y distance from which the character's duck object should be offset from the character's object.")]
        public float DuckFlavoPositionOffsetY;

        [Tooltip("The prefab for the heal particle effect.")]
        public GameObject HealParticleEffect;

        [Tooltip("The prefab for Flavo's damage particle effect.")]
        public GameObject FlavoDamageParticleEffect;

        [Tooltip("The prefab for Snailder's damage particle effect.")]
        public GameObject SnailderDamageParticleEffect;

        [Tooltip("The prefab for John's damage particle effect.")]
        public GameObject JohnDamageParticleEffect;

        [Tooltip("The prefab for Bebek's damage particle effect.")]
        public GameObject BebekDamageParticleEffect;

        [Tooltip("The prefab for the Duckify particle effect.")]
        public GameObject DuckifyParticleEffect;

        /// <summary>
        /// Array of AI agents which control the enemy characters.
        /// </summary>
        private AIAgent[] aiAgents;

        /// <summary>
        /// Array of all characters in the current battle.
        /// </summary>
        private BattleCharacter[] battleCharacters;

        /// <summary>
        /// The distance from screen centre with which the characters should be offset.
        /// </summary>
        private const float battleCharactersPositionX = 2f;

        /// <summary>
        /// The random generator which is passed to characters for randomising damage rolls.
        /// </summary>
        private System.Random randomGenerator = new System.Random();

        public void Initialise(AppManager inputAppManager)
        {
            Debug.Assert(inputAppManager != null);
            appManager = inputAppManager;

            AssertInspectorInputs();

            InitialiseBattleCharacters();

            SetRigAnimatorTriggerHashes();

            AllocateHandCards();

            InitialiseAIAgents(appManager.BattleManager.GetBattleCharactersQuantity() - 1);
        }

        /// <summary>
        /// Assert that all Inspector fields have been set or filled.
        /// </summary>
        private void AssertInspectorInputs()
        {
            Debug.Assert(HealthGraphicsPrefab != null);
            Debug.Assert(HealthGraphicsParent != null);

            Debug.Assert(WorldObjectsParent != null);

            Debug.Assert(DuckFlavoPrefab != null);
            Debug.Assert(!Mathf.Approximately(DuckFlavoPositionOffsetY, 0f));

            Debug.Assert(HealParticleEffect != null);
            Debug.Assert(FlavoDamageParticleEffect != null);
            Debug.Assert(SnailderDamageParticleEffect != null);
            Debug.Assert(JohnDamageParticleEffect != null);
            Debug.Assert(BebekDamageParticleEffect != null);
            Debug.Assert(DuckifyParticleEffect != null);
        }

        private void InitialiseBattleCharacters()
        {
            Debug.Assert(appManager != null);
            Debug.Assert(appManager.CardsManager != null);
            Debug.Assert(appManager.BattleManager != null);

            int charactersQuantity = appManager.BattleManager.GetBattleCharactersQuantity();
            int handCardsQuantity = appManager.CardsManager.GetCardsQuantity();

            Debug.Assert(charactersQuantity >= 2, charactersQuantity);
            Debug.Assert(handCardsQuantity > 0, handCardsQuantity);

            CreateDuckFlavo();

            battleCharacters = new BattleCharacter[charactersQuantity];

            InstantiatePlayerCharacter(handCardsQuantity);
            InstantiateEnemyCharacters(handCardsQuantity);
        }

        /// <summary>
        /// Creates the GameObject for the player character's duck form.
        /// </summary>
        private void CreateDuckFlavo()
        {
            Debug.Assert(DuckFlavo == null);
            Debug.Assert(DuckFlavoPrefab != null);
            Debug.Assert(WorldObjectsParent != null);

            DuckFlavo = Instantiate(DuckFlavoPrefab);

            DuckFlavo.name = "Duck Flavo";
            DuckFlavo.transform.SetParent(WorldObjectsParent.transform);

            DuckFlavo.SetActive(false);
        }

        /// <summary>
        /// Instantiates the player character with the given number of hand-card sets.
        /// </summary>
        /// <param name="handCardsQuantity">The player's number of hand-card sets.</param>
        private void InstantiatePlayerCharacter(int handCardsQuantity)
        {
            Debug.Assert(handCardsQuantity > 0, handCardsQuantity);

            Debug.Assert(appManager != null);
            Debug.Assert(appManager.CharactersManager != null);
            Debug.Assert(WorldObjectsParent != null);
            Debug.Assert(HealthGraphicsPrefab != null);
            Debug.Assert(HealthGraphicsParent != null);
            Debug.Assert(HealParticleEffect != null);
            Debug.Assert(FlavoDamageParticleEffect != null);
            Debug.Assert(DuckifyParticleEffect != null);

            CharacterAttributes currentCharacterAttributes = appManager.CharactersManager.GetCharacterAttributes(CharactersDatabase.Characters.Flavo);
            Debug.Assert(currentCharacterAttributes != null);
            Debug.Assert(currentCharacterAttributes.CharacterPrefab != null);

            GameObject characterObject = Instantiate(currentCharacterAttributes.CharacterPrefab);

            characterObject.name = CharacterName;
            characterObject.transform.SetParent(WorldObjectsParent.transform);
            characterObject.transform.position = new Vector3(-battleCharactersPositionX, -1f + currentCharacterAttributes.CharacterObjectOffset, 0f);

            float characterObjectScale = currentCharacterAttributes.CharacterObjectScale;
            characterObject.transform.localScale = new Vector3(characterObjectScale, characterObjectScale, characterObjectScale);

            GameObject characterHealthGraphics = Instantiate(HealthGraphicsPrefab);
            characterHealthGraphics.transform.SetParent(HealthGraphicsParent.transform);
            characterHealthGraphics.name = currentCharacterAttributes.Name + " Health Graphics";

            UI_HealthGraphicsAttributesManager characterHealthGraphicsManager = characterHealthGraphics.GetComponent<UI_HealthGraphicsAttributesManager>();
            Debug.Assert(characterHealthGraphicsManager != null); // Assert that the component was found.

            characterHealthGraphicsManager.SetIsPlayerHealthGraphics(true);

            GameObject[] characterObjects = { characterObject, DuckFlavo };

            GameObject healParticleEffect = Instantiate(HealParticleEffect);
            GameObject damageParticleEffect = Instantiate(FlavoDamageParticleEffect);
            GameObject duckifyParticleEffect = Instantiate(DuckifyParticleEffect);

            battleCharacters[0] = new BattleCharacter(currentCharacterAttributes, ref characterObjects, characterHealthGraphicsManager, 0, handCardsQuantity, appManager.CanvasConversionManager, damageParticleEffect, healParticleEffect, duckifyParticleEffect);

            Vector3 characterPosition = battleCharacters[0].GetCharacterWorldPosition();

            Vector3 newDuckPosition = Vector3.zero;
            newDuckPosition.x = characterPosition.x;
            newDuckPosition.y = DuckFlavoPositionOffsetY;
            newDuckPosition.z = characterPosition.z;

            DuckFlavo.transform.position = newDuckPosition;
        }

        /// <summary>
        /// Instantiates the enemy character with the given number of hand-card sets.
        /// </summary>
        /// <param name="handCardsQuantity">The enemy's number of hand-card sets.</param>
        private void InstantiateEnemyCharacters(int handCardsQuantity)
        {
            Debug.Assert(handCardsQuantity > 0, handCardsQuantity);

            Debug.Assert(battleCharacters.Length >= 2);
            Debug.Assert(appManager != null);
            Debug.Assert(appManager.BattleManager != null);
            Debug.Assert(appManager.CharactersManager != null);
            Debug.Assert(WorldObjectsParent != null);
            Debug.Assert(HealthGraphicsPrefab != null);
            Debug.Assert(HealthGraphicsParent != null);
            Debug.Assert(HealParticleEffect != null);
            Debug.Assert(DuckifyParticleEffect != null);

            for (int characterIndex = 1; characterIndex < battleCharacters.Length; characterIndex++) // Iterate through each non-player character in this battle.
            {
                CharactersDatabase.Characters currentCharacter = appManager.BattleManager.GetEnemyCharacter(characterIndex - 1);

                CharacterAttributes currentCharacterAttributes = appManager.CharactersManager.GetCharacterAttributes(currentCharacter);
                Debug.Assert(currentCharacterAttributes != null);
                Debug.Assert(currentCharacterAttributes.CharacterPrefab != null);

                GameObject characterObject = Instantiate(currentCharacterAttributes.CharacterPrefab);

                characterObject.name = EnemyName;
                characterObject.transform.SetParent(WorldObjectsParent.transform);
                characterObject.transform.position = new Vector3(battleCharactersPositionX, -1f + currentCharacterAttributes.CharacterObjectOffset, 0f);

                float characterObjectScale = currentCharacterAttributes.CharacterObjectScale;
                characterObject.transform.localScale = new Vector3(characterObjectScale, characterObjectScale, characterObjectScale);

                GameObject characterHealthGraphics = Instantiate(HealthGraphicsPrefab);
                characterHealthGraphics.transform.SetParent(HealthGraphicsParent.transform);
                characterHealthGraphics.name = currentCharacterAttributes.Name + " Health Graphics";

                UI_HealthGraphicsAttributesManager characterHealthGraphicsManager = characterHealthGraphics.GetComponent<UI_HealthGraphicsAttributesManager>();
                Debug.Assert(characterHealthGraphicsManager != null); // Assert that the component was found.

                characterHealthGraphicsManager.SetIsPlayerHealthGraphics(false);

                GameObject[] characterObjects = { characterObject };

                GameObject damageParticleEffectPrefab = GetCharacterDamageParticleEffect(currentCharacter);
                Debug.Assert(damageParticleEffectPrefab != null);

                GameObject damageParticleEffect = Instantiate(damageParticleEffectPrefab);
                GameObject healParticleEffect = Instantiate(HealParticleEffect);
                GameObject duckifyParticleEffect = Instantiate(DuckifyParticleEffect);

                Debug.Assert(battleCharacters[characterIndex] == null);
                battleCharacters[characterIndex] = new BattleCharacter(currentCharacterAttributes, ref characterObjects, characterHealthGraphicsManager, characterIndex, handCardsQuantity, appManager.CanvasConversionManager, damageParticleEffect, healParticleEffect, duckifyParticleEffect);
            }
        }

        /// <summary>
        /// Returns the damage particle effect for the given character.
        /// </summary>
        /// <param name="characterToGet">The characters whose particle effect should be returned.</param>
        private GameObject GetCharacterDamageParticleEffect(CharactersDatabase.Characters characterToGet)
        {
            Debug.Assert(FlavoDamageParticleEffect != null);
            Debug.Assert(SnailderDamageParticleEffect != null);
            Debug.Assert(JohnDamageParticleEffect != null);
            Debug.Assert(BebekDamageParticleEffect != null);

            switch (characterToGet)
            {
                case CharactersDatabase.Characters.Flavo:

                    return FlavoDamageParticleEffect;

                case CharactersDatabase.Characters.Snailor:

                    return SnailderDamageParticleEffect;

                case CharactersDatabase.Characters.John:

                    return JohnDamageParticleEffect;

                case CharactersDatabase.Characters.DuckDragon:

                    return BebekDamageParticleEffect;

                default:

                    Debug.LogError(characterToGet);
                    return null;
            }
        }

        private void SetRigAnimatorTriggerHashes()
        {
            int attackTriggerHash = Animator.StringToHash("Attack Trigger");
            int healTriggerHash = Animator.StringToHash("Heal Trigger");
            int recoilTriggerHash = Animator.StringToHash("Recoil Trigger");
            int duckifyTriggerHash = Animator.StringToHash("Duckify Trigger");

            int unfreezeTriggerHash = Animator.StringToHash("Unfreeze Trigger");

            int winTriggerHash = Animator.StringToHash("Win Trigger");
            int deathTriggerHash = Animator.StringToHash("Death Trigger");

            RigAnimator.SetTriggerHashes(attackTriggerHash, healTriggerHash, recoilTriggerHash, duckifyTriggerHash, unfreezeTriggerHash, winTriggerHash, deathTriggerHash);
        }

        private void Update()
        {
            AdvanceDamageTintTimers();
        }

        /// <summary>
        /// Advances the damage tint timers for each character.
        /// </summary>
        private void AdvanceDamageTintTimers()
        {
            BattleCharacter currentBattleCharacter;

            for (int index = 0; index < battleCharacters.Length; index++) // Iterate through each character index.
            {
                currentBattleCharacter = battleCharacters[index];
                Debug.Assert(currentBattleCharacter != null);

                currentBattleCharacter.AdvanceDamageTintTimer(Time.deltaTime); // Advance the damage tint timer for the current character.
            }
        }

        /// <summary>
        /// Returns whether the player has the given card.
        /// </summary>
        /// <param name="cardID">The card whose availability should be checked.</param>
        public bool GetPlayerHasCard(int cardID)
        {
            Debug.Assert(cardID >= 0, cardID);

            BattleCharacter playerCharacter = battleCharacters[0];
            Debug.Assert(playerCharacter != null);

            return playerCharacter.GetHasCard(cardID);
        }

        /// <summary>
        /// Initialises AI agents with the given quantity of agents.
        /// </summary>
        /// <param name="agentsQuantity">The number of AI agents which should be initialised.</param>
        private void InitialiseAIAgents(int agentsQuantity)
        {
            Debug.Assert(agentsQuantity >= 1, agentsQuantity);

            Debug.Assert(appManager != null);
            Debug.Assert(appManager.CardsManager != null);
            Debug.Assert(appManager.CharactersManager != null);

            aiAgents = new AIAgent[agentsQuantity];

            for (int index = 0; index < aiAgents.Length; index++)
            {
                int battleCharacterIndex = index + 1;

                aiAgents[index] = new AIAgent(battleCharacters[battleCharacterIndex], appManager.CardsManager, appManager.CharactersManager);
            }
        }

        /// <summary>
        /// Allocates each card into each character's hands.
        /// </summary>
        public void AllocateHandCards()
        {
            Debug.Assert(appManager != null);
            Debug.Assert(appManager.CardsManager != null);
            Debug.Assert(appManager.BattleManager != null);

            Card currentCard;
            BattleCharacter currentBattleCharacter;

            for (int cardIndex = 0; cardIndex < appManager.CardsManager.GetCardsQuantity(); cardIndex++) // Iterate through each card.
            {
                currentCard = appManager.CardsManager.GetDatabaseCardByID(cardIndex);
                Debug.Assert(currentCard != null);

                for (int battleCharacterIndex = 0; battleCharacterIndex < battleCharacters.Length; battleCharacterIndex++) // Iterate through each battle character.
                {
                    currentBattleCharacter = battleCharacters[battleCharacterIndex];
                    Debug.Assert(currentBattleCharacter != null);

                    currentBattleCharacter.InitialiseCardSet(currentCard, (int)appManager.BattleManager.GetInitialAttackCardsQuantity());
                }
            }
        }

        /// <summary>
        /// Modifies the given character's current health value by the given amount.
        /// </summary>
        /// <param name="battleCharacterID">The character whose health should be modified.</param>
        /// <param name="healthModifier">The value by which the character's health should be modified.</param>
        public void ModifyCharacterHealth(int battleCharacterID, float healthModifier)
        {
            Debug.Assert(battleCharacterID >= 0, battleCharacterID);
            Debug.Assert(battleCharacterID < battleCharacters.Length, battleCharacterID);

            BattleCharacter battleCharacter = battleCharacters[battleCharacterID];
            Debug.Assert(battleCharacter != null);

            battleCharacter.ModifyHealth(healthModifier);
        }

        /// <summary>
        /// Returns the current health value of the given character.
        /// </summary>
        /// <param name="battleCharacterID">The character whose health value should be returned.</param>
        public float GetCharacterHealth(int battleCharacterID)
        {
            Debug.Assert(battleCharacterID >= 0, battleCharacterID);
            Debug.Assert(battleCharacterID < battleCharacters.Length, battleCharacterID);

            BattleCharacter battleCharacter = battleCharacters[battleCharacterID];
            Debug.Assert(battleCharacter != null);

            return battleCharacter.CurrentHealth;
        }

        /// <summary>
        /// Debits the given card from the character's hand.
        /// If the card is a skill card, it will be put into cooldown.
        /// If the card is a combat card, 1 use will be removed from the set.
        /// </summary>
        /// <param name="handCardID"></param>
        public void DebitPlayerCardFromSet(int handCardID)
        {
            Debug.Assert(handCardID >= 0);

            BattleCharacter playerCharacter = battleCharacters[0];
            Debug.Assert(playerCharacter != null);

            playerCharacter.DebitCardFromSet(handCardID);
        }

        /// <summary>
        /// Triggers each AI to select & play a card to the table.
        /// </summary>
        public void TableAICards()
        {
            AIAgent currentAgent;

            for (int index = 0; index < aiAgents.Length; index++) // Iterate through each AI agent.
            {
                currentAgent = aiAgents[index];
                Debug.Assert(currentAgent != null, index);

                currentAgent.PlayCardToTable();
            }
        }

        /// <summary>
        /// Returns the quantity of cards the player has in the given card set.
        /// </summary>
        /// <param name="cardSetID">The card whose quantity should be returned.</param>
        public int GetPlayerCardsQuantity(int cardSetID)
        {
            Debug.Assert(cardSetID >= 0, cardSetID);

            BattleCharacter playerCharacter = battleCharacters[0];
            Debug.Assert(playerCharacter != null);

            return playerCharacter.GetCardQuantity(cardSetID);
        }

        /// <summary>
        /// Returns whether the player has the given card
        /// set available to be played to the table.
        /// </summary>
        /// <param name="cardID">The card whose availability should be returned.</param>
        public bool GetPlayerHasCardAvailable(int cardID)
        {
            Debug.Assert(cardID >= 0, cardID);

            BattleCharacter playerCharacter = battleCharacters[0];
            Debug.Assert(playerCharacter != null);

            return playerCharacter.GetHasCardsAvailable(cardID);
        }

        /// <summary>
        /// Returns the number of cooldown turns remaining on the player's given card.
        /// </summary>
        /// <param name="cardSetID">The card whose cooldown turns should be returned.</param>
        public int GetPlayerCardCooldowns(int cardSetID)
        {
            Debug.Assert(cardSetID >= 0, cardSetID);

            BattleCharacter playerCharacter = battleCharacters[0];
            Debug.Assert(playerCharacter != null);

            return playerCharacter.GetCardCooldowns(cardSetID);
        }

        /// <summary>
        /// Advances all skill card cooldowns for each skill card for each character.
        /// </summary>
        public void AdvanceSkillCardCooldowns()
        {
            BattleCharacter currentBattleCharacter;

            for (int index = 0; index < battleCharacters.Length; index++)
            {
                currentBattleCharacter = battleCharacters[index];
                Debug.Assert(currentBattleCharacter != null, index);

                currentBattleCharacter.AdvanceSkillCardCooldowns();
            }
        }

        /// <summary>
        /// Sets the turns for which the player is Duckified.
        /// </summary>
        /// <param name="newTurnsDuckified"></param>
        public void SetPlayerDuckified(int newTurnsDuckified)
        {
            Debug.Assert(newTurnsDuckified > 0, newTurnsDuckified);

            BattleCharacter playerCharacter = battleCharacters[0];
            Debug.Assert(playerCharacter != null);

            playerCharacter.SetTurnsDuckified(newTurnsDuckified);
        }

        /// <summary>
        /// Advances the turns for which the player is Duckified,
        /// and un-Duckifies the player if appropriate.
        /// </summary>
        public void AdvanceTurnsDuckified()
        {
            BattleCharacter playerCharacter = battleCharacters[0];
            Debug.Assert(playerCharacter != null);

            if (playerCharacter.TurnsDuckified > 0) // The player is currently Duckified.
            {
                bool playerUnduckifiedThisTurn = playerCharacter.TurnsDuckified == 1; // Caches whether the player will be unduckified once this turn is advanced.

                playerCharacter.AdvanceTurnsDuckified(); // Advances the turns for which the player is Duckified.

                if (playerUnduckifiedThisTurn)
                {
                    SetDuckFlavoActive(false);
                }
            }
        }

        /// <summary>
        /// Returns whether the player has at least 1 card available to play from their hand.
        /// </summary>
        public bool GetPlayerHasAnyCardsAvailable()
        {
            BattleCharacter playerCharacter = battleCharacters[0];
            Debug.Assert(playerCharacter != null);

            return playerCharacter.GetHasAnyCardsAvailable();
        }

        /// <summary>
        /// Returns whether any enemies have at least 1 card available to play.
        /// </summary>
        public bool GetEnemiesHaveAnyCardsAvailable()
        {
            BattleCharacter currentEnemyCharacter;

            for (int index = 1; index < battleCharacters.Length; index++) // Iterate through each enemy index.
            {
                currentEnemyCharacter = battleCharacters[index];
                Debug.Assert(currentEnemyCharacter != null);

                if (currentEnemyCharacter.GetHasAnyCardsAvailable()) // The current enemy has cards available.
                {
                    return true; // Return true, as the current enemy has cards available.
                }
            }

            return false; // Return false, as no enemies were found to have cards available.
        }

        /// <summary>
        /// Returns the randomised damage roll by the given character.
        /// </summary>
        /// <param name="characterID">The character whose damage should be rolled.</param>
        public uint GetRolledCharacterDamageValue(int characterID)
        {
            Debug.Assert(characterID >= 0, characterID);
            Debug.Assert(characterID < battleCharacters.Length, characterID);

            Debug.Assert(randomGenerator != null);

            BattleCharacter battleCharacter = battleCharacters[characterID];
            Debug.Assert(battleCharacter != null);

            return battleCharacter.GetRolledCharacterDamageValue(randomGenerator);
        }

        /// <summary>
        /// Triggers the given animation to be played on the given character.
        /// </summary>
        /// <param name="characterID">The character who should play the animation.</param>
        /// <param name="animationToTrigger">The animation which the character should play.</param>
        public void TriggerCharacterAnimation(int characterID, RigAnimator.CharacterAnimations animationToTrigger)
        {
            Debug.Assert(characterID >= 0, characterID);
            Debug.Assert(characterID < battleCharacters.Length, characterID);

            BattleCharacter battleCharacter = battleCharacters[characterID];
            Debug.Assert(battleCharacter != null);

            battleCharacter.TriggerAnimation(animationToTrigger);
        }

        /// <summary>
        /// Returns the remaining turns for which the player is Duckified.
        /// </summary>
        public int GetPlayerTurnsDuckified()
        {
            BattleCharacter playerCharacter = battleCharacters[0];
            Debug.Assert(playerCharacter != null);

            Debug.Assert(playerCharacter.TurnsDuckified >= 0);
            return playerCharacter.TurnsDuckified;
        }

        /// <summary>
        /// Sets the active status of the Flavo and Duck-Flavo character objects,
        /// inverse to each other depending on the given boolean.
        /// </summary>
        /// <param name="newIsDuckified"></param>
        public void SetDuckFlavoActive(bool newIsDuckified)
        {
            Debug.Assert(DuckFlavo != null);

            DuckFlavo.SetActive(newIsDuckified);

            BattleCharacter playerCharacter = battleCharacters[0];
            Debug.Assert(playerCharacter != null);

            playerCharacter.SetCharacterObjectActive(!newIsDuckified); // Set the character object active when the character is not duckified, or inactive if duckified.

            if (!newIsDuckified) // The player is being un-duckified.
            {
                playerCharacter.UnfreezeCharacter(); // Unfreeze the character, and transition it into its idle animation.
            }
        }

        /// <summary>
        /// Sets the player character to have the given tint.
        /// </summary>
        /// <param name="tintToSet"></param>
        public void SetPlayerTint(CharacterTinter.CharacterTints tintToSet)
        {
            BattleCharacter playerCharacter = battleCharacters[0];
            Debug.Assert(playerCharacter != null);

            playerCharacter.SetCharacterTint(tintToSet);
        }

        /// <summary>
        /// Sets the enemy character to have the given tint.
        /// </summary>
        /// <param name="tintToSet"></param>
        public void SetEnemyTint(CharacterTinter.CharacterTints tintToSet)
        {
            BattleCharacter enemyCharacter = battleCharacters[1];
            Debug.Assert(enemyCharacter != null);

            enemyCharacter.SetCharacterTint(tintToSet);
        }

        /// <summary>
        /// Unfreezes the character animations, after being frozen for the cameos.
        /// </summary>
        public void UnfreezeCharacters()
        {
            BattleCharacter currentBattleCharacter;

            for (int index = 0; index < battleCharacters.Length; index++) // Iterate through each character.
            {
                currentBattleCharacter = battleCharacters[index];
                Debug.Assert(currentBattleCharacter != null);

                currentBattleCharacter.UnfreezeCharacter();
            }
        }

        /// <summary>
        /// Plays the Duckify particle effect on the enemy character.
        /// </summary>
        public void PlayEnemyDuckifyEffect()
        {
            BattleCharacter enemyCharacter = battleCharacters[1];
            Debug.Assert(enemyCharacter != null);

            enemyCharacter.PlayDuckifyParticleEffect();
        }
    }
}