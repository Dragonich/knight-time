﻿// AUTHOR: Benjamin Chaves

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using TMPro;

namespace Battle
{
    public class UI_TurnCounterManager : MonoBehaviour
    {
        public GameObject GO_TurnCounterText;

        /// <summary>
        /// The TextMesh Pro component of the turn counter text GameObject.
        /// </summary>
        private TextMeshProUGUI turnCounterText;

        public void Initialise()
        {
            AssertInspectorInputs();

            CacheSubordinates();

            SetTurnCounterActive(false);
        }

        /// <summary>
        /// Assert that all Inspector fields have been set or filled.
        /// </summary>
        private void AssertInspectorInputs()
        {
            Debug.Assert(GO_TurnCounterText != null);
        }

        private void CacheSubordinates()
        {
            turnCounterText = GO_TurnCounterText.GetComponent<TextMeshProUGUI>();
            Debug.Assert(turnCounterText != null); // Assert that the component was found.
        }

        /// <summary>
        /// Sets the turn counter text to represent the current turn number.
        /// </summary>
        public void SetTurnCounterText(int newTurn)
        {
            Debug.Assert(newTurn >= 0);

            Debug.Assert(turnCounterText != null);

            turnCounterText.text = newTurn.ToString();
        }

        /// <summary>
        /// Sets whether the turn counter GameObject is active.
        /// </summary>
        /// <param name="newIsActive">The new active status of the turn counter GameObject.</param>
        public void SetTurnCounterActive(bool newIsActive)
        {
            Debug.Assert(GO_TurnCounterText != null);

            GO_TurnCounterText.SetActive(newIsActive);
        }
    }
}