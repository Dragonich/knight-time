﻿// AUTHOR: Benjamin Chaves

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UserPreferences
{
    public class UserPreferencesFileData
    {
        /// <summary>
        /// The value of the music volume, between 0f and 1f, both inclusive.
        /// </summary>
        public float MusicVolume { get; private set; }

        public UserPreferencesFileData(float inputMusicVolume)
        {
            Debug.Assert(inputMusicVolume >= 0f);
            Debug.Assert(inputMusicVolume <= 1f);

            MusicVolume = inputMusicVolume;
        }
    }
}