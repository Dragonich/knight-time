﻿// Author: Luke Saliba
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace DisolveTransitionShader
{
    
    public class DisolveShader : MonoBehaviour
    {
        public float FadeTime;
        public float Delay;
        
        public Material material;

        private void Awake()
        {
            //material = GetComponent<Material>().material;
        }

        private void Update()
        {
            //setting fade time to 0 will change the progression stage of the transition
            FadeTime += Time.deltaTime;
            var FadeLevel = FadeTime * Mathf.PI * 0.1f; //editing this float will allow the trasition to happen at a faster or slower pace
            if (FadeLevel < 1)
            {
                SetFade(FadeLevel);
            }
        }
        private void SetFade(float fade)
        {

            material.SetFloat("_fade", fade);
        }
        public void ResetFade()
        {

            StartCoroutine(ButtonDelay());

        }
        private IEnumerator ButtonDelay()
        {
            yield return new WaitForSeconds(Delay);
            FadeTime = 0;
            
        }
    }
}
