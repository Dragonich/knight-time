﻿// AUTHOR: Benjamin Chaves

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Battle
{
    public class TabledCardsManager : MonoBehaviour
    {
        /// <summary>
        /// Array of the cards which the characters have played to the table.
        /// </summary>
        private TabledCard[] tabledCards;

        public void Initialise(int playersQuantity)
        {
            Debug.Assert(playersQuantity >= 2);

            InitialiseTabledCards(playersQuantity);
        }

        private void InitialiseTabledCards(int playersQuantity)
        {
            Debug.Assert(playersQuantity >= 2);

            tabledCards = new TabledCard[playersQuantity];

            for(int index = 0; index < tabledCards.Length; index++)
            {
                Debug.Assert(tabledCards[index] == null);
                tabledCards[index] = new TabledCard();
            }
        }

        /// <summary>
        /// Returns the card which the given character has tabled.
        /// </summary>
        /// <param name="battleCharacterID">The character whose tabled card should be returned.</param>
        public TabledCard GetTabledCard(int battleCharacterID)
        {
            Debug.Assert(battleCharacterID >= 0, battleCharacterID);
            Debug.Assert(battleCharacterID < tabledCards.Length, battleCharacterID);

            TabledCard tabledCard = tabledCards[battleCharacterID];
            Debug.Assert(tabledCards != null);

            return tabledCard;
        }

        /// <summary>
        /// Sets the given character's tabled card to be of the given card,
        /// and for the card to target the given character.
        /// </summary>
        /// <param name="battleCharacterID">The character playing the card to the table.</param>
        /// <param name="cardToTable">The card which should be played to the table.</param>
        /// <param name="characterTarget">The character who is being targeted by the played card.</param>
        public void SetCharacterTabledCard(int battleCharacterID, Card cardToTable, int characterTarget)
        {
            Debug.Assert(battleCharacterID >= 0, battleCharacterID);
            Debug.Assert(battleCharacterID < tabledCards.Length, battleCharacterID);
            Debug.Assert(cardToTable != null);
            Debug.Assert(characterTarget >= 0);

            TabledCard tabledCard = tabledCards[battleCharacterID];
            Debug.Assert(tabledCard != null);

            tabledCard.SetAttributes(cardToTable, characterTarget);
        }

        /// <summary>
        /// Clears any cards currently played to the table.
        /// </summary>
        public void ClearTabledCards()
        {
            TabledCard currentTabledCard;

            for (int characterIndex = 0; characterIndex < tabledCards.Length; characterIndex++)
            {
                currentTabledCard = tabledCards[characterIndex];
                Debug.Assert(currentTabledCard != null);

                currentTabledCard.SetBlank();
            }
        }

        /// <summary>
        /// Returns the mouse symbol of the player's tabled card.
        /// </summary>
        public MouseActionsManager.Symbols GetPlayerCardSymbol()
        {
            Debug.Assert(tabledCards[0].Card != null); // Assert that the player has played a card to the table.

            return tabledCards[0].Card.MouseSymbol;
        }
    }
}