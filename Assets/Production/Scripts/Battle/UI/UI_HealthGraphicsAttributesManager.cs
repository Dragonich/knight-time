﻿// AUTHOR: Benjamin Chaves

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using TMPro;

namespace Battle
{
    public class UI_HealthGraphicsAttributesManager : MonoBehaviour
    {
        [Header("Prefab Items")]

        [Tooltip("The health which each potion should hold.")]
        public float HealthPerPotion;

        [Tooltip("The seconds for which the hit-score should hold on-screen.")]
        public float HitScoreHoldSeconds;
        private Coroutine holdHitScoreCoroutine;
        private WaitForSeconds waitForHitScoreHoldSeconds;

        [Tooltip("The horizontal separation between each potion.")]
        public float RearPotionsSeparationX;

        [Tooltip("The vertical separation between each potion.")]
        public float RearPotionsSeparationY;

        [Tooltip("The negative vertical distance which the liquid graphics need to be translated for the potion to appear empty.")]
        public float ZeroLiquidPositionY;

        public GameObject FrontPotion;
        public GameObject RearPotionsParent;
        public GameObject RearPotionTemplate;

        public GameObject GO_HealthText;
        private TextMeshProUGUI healthText;

        public GameObject ScalingHealthImage;
        private RectTransform scalingHealthRectTransform;
        private Vector2 healthLiquidPositionSetter = Vector2.zero;

        public GameObject GO_HitScoreText;
        private TextMeshProUGUI hitScoreText;

        /// <summary>
        /// Whether this potion set is the player's or the enemy's.
        /// </summary>
        private bool isPlayerHealth;

        private RectTransform parentRectTransform;

        private GameObject[] potionGraphics;
        private float[] potionValues;

        private float maximumHealth;

        public void Initialise(CanvasConversionManager inputCanvasConversionManager, float inputMaximumHealth)
        {
            Debug.Assert(inputMaximumHealth > 0f);
            Debug.Assert(inputCanvasConversionManager != null);

            AssertInspectorInputs();

            CacheSubordinates();

            maximumHealth = inputMaximumHealth;            

            GO_HitScoreText.SetActive(false);            

            waitForHitScoreHoldSeconds = new WaitForSeconds(HitScoreHoldSeconds);

            SetHitScorePosition();

            int isPlayerPositionMultiplier = isPlayerHealth ? -1 : 1;
            parentRectTransform.anchoredPosition = inputCanvasConversionManager.GetMultipliedCanvasPosition(0.325f * isPlayerPositionMultiplier, -0.35f);

            InstantiateRearPotions();

            DistributeHealth(maximumHealth);
            SetPotionHealthGraphicsPositioning();

            SetHealthText(maximumHealth);
        }

        /// <summary>
        /// Assert that all Inspector fields have been set or filled.
        /// </summary>
        private void AssertInspectorInputs()
        {
            Debug.Assert(HealthPerPotion > 0f, HealthPerPotion);

            Debug.Assert(HitScoreHoldSeconds > 0f, HitScoreHoldSeconds);

            Debug.Assert(!Mathf.Approximately(RearPotionsSeparationX, 0f), RearPotionsSeparationX);
            Debug.Assert(!Mathf.Approximately(RearPotionsSeparationY, 0f), RearPotionsSeparationY);

            Debug.Assert(ZeroLiquidPositionY < 0f, ZeroLiquidPositionY);

            Debug.Assert(FrontPotion != null);

            Debug.Assert(RearPotionsParent != null);
            Debug.Assert(RearPotionTemplate != null);

            Debug.Assert(ScalingHealthImage != null);

            Debug.Assert(GO_HealthText != null);
            Debug.Assert(GO_HitScoreText != null);
        }

        private void CacheSubordinates()
        {
            Debug.Assert(ScalingHealthImage != null);
            Debug.Assert(GO_HealthText != null);
            Debug.Assert(GO_HitScoreText != null);

            scalingHealthRectTransform = ScalingHealthImage.GetComponent<RectTransform>();
            Debug.Assert(scalingHealthRectTransform != null); // Assert that the component was found.

            parentRectTransform = GetComponent<RectTransform>();
            Debug.Assert(parentRectTransform != null); // Assert that the component was found.

            healthText = GO_HealthText.GetComponent<TextMeshProUGUI>();
            Debug.Assert(healthText != null); // Assert that the component was found.

            hitScoreText = GO_HitScoreText.GetComponent<TextMeshProUGUI>();
            Debug.Assert(hitScoreText != null); // Assert that the component was found.
        }

        /// <summary>
        /// Sets whether these graphics represent the player's health to the given boolean.
        /// </summary>
        public void SetIsPlayerHealthGraphics(bool newIsPlayerHealth)
        {
            isPlayerHealth = newIsPlayerHealth;
        }

        /// <summary>
        /// Sets the position of the hit score graphic.
        /// </summary>
        private void SetHitScorePosition()
        {
            Debug.Assert(GO_HitScoreText != null);

            RectTransform hitScoreTextRectTransform = GO_HitScoreText.GetComponent<RectTransform>();
            Debug.Assert(hitScoreTextRectTransform != null); // Assert that the component was found.

            Vector2 newPosition = Vector2.zero;

            newPosition.x = isPlayerHealth ? 125f : -125f;
            newPosition.y = 750f;

            hitScoreTextRectTransform.anchoredPosition = newPosition;
        }

        /// <summary>
        /// Instantiates the non-front potions required for this potion set.
        /// </summary>
        private void InstantiateRearPotions()
        {
            Debug.Assert(FrontPotion != null);
            Debug.Assert(RearPotionTemplate != null);

            int rearPotionsQuantity = (int)Mathf.Ceil((maximumHealth - HealthPerPotion) / HealthPerPotion); // Calculate the number of rear potions required for the character's max health.

            potionGraphics = new GameObject[rearPotionsQuantity + 1];
            potionValues = new float[potionGraphics.Length + 1];

            potionGraphics[0] = FrontPotion;

            Vector2 newRearPotionPosition = Vector2.zero;

            for (int index = 1; index < potionGraphics.Length; index++) // Iterate through each potion graphic, starting with the first rear potion.
            {
                GameObject newRearPotion = Instantiate(RearPotionTemplate);

                potionGraphics[index] = newRearPotion;
                newRearPotion.name = "Rear Potion " + index;

                newRearPotion.transform.SetParent(RearPotionsParent.transform);
                newRearPotion.transform.SetAsFirstSibling();

                RectTransform newRearPotionRectTransform = newRearPotion.GetComponent<RectTransform>();
                Debug.Assert(newRearPotionRectTransform != null); // Assert that the component was found.

                newRearPotionPosition.x += isPlayerHealth ? -RearPotionsSeparationX : RearPotionsSeparationX; // Move this potion's position left or right on-screen, if player potions or not.
                newRearPotionPosition.y += RearPotionsSeparationY;

                newRearPotionRectTransform.anchoredPosition = newRearPotionPosition;
            }
        }

        /// <summary>
        /// Updates the character's health graphics to reflect the given health value.
        /// </summary>
        /// <param name="newCurrentHealth">The character's new current health.</param>
        public void UpdateHealthGraphics(float newCurrentHealth)
        {
            Debug.Assert(newCurrentHealth >= 0f, newCurrentHealth);
            Debug.Assert(newCurrentHealth <= maximumHealth, newCurrentHealth);

            DistributeHealth(newCurrentHealth);
            SetPotionHealthGraphicsPositioning();

            SetHealthText(newCurrentHealth);
        }

        /// <summary>
        /// Sets the position of each health graphic of each health-potion
        /// to reflect each health-potion's health distribution.
        /// </summary>
        private void SetPotionHealthGraphicsPositioning()
        {
            for (int index = 0; index < potionGraphics.Length; index++) // Iterate through each potion graphic.
            {
                GameObject currentPotion = potionGraphics[index];
                Debug.Assert(currentPotion != null);

                Transform currentPotionChild = currentPotion.transform.GetChild(1);
                Debug.Assert(currentPotionChild != null);

                Transform currentPotionGraphic = currentPotionChild.GetChild(0);
                Debug.Assert(currentPotionGraphic != null); // Assert that the component was found.

                RectTransform currentPotionGraphicRectTransform = currentPotionGraphic.GetComponent<RectTransform>();
                Debug.Assert(currentPotionGraphicRectTransform != null); // Assert that the component was found.

                float potionPercentage = potionValues[index] / HealthPerPotion; // Calculate this potion's health percentage of the health per potion.
                healthLiquidPositionSetter.y = ZeroLiquidPositionY * (1f - potionPercentage);
                currentPotionGraphicRectTransform.anchoredPosition = healthLiquidPositionSetter; // Apply this potion's health percentage to the health graphic.

                bool currentPotionSetVisible = !Mathf.Approximately(potionPercentage, 0f);
                currentPotionGraphic.gameObject.SetActive(currentPotionSetVisible);
            }
        }

        /// <summary>
        /// Sets the health text to the given value.
        /// </summary>
        /// <param name="newHealthValue">The character's new health value.</param>
        private void SetHealthText(float newHealthValue)
        {
            Debug.Assert(newHealthValue >= 0f, newHealthValue);

            Debug.Assert(healthText != null);

            healthText.text = newHealthValue.ToString();
        }

        /// <summary>
        /// Distributes the given total health value through each index of the values array.
        /// </summary>
        private void DistributeHealth(float healthToDistribute)
        {
            Debug.Assert(healthToDistribute >= 0f, healthToDistribute);

            float currentPotionDistribution;

            for(int index = 0; index < potionGraphics.Length; index++) // Iterate through each potion, from the front-most potion to the back-most potion.
            {
                currentPotionDistribution = Mathf.Clamp(healthToDistribute, 0f, HealthPerPotion);

                potionValues[index] = currentPotionDistribution; // Apply the current distribution to the current potion value.
                healthToDistribute -= currentPotionDistribution; // Deduct the current distribution from the total health value.
            }
        }

        /// <summary>
        /// Creates a new hit-score to animate itself.
        /// </summary>
        /// <param name="newHitScore"></param>
        public void CreateHitScore(int newHitScore)
        {
            Debug.Assert(hitScoreText != null);
            Debug.Assert(GO_HitScoreText != null);

            GO_HitScoreText.SetActive(true);

            hitScoreText.text = newHitScore.ToString();

            if (holdHitScoreCoroutine != null) // The coroutine is already running.
            {
                StopCoroutine(holdHitScoreCoroutine);

                holdHitScoreCoroutine = null;
            }

            holdHitScoreCoroutine = StartCoroutine(HoldHitScore());
        }

        /// <summary>
        /// Waits for the set number of seconds, then deactivates the hit score text.
        /// </summary>
        private IEnumerator HoldHitScore()
        {
            Debug.Assert(GO_HitScoreText != null);
            Debug.Assert(holdHitScoreCoroutine == null); // Assert that the coroutine is not already running.

            yield return waitForHitScoreHoldSeconds;

            GO_HitScoreText.SetActive(false);

            holdHitScoreCoroutine = null;
        }
    }
}