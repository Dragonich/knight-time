﻿// AUTHOR: Benjamin Chaves

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Battle
{
    public class TutorialCursorManager : MonoBehaviour
    {
        public GameObject Mouse;
        public GameObject MouseClicked;

        /// <summary>
        /// Cached reference to this GameObject's RectTransform component.
        /// </summary>
        private RectTransform cursorRectTransform;

        public void Initialise()
        {
            AssertInspectorInputs();

            CacheSubordinates();
        }

        /// <summary>
        /// Asserts that all Inspector fields have been set or filled.
        /// </summary>
        private void AssertInspectorInputs()
        {
            Debug.Assert(Mouse != null);

            Debug.Assert(MouseClicked != null);
        }

        private void CacheSubordinates()
        {
            cursorRectTransform = GetComponent<RectTransform>();
            Debug.Assert(cursorRectTransform != null); // Assert that the component was found.
        }

        /// <summary>
        /// Sets the GameObject's active status to the given boolean.
        /// </summary>
        public void SetActive(bool newIsActive)
        {
            gameObject.SetActive(newIsActive);
        }

        /// <summary>
        /// Returns the anchoredPosition of the RectTransform.
        /// </summary>
        public Vector2 GetAnchoredPosition()
        {
            Debug.Assert(cursorRectTransform != null);

            return cursorRectTransform.anchoredPosition;
        }

        /// <summary>
        /// Returns the standard transform position of the object.
        /// </summary>
        public Vector3 GetPosition()
        {
            Debug.Assert(cursorRectTransform != null);

            return cursorRectTransform.position;
        }

        /// <summary>
        /// Sets the anchoredPosition of the RectTransform to the given Vector2.
        /// </summary>
        public void SetPosition(Vector2 newPosition)
        {
            Debug.Assert(cursorRectTransform != null);

            cursorRectTransform.anchoredPosition = newPosition;
        }

        /// <summary>
        /// Sets the RectTransform anchoredPosition to (0, 0).
        /// </summary>
        public void ZeroPosition()
        {
            Debug.Assert(cursorRectTransform != null);

            cursorRectTransform.anchoredPosition = Vector2.zero;
        }

        /// <summary>
        /// Sets the mouse graphics depending on the given boolean.
        /// </summary>
        public void SetMouseClicked(bool newIsClicked)
        {
            Debug.Assert(Mouse != null);
            Debug.Assert(MouseClicked != null);

            Mouse.SetActive(!newIsClicked);
            MouseClicked.SetActive(newIsClicked);
        }
    }
}