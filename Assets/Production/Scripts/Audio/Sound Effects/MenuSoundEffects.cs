﻿// AUTHOR: Benjamin Chaves

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Audio;

public class MenuSoundEffects : MonoBehaviour
{
    [Header("Override Items")]

    [Tooltip("The GameObject which holds the Audio Manager as a component.")]
    public GameObject GO_AudioManager;
    private AudioManager audioManager;

    [Header("Prefab Items")]

    public AudioClip UpgradePanelClip;

    [Tooltip("Cached reference to the Audio Source for playing the menu sound effects.")]
    private AudioSource audioSourceComponent;

    public void Initialise()
    {
        AssertInspectorInputs();

        CacheSubordinates();
    }

    /// <summary>
    /// Assert that all Inspector fields have been set or filled.
    /// </summary>
    private void AssertInspectorInputs()
    {
        Debug.Assert(GO_AudioManager != null);
        Debug.Assert(UpgradePanelClip != null);
    }

    private void CacheSubordinates()
    {
        Debug.Assert(GO_AudioManager != null);

        audioSourceComponent = GetComponent<AudioSource>();
        Debug.Assert(audioSourceComponent != null); // Assert that the component was found.

        audioManager = GO_AudioManager.GetComponent<AudioManager>();
        Debug.Assert(audioManager != null); // Assert that the component was found.
    }

    public void PlayUpgradePanelClip()
    {
        Debug.Assert(audioManager != null);
        Debug.Assert(UpgradePanelClip != null);
        Debug.Assert(audioSourceComponent != null);

        audioManager.StopMusic(); // Stop any music which is playing.

        audioSourceComponent.clip = UpgradePanelClip;
        audioSourceComponent.Play();
    }
}