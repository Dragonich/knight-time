﻿// AUTHOR: Benjamin Chaves

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Pause;

namespace Battle
{
    public class DrainActionManager : MonoBehaviour
    {
        private UIManager uiManager;
        private Camera cameraComponent;
        private TutorialsManager tutorialsManager;
        private MouseActionsManager mouseActionsManager;

        /// <summary>
        /// Whether the Drain action is available to be used by the user.
        /// </summary>
        private bool drainAvailable;

        /// <summary>
        /// Whether the user has initiated the Drain action.
        /// </summary>
        private bool drainInitiated;

        public void Initialise(MouseActionsManager inputMouseActionsManager, Camera inputCameraComponent, UIManager inputUIManager, TutorialsManager inputTutorialsNumber)
        {
            Debug.Assert(inputMouseActionsManager != null);
            Debug.Assert(inputCameraComponent != null);
            Debug.Assert(inputUIManager != null);
            Debug.Assert(inputTutorialsNumber != null);

            uiManager = inputUIManager;
            cameraComponent = inputCameraComponent;
            tutorialsManager = inputTutorialsNumber;
            mouseActionsManager = inputMouseActionsManager;
        }

        /// <summary>
        /// Sets the action to be available to be used by the user.
        /// </summary>
        public void ActivateAction()
        {
            drainAvailable = true;
            drainInitiated = false;
        }

        /// <summary>
        /// Sets the action to not be available to be used by the user,
        /// and resets the tints on all characters.
        /// </summary>
        public void CancelAction()
        {
            Debug.Assert(mouseActionsManager != null);

            drainAvailable = false;

            mouseActionsManager.SetEnemyTint(CharacterTinter.CharacterTints.None);
            mouseActionsManager.SetPlayerTint(CharacterTinter.CharacterTints.None);
        }

        private void Update()
        {
            Debug.Assert(uiManager != null);
            Debug.Assert(tutorialsManager != null);
            Debug.Assert(cameraComponent != null);
            Debug.Assert(mouseActionsManager != null);

            if(drainAvailable && !PauseMenu.isPaused) // The Drain action is available, and the game is not paused.
            {
                bool mouseOverEnemy = false;
                bool mouseOverPlayer = false;

                Ray hitRay = cameraComponent.ScreenPointToRay(Input.mousePosition);

                if (Physics.Raycast(hitRay, out RaycastHit rayHit))
                {
                    mouseOverEnemy = rayHit.collider.name == BattleCharactersManager.EnemyName;
                    mouseOverPlayer = rayHit.collider.name == BattleCharactersManager.CharacterName;
                }

                if (Input.GetMouseButtonDown(0)) // The user has pressed the left-mouse button this frame.
                {
                    if(!uiManager.GetPointerOverAnyHandCard() && !tutorialsManager.MouseIsOverTutorialPrompt())
                    {
                        if (mouseOverEnemy)
                        {
                            drainInitiated = true;
                            mouseActionsManager.TriggerActionInitiated();

                            mouseActionsManager.LockPlayerCard();
                        }
                        else
                        {
                            ResolveAction(false);
                        }
                    }
                }
                else if(Input.GetMouseButtonUp(0)) // The user has not pressed the left-mouse button this frame, and has released the left-mouse button this frame.
                {
                    if(drainInitiated)
                    {
                        ResolveAction(mouseOverPlayer);
                    }
                }
                else // The user has not pressed the left-mouse button this frame, and has not released the left-mouse button this frame.
                {
                    if(mouseOverPlayer && drainInitiated) // The mouse is over the player, and the drain has been initiated.
                    {
                        mouseActionsManager.SetPlayerTint(CharacterTinter.CharacterTints.Blue);
                    }
                    else // The mouse is not over the player, or the drain has not been initiated.
                    {
                        mouseActionsManager.SetPlayerTint(CharacterTinter.CharacterTints.None);
                    }

                    if(mouseOverEnemy && !drainInitiated) // The mouse is over the player, and the drain has not been initiated.
                    {
                        mouseActionsManager.SetEnemyTint(CharacterTinter.CharacterTints.Red);
                    }
                    else // The mouse is not over the enemy, or the drain has been initiated.
                    {
                        mouseActionsManager.SetEnemyTint(CharacterTinter.CharacterTints.None);
                    }
                }
            }
        }

        /// <summary>
        /// Finishes the action with the given success result.
        /// </summary>
        /// <param name="actionSuccessful">Whether the user completed the action successfully.</param>
        private void ResolveAction(bool actionSuccessful)
        {
            Debug.Assert(mouseActionsManager != null);

            drainAvailable = false;
            mouseActionsManager.SetPlayerTint(CharacterTinter.CharacterTints.None);
            mouseActionsManager.SetEnemyTint(CharacterTinter.CharacterTints.None);

            mouseActionsManager.TriggerActionResolved(MouseActionsManager.Symbols.Drain, actionSuccessful);
        }
    }
}