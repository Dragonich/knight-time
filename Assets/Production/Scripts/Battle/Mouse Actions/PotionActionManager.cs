﻿// AUTHOR: Benjamin Chaves

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Pause;

namespace Battle
{
    public class PotionActionManager : MonoBehaviour
    {
        private UIManager uiManager;
        private Camera cameraComponent;
        private TutorialsManager tutorialsManager;
        private MouseActionsManager mouseActionsManager;

        /// <summary>
        /// Whether the Potion action is available to be used by the user.
        /// </summary>
        private bool potionAvailable;

        public void Initialise(MouseActionsManager inputMouseActionsManager, Camera inputCameraComponent, UIManager inputUIManager, TutorialsManager inputTutorialsManager)
        {
            Debug.Assert(inputMouseActionsManager != null);
            Debug.Assert(inputCameraComponent != null);
            Debug.Assert(inputUIManager != null);
            Debug.Assert(inputTutorialsManager != null);

            mouseActionsManager = inputMouseActionsManager;
            cameraComponent = inputCameraComponent;
            uiManager = inputUIManager;
            tutorialsManager = inputTutorialsManager;
        }

        /// <summary>
        /// Sets the action to be available to be used by the user.
        /// </summary>
        public void ActivateAction()
        {
            potionAvailable = true;
        }

        /// <summary>
        /// Sets the action to not be available to be used by the user,
        /// and resets the tint on the player character.
        /// </summary>
        public void CancelAction()
        {
            Debug.Assert(mouseActionsManager != null);

            potionAvailable = false;

            mouseActionsManager.SetPlayerTint(CharacterTinter.CharacterTints.None);
        }

        private void Update()
        {
            Debug.Assert(cameraComponent != null);
            Debug.Assert(mouseActionsManager != null);

            if(potionAvailable && !PauseMenu.isPaused) // The Potion action is available, and the game is not paused.
            {
                bool mouseIsOverCharacter = false;

                Ray hitRay = cameraComponent.ScreenPointToRay(Input.mousePosition);

                if (Physics.Raycast(hitRay, out RaycastHit rayHit))
                {
                    mouseIsOverCharacter = rayHit.collider.name == BattleCharactersManager.CharacterName;
                }

                if (Input.GetMouseButtonDown(0)) // The user has pressed left-click this frame.
                {
                    if(!uiManager.GetPointerOverAnyHandCard() && !tutorialsManager.MouseIsOverTutorialPrompt()) // The mouse is not over any hand card or any tutorial prompt.
                    {
                        mouseActionsManager.LockPlayerCard();

                        potionAvailable = false;

                        mouseActionsManager.SetPlayerTint(CharacterTinter.CharacterTints.None);

                        mouseActionsManager.TriggerActionResolved(MouseActionsManager.Symbols.Potion, mouseIsOverCharacter);
                    }
                }
                else // The user has not pressed left-click this frame.
                {
                    if (mouseIsOverCharacter)
                    {
                        mouseActionsManager.SetPlayerTint(CharacterTinter.CharacterTints.Blue); // Set the character to be highlighted.
                    }
                    else
                    {
                        mouseActionsManager.SetPlayerTint(CharacterTinter.CharacterTints.None); // Set the character to not be highlighted.
                    }
                }
            }
        }
    }
}