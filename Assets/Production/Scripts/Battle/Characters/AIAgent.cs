﻿// AUTHOR: Benjamin Chaves

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Battle
{
    public class AIAgent
    {
        private CardsManager cardsManager;
        private BattleCharacter battleCharacter;
        private CharactersManager charactersManager;

        public AIAgent(BattleCharacter inputBattleCharacter, CardsManager inputCardsManager, CharactersManager inputCharactersManager)
        {
            Debug.Assert(inputCardsManager != null);
            Debug.Assert(inputBattleCharacter != null);
            Debug.Assert(inputCharactersManager != null);

            cardsManager = inputCardsManager;
            battleCharacter = inputBattleCharacter;
            charactersManager = inputCharactersManager;
        }

        /// <summary>
        /// Triggers this AI to select & play a card to the table.
        /// </summary>
        public void PlayCardToTable()
        {
            Debug.Assert(cardsManager != null);
            Debug.Assert(battleCharacter != null);

            int cardToTableID = -1;

            float healthPercentage = battleCharacter.GetHealthPercentage();

            if(healthPercentage > 0.5f || GetPrioritiseAttack()) // This character's health is above 50%, or the character intends to prioritise an offensive move.
            {
                cardToTableID = (int)SelectOffensiveCard();
            }
            else
            {
                cardToTableID = (int)SelectDefensiveCard();
            }

            Debug.Assert(cardToTableID >= 0, cardToTableID); // Assert that a card was successfully chosen.

            battleCharacter.DebitCardFromSet(cardToTableID); // Debit the chosen card from its set.

            Card cardToTable = cardsManager.GetDatabaseCardByID(cardToTableID); // Get the Card version of the chosen card.
            Debug.Assert(cardToTable != null);

            cardsManager.PlayCardToTable(battleCharacter.CharacterID, cardToTable, 0); // Play the chosen card to the table.
        }

        /// <summary>
        /// Returns whether this character can defeat the player within this turn,
        /// and should therefore prioritise an offensive card.
        /// </summary>
        private bool GetPrioritiseAttack()
        {
            Debug.Assert(battleCharacter != null);
            Debug.Assert(charactersManager != null);

            float playerHealth = charactersManager.GetCharacterHealth(0);
            int damageRangeLower = battleCharacter.GetCharacterDamageRangeLower();

            return playerHealth <= damageRangeLower;
        }

        /// <summary>
        /// Returns the most-offensive available card.
        /// </summary>
        private CardsDatabase.CardTypes SelectOffensiveCard()
        {
            if(CanPlayCard(CardsDatabase.CardTypes.DragonAttack))
            {
                return CardsDatabase.CardTypes.DragonAttack;
            }
            else if(CanPlayCard(CardsDatabase.CardTypes.DoubleAttack))
            {
                return CardsDatabase.CardTypes.DoubleAttack;
            }
            else if(CanPlayCard(CardsDatabase.CardTypes.FastAttack))
            {
                return CardsDatabase.CardTypes.FastAttack;
            }
            else if(CanPlayCard(CardsDatabase.CardTypes.NormalAttack))
            {
                return CardsDatabase.CardTypes.NormalAttack;
            }
            else if(CanPlayCard(CardsDatabase.CardTypes.DrainAttack))
            {
                return CardsDatabase.CardTypes.DrainAttack;
            }
            else if(CanPlayCard(CardsDatabase.CardTypes.Potion))
            {
                return CardsDatabase.CardTypes.Potion;
            }
            else
            {
                Debug.LogError("Failed to select offensive card.");
                return CardsDatabase.CardTypes.NormalAttack;
            }
        }

        /// <summary>
        /// Returns the most-defensive available card.
        /// </summary>
        private CardsDatabase.CardTypes SelectDefensiveCard()
        {
            if(CanPlayCard(CardsDatabase.CardTypes.DragonAttack))
            {
                return CardsDatabase.CardTypes.DragonAttack;
            }
            else if(CanPlayCard(CardsDatabase.CardTypes.Potion))
            {
                return CardsDatabase.CardTypes.Potion;
            }
            else if(CanPlayCard(CardsDatabase.CardTypes.DrainAttack))
            {
                return CardsDatabase.CardTypes.DrainAttack;
            }
            else if(CanPlayCard(CardsDatabase.CardTypes.NormalAttack))
            {
                return CardsDatabase.CardTypes.NormalAttack;
            }
            else if(CanPlayCard(CardsDatabase.CardTypes.FastAttack))
            {
                return CardsDatabase.CardTypes.FastAttack;
            }
            else if(CanPlayCard(CardsDatabase.CardTypes.DoubleAttack))
            {
                return CardsDatabase.CardTypes.DoubleAttack;
            }
            else
            {
                Debug.LogError("Failed to select defensive card.");
                return CardsDatabase.CardTypes.NormalAttack;
            }
        }

        /// <summary>
        /// Returns whether the character can play the given card.
        /// </summary>
        /// <param name="cardToCheck">The card whose availability should be checked.</param>
        private bool CanPlayCard(CardsDatabase.CardTypes cardToCheck)
        {
            Debug.Assert(battleCharacter != null);

            return battleCharacter.GetHasCardsAvailable((int)cardToCheck);
        }
    }
}