﻿// AUTHOR: Benjamin Chaves

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Battle
{
    public class CameraAnimationsManager : MonoBehaviour
    {
        public GameObject Camera;

        [Tooltip("The speed with which the camera should animate to the target position.")]
        public float StrikeAnimationSpeed;

        [Tooltip("The value by which the normalised strike vector should be multiplied.")]
        public float StrikeVectorMultiplier;

        private Vector3 cameraInitialPosition;
        private Vector3 cameraTargetPosition = Vector3.zero;

        /// <summary>
        /// The current state of the strike animation.
        /// </summary>
        private StrikeAnimationStates strikeAnimationState;

        /// <summary>
        /// The states in which the strike animation can be, including None.
        /// </summary>
        private enum StrikeAnimationStates
        {
            /// <summary>
            /// The state where the camera is not being animated.
            /// </summary>
            None,

            /// <summary>
            /// The state where the camera is animating towards its target position.
            /// </summary>
            AnimatingToTarget,

            /// <summary>
            /// The state where the camera is animating towards its default position.
            /// </summary>
            AnimatingToInitial,
        }
        
        public void Initialise()
        {
            AssertInspectorInputs();

            CacheSubordinates();

            cameraInitialPosition = Camera.transform.position;
            cameraTargetPosition.z = cameraInitialPosition.z;
        }

        /// <summary>
        /// Assert that all Inspector fields have been set or filled.
        /// </summary>
        private void AssertInspectorInputs()
        {
            Debug.Assert(Camera != null);

            Debug.Assert(StrikeAnimationSpeed > 0f, StrikeAnimationSpeed);
            Debug.Assert(StrikeVectorMultiplier > 0f, StrikeVectorMultiplier);
        }

        private void CacheSubordinates()
        {
            Debug.Assert(Camera != null);
        }

        /// <summary>
        /// Triggers the camera to animate to a position target based on the given strike vector.
        /// </summary>
        /// <param name="strikeVector">The Vector2 which represents the direction in which the strike has happened.</param>
        public void PushCamera(Vector2 strikeVector)
        {
            Vector2 strikeVectorNormalised = strikeVector.normalized;

            cameraTargetPosition.x = cameraInitialPosition.x + (strikeVectorNormalised.x * StrikeVectorMultiplier);
            cameraTargetPosition.y = cameraInitialPosition.y + (strikeVectorNormalised.y * StrikeVectorMultiplier);

            strikeAnimationState = StrikeAnimationStates.AnimatingToTarget;
        }

        private void Update()
        {
            if(strikeAnimationState != StrikeAnimationStates.None) // The camera is animating due to the user making a strike.
            {
                AnimateStrike();
            }
        }

        /// <summary>
        /// Animates the camera as per its animation state.
        /// </summary>
        private void AnimateStrike()
        {
            Debug.Assert(Camera != null);

            if (strikeAnimationState == StrikeAnimationStates.AnimatingToTarget)
            {
                Camera.transform.position = Vector3.MoveTowards(Camera.transform.position, cameraTargetPosition, StrikeAnimationSpeed * Time.deltaTime); // Move camera towards target position.

                if (VectorsApproximatelyEqual(Camera.transform.position, cameraTargetPosition)) // The camera has reached the target position.
                {
                    strikeAnimationState = StrikeAnimationStates.AnimatingToInitial;
                }
            }
            else if (strikeAnimationState == StrikeAnimationStates.AnimatingToInitial)
            {
                Camera.transform.position = Vector3.MoveTowards(Camera.transform.position, cameraInitialPosition, StrikeAnimationSpeed * Time.deltaTime); // Move camera towards initial position.

                if (VectorsApproximatelyEqual(Camera.transform.position, cameraInitialPosition)) // The camera has reached its initial position.
                {
                    strikeAnimationState = StrikeAnimationStates.None;

                    Camera.transform.position = cameraInitialPosition; // Set the camera to be at its initial position.
                }
            }
        }

        /// <summary>
        /// Returns whether each float component of the given Vector3's are approximately equal.
        /// </summary>
        private bool VectorsApproximatelyEqual(Vector3 a, Vector3 b)
        {
            return Mathf.Approximately(a.x, b.x) && Mathf.Approximately(a.y, b.y);
        }
    }
}