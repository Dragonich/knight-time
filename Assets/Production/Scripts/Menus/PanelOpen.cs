﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
// Author: Luke Saliba
namespace Panel
{
    public class PanelOpen : MonoBehaviour
    {
        public GameObject panelOpen;
        static public bool isPanelOpen;
        public float Delay;
        // Start is called before the first frame update
        void Start()
        {
            panelOpen.SetActive(false);
        }

        // Update is called once per frame
        void Update()
        {

        }
        public void OpenPanel()
        {

            panelOpen.SetActive(true);
            StartCoroutine(ButtonDelay());

        }
        private IEnumerator ButtonDelay()
        {
            yield return new WaitForSeconds(Delay);

            panelOpen.SetActive(false);
        }
    }
}
