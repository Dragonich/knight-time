﻿// AUTHOR: Benjamin Chaves

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Battle
{
    public class CharacterTinter
    {
        private List<SpriteRenderer> spriteRenderers = new List<SpriteRenderer>();

        private bool damageTintActive;

        private static float tintHoldTimerMax = 0.3f;
        private float tintHoldTimer = tintHoldTimerMax;

        private CharacterTints currentTint;

        /// <summary>
        /// The tints available to be applied to characters.
        /// </summary>
        public enum CharacterTints
        {
            None,
            Red,
            Blue,
        }

        private static Color tintNone = Color.white;
        private static Color tintRed = Color.Lerp(Color.red, Color.white, 0.67f);
        private static Color tintBlue = Color.Lerp(Color.blue, Color.white, 0.9f);

        public CharacterTinter(ref GameObject[] inputCharacterObjects)
        {
            Debug.Assert(inputCharacterObjects != null);

            CacheSpriteRenderers(ref inputCharacterObjects);
        }

        /// <summary>
        /// Caches any Sprite Renderer components found on each
        /// character object, and each child of each character object.
        /// 
        /// The purpose of including multiple character objects is to
        /// account for other character forms, such as being Duckified.
        /// </summary>
        /// <param name="inputCharacterObjects">Referenced array of character objects to be searched through.</param>
        private void CacheSpriteRenderers(ref GameObject[] inputCharacterObjects)
        {
            for (int characterIndex = 0; characterIndex < inputCharacterObjects.Length; characterIndex++) // Iterate through each character object.
            {
                GameObject currentCharacterObject = inputCharacterObjects[characterIndex];
                Debug.Assert(currentCharacterObject != null);

                for (int childIndex = 0; childIndex < currentCharacterObject.transform.childCount; childIndex++) // Iterate through each child of the current character object.
                {
                    Transform currentChild = currentCharacterObject.transform.GetChild(childIndex);
                    Debug.Assert(currentChild != null);

                    SpriteRenderer currentChildSpriteRenderer = currentChild.GetComponent<SpriteRenderer>();

                    if (currentChildSpriteRenderer != null) // A Sprite Renderer component was found on the current child object.
                    {
                        spriteRenderers.Add(currentChildSpriteRenderer); // Add this Sprite Renderer to the list of Sprite Renderers.
                    }
                }

                SpriteRenderer currentParentSpriteRenderer = currentCharacterObject.GetComponent<SpriteRenderer>();

                if(currentParentSpriteRenderer != null) // A Sprite Renderer component was found on the current character object.
                {
                    spriteRenderers.Add(currentParentSpriteRenderer);
                }
            }
        }

        /// <summary>
        /// Applies the given tint enum to the character.
        /// </summary>
        /// <param name="newCharacterTint">The tint which should be applied.</param>
        public void SetCharacterTint(CharacterTints newCharacterTint)
        {
            if(currentTint != newCharacterTint) // The tint to apply is not already the current tint.
            {
                currentTint = newCharacterTint;

                Color colorToTint = tintNone;

                switch (newCharacterTint)
                {
                    case CharacterTints.None:

                        colorToTint = tintNone;
                        break;

                    case CharacterTints.Red:

                        colorToTint = tintRed;
                        break;

                    case CharacterTints.Blue:

                        colorToTint = tintBlue;
                        break;

                    default:

                        Debug.LogError(newCharacterTint);
                        break;
                }

                foreach (SpriteRenderer currentSpriteRenderer in spriteRenderers)
                {
                    Debug.Assert(currentSpriteRenderer != null);

                    currentSpriteRenderer.color = colorToTint;
                }
            }
        }

        /// <summary>
        /// Activates the damage tint on the character.
        /// </summary>
        public void ActivateDamageTint()
        {
            SetCharacterTint(CharacterTints.Red);

            damageTintActive = true;
        }

        /// <summary>
        /// Advances the damage tint timer by the given delta-time value.
        /// </summary>
        public void AdvanceDamageTintTimer(float deltaTime)
        {
            if(damageTintActive)
            {
                tintHoldTimer -= deltaTime;

                if(tintHoldTimer < 0f) // The tint hold timer has expired.
                {
                    damageTintActive = false;
                    tintHoldTimer = tintHoldTimerMax; // Reset the tint hold timer.

                    SetCharacterTint(CharacterTints.None); // Reset the character's tint to none.
                }
            }
        }
    }
}