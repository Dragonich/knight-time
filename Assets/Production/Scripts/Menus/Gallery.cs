﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
// Author: Luke Saliba
namespace Gallery
{
    public class Gallery : MonoBehaviour
    {
        public GameObject flavoPanelOpen;
        public GameObject snailderPanelOpen;
        public GameObject johnPanelOpen;
        public GameObject bebekPanelOpen;
        public GameObject extraPanelOpen;
        static public bool isFlavoPanelOpen;
        static public bool isSnailderPanelOpen;
        static public bool isJohnPanelOpen;
        static public bool isBebekPanelOpen;
        static public bool isExtraPanelOpen;






        // Start is called before the first frame update
        void Start()
        {
            flavoPanelOpen.SetActive(false);
            snailderPanelOpen.SetActive(false);
            johnPanelOpen.SetActive(false);
            bebekPanelOpen.SetActive(false);
            extraPanelOpen.SetActive(false);
        }

        // Update is called once per frame
        void Update()
        {

        }
        public void openPanelFlavo()
        {
            flavoPanelOpen.SetActive(true);
            snailderPanelOpen.SetActive(false);
            johnPanelOpen.SetActive(false);
            bebekPanelOpen.SetActive(false);
            extraPanelOpen.SetActive(false);
        }
        public void openPanelSnailder()
        {
            flavoPanelOpen.SetActive(false);
            snailderPanelOpen.SetActive(true);
            johnPanelOpen.SetActive(false);
            bebekPanelOpen.SetActive(false);
            extraPanelOpen.SetActive(false);
        }
        public void openPanelJohn()
        {
            flavoPanelOpen.SetActive(false);
            snailderPanelOpen.SetActive(false);
            johnPanelOpen.SetActive(true);
            bebekPanelOpen.SetActive(false);
            extraPanelOpen.SetActive(false);
        }
        public void openPanelBebek()
        {
            flavoPanelOpen.SetActive(false);
            snailderPanelOpen.SetActive(false);
            johnPanelOpen.SetActive(false);
            bebekPanelOpen.SetActive(true);
            extraPanelOpen.SetActive(false);
        }
        public void openPanelExtra()
        {
            flavoPanelOpen.SetActive(false);
            snailderPanelOpen.SetActive(false);
            johnPanelOpen.SetActive(false);
            bebekPanelOpen.SetActive(false);
            extraPanelOpen.SetActive(true);
        }

    }
}