﻿// AUTHOR: Benjamin Chaves

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Battle
{
    public class CardSelectionTutorial : MonoBehaviour
    {
        private UIManager uiManager;
        private TutorialsManager tutorialsManager;
        private TutorialCursorManager tutorialCursorManager;

        /// <summary>
        /// Whether this tutorial is active.
        /// </summary>
        public bool TutorialActive { get; private set; }

        /// <summary>
        /// A Vector2 reserved for assigning a new position to the tutorial cursor.
        /// </summary>
        private Vector2 cursorPositionSetter;

        /// <summary>
        /// A Vector2 for the target position to which the tutorial cursor should move.
        /// </summary>
        private Vector2 cursorPositionTarget;

        private WaitForEndOfFrame waitForEndOfFrame;
        private WaitForSeconds waitForTutorialSeconds;
        private WaitForSeconds waitForTutorialSecondsThird;

        private Coroutine holdOnScreenCentreCoroutine;
        private Coroutine moveToCardCoroutine;
        private Coroutine holdOnCardCoroutine;
        private Coroutine holdInvisibleCoroutine;

        public void Initialise(TutorialsManager inputTutorialsManager, GameObject inputTutorialCursorPrefab, GameObject inputTutorialCursorParent, UIManager inputUIManager)
        {
            Debug.Assert(inputUIManager != null);
            Debug.Assert(inputTutorialsManager != null);
            Debug.Assert(inputTutorialCursorPrefab != null);
            Debug.Assert(inputTutorialCursorParent != null);

            uiManager = inputUIManager;
            tutorialsManager = inputTutorialsManager;

            InstantiateTutorialCursor(inputTutorialCursorPrefab, inputTutorialCursorParent);

            waitForEndOfFrame = new WaitForEndOfFrame();
            waitForTutorialSeconds = new WaitForSeconds(tutorialsManager.CursorHoldSeconds);
            waitForTutorialSecondsThird = new WaitForSeconds(tutorialsManager.CursorHoldSeconds / 3f);
        }

        /// <summary>
        /// Instantiates the tutorial cursor for this tutorial.
        /// </summary>
        /// <param name="inputTutorialCursorPrefab">The prefab of the tutorial cursor GameObject to be instantiated.</param>
        /// <param name="inputTutorialCursorsParent">The GameObject which should be the parent of the instantiated tutorial cursor.</param>
        private void InstantiateTutorialCursor(GameObject inputTutorialCursorPrefab, GameObject inputTutorialCursorsParent)
        {
            Debug.Assert(inputTutorialCursorPrefab != null);
            Debug.Assert(inputTutorialCursorsParent != null);

            GameObject newTutorialCursor = Instantiate(inputTutorialCursorPrefab);

            newTutorialCursor.SetActive(false);
            newTutorialCursor.name = "Card Selection Tutorial Cursor";
            newTutorialCursor.transform.SetParent(inputTutorialCursorsParent.transform);

            tutorialCursorManager = newTutorialCursor.GetComponent<TutorialCursorManager>();
            Debug.Assert(tutorialCursorManager != null); // Assert that the component was found.

            tutorialCursorManager.Initialise();
        }

        /// <summary>
        /// Starts this tutorial.
        /// </summary>
        public void StartTutorial()
        {
            Debug.Assert(tutorialCursorManager != null);
            Debug.Assert(!TutorialActive); // Assert that the tutorial is not already active.

            TutorialActive = true;
            tutorialCursorManager.SetMouseClicked(false);

            holdOnScreenCentreCoroutine = StartCoroutine(HoldOnScreenCentre());
        }

        /// <summary>
        /// Ends this tutorial.
        /// </summary>
        public void EndTutorial()
        {
            Debug.Assert(tutorialCursorManager != null);
            Debug.Assert(TutorialActive); // Assert that the tutorial is currently active.

            TutorialActive = false;

            StopAllTutorialCoroutines();

            tutorialCursorManager.SetActive(false);
        }

        /// <summary>
        /// Stops all coroutines in this tutorial.
        /// </summary>
        private void StopAllTutorialCoroutines()
        {
            StopTutorialCoroutine(ref holdOnScreenCentreCoroutine);

            StopTutorialCoroutine(ref moveToCardCoroutine);
            StopTutorialCoroutine(ref holdOnCardCoroutine);

            StopTutorialCoroutine(ref holdInvisibleCoroutine);
        }

        /// <summary>
        /// Stops the given coroutine, and sets the variable to null.
        /// </summary>
        /// <param name="coroutineToStop"></param>
        private void StopTutorialCoroutine(ref Coroutine coroutineToStop)
        {
            if (coroutineToStop != null)
            {
                StopCoroutine(coroutineToStop);

                coroutineToStop = null;
            }
        }

        /// <summary>
        /// Activates & holds the tutorial cursor on the screen centre,
        /// then activates the MoveToCard coroutine.
        /// </summary>
        private IEnumerator HoldOnScreenCentre()
        {
            Debug.Assert(tutorialCursorManager != null);

            tutorialCursorManager.ZeroPosition();
            tutorialCursorManager.SetActive(true);

            yield return waitForTutorialSeconds;

            moveToCardCoroutine = StartCoroutine(MoveToCard());
            holdOnScreenCentreCoroutine = null;
        }

        /// <summary>
        /// Moves the tutorial cursor to the first hand-card,
        /// then activates the HoldOnCard coroutine.
        /// </summary>
        private IEnumerator MoveToCard()
        {
            Debug.Assert(uiManager != null);
            Debug.Assert(tutorialsManager != null);
            Debug.Assert(tutorialCursorManager != null);

            cursorPositionTarget = uiManager.GetHandCardPosition(0);
            cursorPositionTarget.x += 50f;
            cursorPositionTarget.y += 50f;

            while (!TutorialsManager.VectorsApproximatelyEqual(tutorialCursorManager.GetAnchoredPosition(), cursorPositionTarget)) // The cursor has not reached its target.
            {
                float movementSpeed = tutorialsManager.CursorMovementSpeed * Time.deltaTime;
                cursorPositionSetter = Vector2.MoveTowards(tutorialCursorManager.GetAnchoredPosition(), cursorPositionTarget, movementSpeed); // Move the cursor towards its target.

                tutorialCursorManager.SetPosition(cursorPositionSetter);

                yield return waitForEndOfFrame;
            }

            holdOnCardCoroutine = StartCoroutine(HoldOnCard());
            moveToCardCoroutine = null;
        }

        /// <summary>
        /// Holds the tutorial cursor, then activates the HoldInvisible coroutine.
        /// </summary>
        private IEnumerator HoldOnCard()
        {
            Debug.Assert(tutorialCursorManager != null);

            yield return waitForTutorialSecondsThird;

            tutorialCursorManager.SetMouseClicked(true);

            yield return waitForTutorialSecondsThird;

            tutorialCursorManager.SetMouseClicked(false);

            yield return waitForTutorialSecondsThird;

            holdInvisibleCoroutine = StartCoroutine(HoldInvisible());
            holdOnCardCoroutine = null;
        }

        /// <summary>
        /// Sets the tutorial cursor to inactive and holds,
        /// then activates the HoldOnScreenCentre coroutine.
        /// </summary>
        private IEnumerator HoldInvisible()
        {
            Debug.Assert(tutorialCursorManager != null);

            tutorialCursorManager.SetActive(false);

            yield return waitForTutorialSeconds;

            holdOnScreenCentreCoroutine = StartCoroutine(HoldOnScreenCentre());
            holdInvisibleCoroutine = null;
        }
    }
}