﻿// Author: Luke Saliba
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using UnityEngine.Audio;
using TMPro;

namespace Settings
{
    public class SettingsMenu : MonoBehaviour
    {
        public float variable;
        public GameObject settingsMenu;
        public float globalVolume;


        void Start()
        {
            settingsMenu.SetActive(false);
        }

        public void SetQuality(int index)
        {
            QualitySettings.SetQualityLevel(index);
        }
        public void Fullscreen(bool isFullscreeen)
        {
            Screen.fullScreen = isFullscreeen;
        }
        public void OpenSettings()
        {
            settingsMenu.SetActive(true);
        }
        public void CloseSettings()
        {
            settingsMenu.SetActive(false);
        }
    }
}
