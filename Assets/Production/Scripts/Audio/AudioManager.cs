﻿// Author: Luke Saliba
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;
using UserPreferences;

namespace Audio
{
    public class AudioManager : MonoBehaviour
    {
        public bool BlockUserPreferences;

        public GameObject GO_UserPreferencesManager;
        private UserPreferencesFileManager userPreferencesManager;

        public GameObject GO_VolumeSlider;
        private Slider sliderComponent;

        [Tooltip("Put audioSource here")]
        public AudioSource _audioSource;
        [Tooltip("Put audiomixer here")]
        public AudioMixer MasterVolume;

        public float SliderValue;
        private float musVol = 0.5f;
        static bool AudioPlaying = false;
        void Start()
        {
            _audioSource = GetComponent<AudioSource>();

            if (AudioPlaying == false)
            {
                PlayMusic();
                AudioPlaying = true;
            }

            AlignAudio();

            if (!BlockUserPreferences)
            {
                userPreferencesManager = GO_UserPreferencesManager.GetComponent<UserPreferencesFileManager>();
                Debug.Assert(userPreferencesManager != null);

                sliderComponent = GO_VolumeSlider.GetComponent<Slider>();
                Debug.Assert(sliderComponent != null);

                LoadSliderValue();
            }
        }
        void Update()
        {
            _audioSource.volume = musVol;
            
        }
        public void volumeSet(float vol)
        {
            MasterVolume.SetFloat("MasterVolume", Mathf.Log10(vol) * 20);
        }
        public float AlignAudio()
        {
            bool result = MasterVolume.GetFloat("MasterVolume", out SliderValue);
            if (result)
            {
                return SliderValue;
            }
            else
            {
                return 0.5f;
            }
        }
        private void Awake()
        {
            // grabs audio source
            _audioSource = GetComponent<AudioSource>();
        }

        public void PlayMusic()
        {
            if (_audioSource.isPlaying) return;
            _audioSource.Play();
        }
        public void PlayMenuMusic()
        {
            if (AudioPlaying == false)
            {
                if (_audioSource.isPlaying) return;
                _audioSource.Play();
            }
        }
        //pauses Audio
        public void PauseMusic()
        {
            _audioSource.Pause();
        }
        //stops Audio
        public void StopMusic()
        {
            _audioSource.Stop();
        }
        // plays secondary audiosource
        public void PlayLossMusic()
        {
            StopMusic();
            AudioSource[] audioSources = GetComponents<AudioSource>();
            
            if (audioSources[1].isPlaying) return;
            audioSources[1].Play();
        }

        /// <summary>
        /// Loads the volume value from disk, and applies it to the volume slider.
        /// </summary>
        public void LoadSliderValue()
        {
            Debug.Assert(!BlockUserPreferences); // Assert that user preferences are not blocked on this script.

            Debug.Assert(userPreferencesManager != null);
            Debug.Assert(sliderComponent != null);

            UserPreferencesFileLoadWrapper loadWrapper = userPreferencesManager.LoadUserPreferencesFile();

            if(loadWrapper.LoadResult == UserPreferencesFileLoadWrapper.LoadResults.Successful)
            {
                sliderComponent.value = loadWrapper.UserPreferenceFileData.MusicVolume;
            }
        }

        /// <summary>
        /// Saves the volume slider value to disk.
        /// </summary>
        public void SaveSliderValue()
        {
            Debug.Assert(!BlockUserPreferences); // Assert that user preferences are not blocked on this script.

            Debug.Assert(userPreferencesManager != null);
            Debug.Assert(sliderComponent != null);

            userPreferencesManager.SaveUserPreferences(sliderComponent.value);
        }
    }
}