﻿// AUTHOR: Benjamin Chaves

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Battle
{
    public class ParticleEffectsManager : MonoBehaviour
    {
        [Tooltip("The GameObject which has the fireworks particle system as a component.")]
        public GameObject FireworksPrefab;

        private ParticleSystem fireworksParticleSystem;

        public void Initialise(uint battleIndex)
        {
            Debug.Assert(battleIndex >= 1);

            AssertInspectorInputs();

            if(battleIndex == 3)
            {
                CreateFireworksSystem();
            }
        }

        private void AssertInspectorInputs()
        {
            Debug.Assert(FireworksPrefab != null);
        }

        /// <summary>
        /// Instantiates an object from the Fireworks Prefab.
        /// </summary>
        private void CreateFireworksSystem()
        {
            Debug.Assert(FireworksPrefab != null);

            GameObject fireworksEffect = Instantiate(FireworksPrefab);

            fireworksEffect.transform.position = Vector3.zero;

            fireworksParticleSystem = fireworksEffect.GetComponent<ParticleSystem>();
            Debug.Assert(fireworksParticleSystem != null); // Assert that the component was found.
        }

        /// <summary>
        /// Sets the fireworks particle system to play.
        /// </summary>
        public void PlayFireworksEffect()
        {
            Debug.Assert(fireworksParticleSystem != null);

            fireworksParticleSystem.Play();
        }
    }
}