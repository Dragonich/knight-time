﻿// AUTHOR: Benjamin Chaves

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Battle
{
    public class BattleManifest : MonoBehaviour
    {
        [Tooltip("The index of this battle, where the first battle is 1.")]
        public uint BattleIndex;

        [Tooltip("The additional max HP offerred to the player upon victory in this battle.")]
        public uint MaxHPUpgrade;

        [Tooltip("The Skill Card offerred to the player upon victory in this battle.")]
        public CardsDatabase.CardTypes CardToSteal;

        [Tooltip("The number of initial attack cards which each character should have.")]
        public uint InitialAttackCards;

        [Tooltip("The enemy / enemies which the player will fight in this battle.")]
        public CharactersDatabase.Characters[] Enemies;

        public void Initialise()
        {
            AssertInspectorInputs();
        }

        /// <summary>
        /// Assert that all Inspector fields have been set or filled.
        /// </summary>
        private void AssertInspectorInputs()
        {
            Debug.Assert(BattleIndex > 0);
            Debug.Assert(BattleIndex <= 3);

            Debug.Assert(MaxHPUpgrade > 0);
            Debug.Assert(CardToSteal != CardsDatabase.CardTypes.NormalAttack);

            Debug.Assert(Enemies.Length == 1);

            for(int index = 0; index < Enemies.Length; index++)
            {
                Debug.Assert(Enemies[index] != CharactersDatabase.Characters.Flavo); // Assert that an enemy has not accidentally been set as Flavo.
            }
        }

        /// <summary>
        /// Returns the number of enemies designed for the game.
        /// </summary>
        public int GetEnemiesQuantity()
        {
            return Enemies.Length;
        }
    }
}