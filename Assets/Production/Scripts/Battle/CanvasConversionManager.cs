﻿// AUTHOR: Benjamin Chaves

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.UI;

namespace Battle
{
    public class CanvasConversionManager : MonoBehaviour
    {
        public GameObject Canvas;

        private RectTransform canvasRectTrans;
        private CanvasScaler canvasScaler;

        /// <summary>
        /// The value by which the X component of the Canvas size should be divided.
        /// </summary>
        private float canvasSizeDivisorX;

        /// <summary>
        /// The value by which the Y component of the Canvas size should be divided.
        /// </summary>
        private float canvasSizeDivisorY;

        public void Initialise()
        {
            AssertInspectorInputs();

            CacheSubordinates();

            canvasSizeDivisorX = canvasRectTrans.sizeDelta.x / canvasScaler.referenceResolution.x;
            canvasSizeDivisorY = canvasRectTrans.sizeDelta.y / canvasScaler.referenceResolution.y;
        }

        private void AssertInspectorInputs()
        {
            Debug.Assert(Canvas != null);
        }

        private void CacheSubordinates()
        {
            Debug.Assert(Canvas != null);

            canvasRectTrans = Canvas.GetComponent<RectTransform>();
            Debug.Assert(canvasRectTrans != null);

            canvasScaler = Canvas.GetComponent<CanvasScaler>();
            Debug.Assert(canvasScaler != null);
        }

        public Vector2 GetCanvasReferenceResolution()
        {
            Debug.Assert(canvasScaler != null);

            return canvasScaler.referenceResolution;
        }

        /// <summary>
        /// Returns a Canvas position based on the given screenspace multipliers.
        /// </summary>
        public Vector2 GetMultipliedCanvasPosition(float screenMultiplierX, float screenMultiplierY)
        {
            Debug.Assert(canvasRectTrans != null);

            Vector2 vectorToReturn = Vector2.zero;

            vectorToReturn.x = (canvasRectTrans.sizeDelta.x / canvasSizeDivisorX) * screenMultiplierX;
            vectorToReturn.y = (canvasRectTrans.sizeDelta.y / canvasSizeDivisorY) * screenMultiplierY;

            return vectorToReturn;
        }

        /// <summary>
        /// Returns the user's mouse position in Canvas space.
        /// </summary>
        public Vector2 GetConvertedMousePosition()
        {
            Vector2 mousePosition = Input.mousePosition;
            
            mousePosition.x -= Screen.width / 2f;
            mousePosition.y -= Screen.height / 2f;

            mousePosition.x /= Screen.width;
            mousePosition.y /= Screen.height;

            mousePosition.x *= 1920f;
            mousePosition.y *= 1080f;

            return mousePosition;
        }
    }
}