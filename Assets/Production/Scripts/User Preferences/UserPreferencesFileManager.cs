﻿// AUTHOR: Benjamin Chaves

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using System.IO;
using SimpleJSON;

namespace UserPreferences
{
    public class UserPreferencesFileManager : MonoBehaviour
    {
        /// <summary>
        /// The name of the user preferences file on-disk.
        /// </summary>
        private const string userPreferencesFileName = "up";

        /// <summary>
        /// The JSON item ID for the music volume.
        /// </summary>
        private const string musicVolumeID = "mv";

        /// <summary>
        /// Represents whether the loaded user preferences file has complete data.
        /// </summary>
        private bool parsedDataComplete;

        /// <summary>
        /// Compiles the given data into JSON format, and saves it to disk.
        /// </summary>
        /// <param name="musicVolumeToSave">The music volume to save, between the values of 0f and 1f, both inclusive.</param>
        public void SaveUserPreferences(float musicVolumeToSave)
        {
            Debug.Assert(musicVolumeToSave >= 0f, musicVolumeToSave);
            Debug.Assert(musicVolumeToSave <= 1f, musicVolumeToSave);

            string fileData = "{";

            string musicVolumeString = ((int)(musicVolumeToSave * 100)).ToString();
            fileData += GetJSONItem(musicVolumeID, ref musicVolumeString);

            fileData += "}";

            string filePath = Path.Combine(Application.dataPath, userPreferencesFileName); // Cache the file-path of the user preferences file.

            using (StreamWriter streamWriter = File.CreateText(filePath))
            {
                streamWriter.Write(fileData);
            }
        }

        /// <summary>
        /// Compiles the given item ID and item content into a JSON item.
        /// </summary>
        /// <param name="itemID">THe ID term of the item.</param>
        /// <param name="itemContent">The content of the item.</param>
        private string GetJSONItem(string itemID, ref string itemContent)
        {
            Debug.Assert(itemID != "", itemID);
            Debug.Assert(itemContent != "", itemContent);

            return ",\"" + itemID + "\":\"" + itemContent + "\"";
        }

        /// <summary>
        /// Attempts to load the user preferences file from disk.
        /// </summary>
        public UserPreferencesFileLoadWrapper LoadUserPreferencesFile()
        {
            string filePath = Path.Combine(Application.dataPath, userPreferencesFileName);

            if (!File.Exists(filePath))
            {
                return new UserPreferencesFileLoadWrapper(UserPreferencesFileLoadWrapper.LoadResults.FileNotFound, null);
            }
            else
            {
                string fileData;

                using (StreamReader streamReader = File.OpenText(filePath))
                {
                    fileData = streamReader.ReadToEnd();
                }

                parsedDataComplete = true;

                JSONNode parsedFileData = JSON.Parse(fileData); // Parse the file-data string to JSON Node.

                float musicVolume = ParseInt(ref parsedFileData, musicVolumeID) / 100f;

                if(musicVolume < 0f || musicVolume > 1f)
                {
                    parsedDataComplete = false;
                }

                UserPreferencesFileData userPreferencesData = new UserPreferencesFileData(musicVolume);

                UserPreferencesFileLoadWrapper.LoadResults loadResult = parsedDataComplete ? UserPreferencesFileLoadWrapper.LoadResults.Successful : UserPreferencesFileLoadWrapper.LoadResults.FileIncomplete;

                return new UserPreferencesFileLoadWrapper(loadResult, userPreferencesData);
            }
        }

        /// <summary>
        /// Parses the given JSON integer item from the given save-file data.
        /// If the given JSON item cannot be found, then the data is marked as incomplete.
        /// </summary>
        private int ParseInt(ref JSONNode userPreferencesData, string itemID)
        {
            Debug.Assert(userPreferencesData != null);
            Debug.Assert(itemID != "");

            string itemStringValue = userPreferencesData[itemID]; // Cache the given JSON item from the user preferences data.

            int itemIntegerValue = 0;

            if (itemStringValue == null) // The given JSON item was not found in the user preferences data.
            {
                parsedDataComplete = false;
            }
            else // The given JSON item was found in the user preferences data.
            {
                try // Attempt to convert the string value to integer.
                {
                    itemIntegerValue = int.Parse(itemStringValue);
                }
                catch // Failed to convert the string value to integer.
                {
                    parsedDataComplete = false;
                }
            }

            return itemIntegerValue;
        }
    }
}