﻿// AUTHOR: Benjamin Chaves

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Battle
{
    public class CharactersDatabase : MonoBehaviour
    {
        public enum Characters
        {
            Flavo,
            Snailor,
            John,
            DuckDragon,
        }

        public CharacterAttributes FlavoAttributes;
        public CharacterAttributes SnailorAttributes;
        public CharacterAttributes JohnAttributes;
        public CharacterAttributes DuckDragonAttributes;

        public void Initialise()
        {
            FlavoAttributes.AssertInspectorInputs();
            SnailorAttributes.AssertInspectorInputs();
            JohnAttributes.AssertInspectorInputs();
            DuckDragonAttributes.AssertInspectorInputs();
        }

        public CharacterAttributes GetCharacterAttributes(Characters characterToGet)
        {
            Debug.Assert(FlavoAttributes != null);
            Debug.Assert(SnailorAttributes != null);
            Debug.Assert(JohnAttributes != null);
            Debug.Assert(DuckDragonAttributes != null);

            switch (characterToGet)
            {
                case Characters.Flavo:

                    return FlavoAttributes;

                case Characters.Snailor:

                    return SnailorAttributes;

                case Characters.John:

                    return JohnAttributes;

                case Characters.DuckDragon:

                    return DuckDragonAttributes;

                default:

                    Debug.LogError(characterToGet);
                    return null;
            }
        }

        /// <summary>
        /// Increases the max HP of the player character by the given amount.
        /// This is used in the context of upgrading the character's
        /// maximum HP after winning a battle.
        /// </summary>
        /// <param name="maxHPIncrease"></param>
        public void IncreaseFlavoMaxHP(uint maxHPIncrease)
        {
            Debug.Assert(maxHPIncrease > 0, maxHPIncrease);

            Debug.Assert(FlavoAttributes != null);

            FlavoAttributes.MaxHealth += maxHPIncrease;
        }

        /// <summary>
        /// Sets the max HP of the player character. This is used in
        /// the context of loading the character's maximum HP from the save-file.
        /// </summary>
        public void SetPlayerMaxHP(int newFlavoMaxHP)
        {
            Debug.Assert(newFlavoMaxHP > 0, newFlavoMaxHP);

            Debug.Assert(FlavoAttributes != null);

            FlavoAttributes.MaxHealth = (uint)newFlavoMaxHP;
        }

        /// <summary>
        /// Sets whether the player has the given card.
        /// </summary>
        public void SetPlayerHasCard(CardsDatabase.CardTypes cardToSet, bool newHasCard)
        {
            Debug.Assert(cardToSet != CardsDatabase.CardTypes.NormalAttack); // Assert that the Normal Attack is not being set, as this is always enabled.

            Debug.Assert(FlavoAttributes != null);
            Debug.Assert(SnailorAttributes != null);
            Debug.Assert(JohnAttributes != null);
            Debug.Assert(DuckDragonAttributes != null);

            switch (cardToSet)
            {
                case CardsDatabase.CardTypes.DoubleAttack:

                    FlavoAttributes.HasDoubleAttack = newHasCard;
                    break;

                case CardsDatabase.CardTypes.FastAttack:

                    FlavoAttributes.HasFastAttack = newHasCard;
                    break;

                case CardsDatabase.CardTypes.Potion:

                    FlavoAttributes.HasPotion = newHasCard;
                    break;

                case CardsDatabase.CardTypes.DrainAttack:

                    FlavoAttributes.HasDrainAttack = newHasCard;
                    break;

                default:

                    Debug.LogError(cardToSet);
                    break;
            }
        }
    }
}