﻿// AUTHOR: Benjamin Chaves

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Battle
{
    public class CardsDatabase : MonoBehaviour
    {
        [Tooltip("The turns for which a Duckifying card should Duckify the player.")]
        public int PlayerTurnsDuckified;

        [Tooltip("The cards which are available in the game.")]
        public Card[] Cards;

        /// <summary>
        /// The card types which are available in the game.
        /// </summary>
        public enum CardTypes
        {
            NormalAttack,
            DoubleAttack,
            FastAttack,
            Potion,
            DrainAttack,
            DragonAttack,
        }

        public void Initialise()
        {
            AssertInspectorsInputs();

            AssignCardsID();
        }

        /// <summary>
        /// Asserts that all Inspector fields have been set or filled.
        /// </summary>
        private void AssertInspectorsInputs()
        {
            Debug.Assert(PlayerTurnsDuckified > 0, PlayerTurnsDuckified);
            Debug.Assert(Cards.Length == 6, Cards.Length); // Assert that the correct number of cards have been inputted, as per the game design.

            for(int cardIndex = 0; cardIndex < Cards.Length; cardIndex++) // Iterate through each card.
            {
                Cards[cardIndex].AssertInspectorInputs();
            }
        }

        /// <summary>
        /// Assigns IDs to each card.
        /// </summary>
        private void AssignCardsID()
        {
            Debug.Assert(Cards.Length == 6); // Assert that the correct number of cards have been inputted, as per the game design.

            for (int cardIndex = 0; cardIndex < Cards.Length; cardIndex++) // Iterate through each card.
            {
                Cards[cardIndex].SetCardID(cardIndex);
            }
        }

        /// <summary>
        /// Returns the number of cards in the game.
        /// </summary>
        public int GetCardsQuantity()
        {
            return Cards.Length;
        }

        /// <summary>
        /// Returns the Card for the given card ID.
        /// </summary>
        /// <param name="cardIndex">The ID of the Card which should be returned.</param>
        public Card GetCardByID(int cardIndex)
        {
            Debug.Assert(cardIndex >= 0, cardIndex);
            Debug.Assert(cardIndex < Cards.Length, cardIndex);

            return Cards[cardIndex];
        }

        /// <summary>
        /// Returns the Card for the given card enum.
        /// </summary>
        /// <param name="cardToGet">The enum of the Card which should be returned.</param>
        public Card GetCardByEnum(CardTypes cardToGet)
        {
            return Cards[(int)cardToGet];
        }
    }
}