﻿// AUTHOR: Benjamin Chaves

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using TMPro;
using UnityEngine.UI;

public class UI_CameoAttributesManager : MonoBehaviour
{
    /// <summary>
    /// Cached reference to the item's parent RectTransform.
    /// </summary>
    private RectTransform rectTransform;

    [Header("Prefab Items")]

    public GameObject GO_CameoImage;
    public GameObject GO_CharacterNameText;
    public GameObject GO_SpeechText;

    private Image cameoImage;
    private TextMeshProUGUI characterNameText;
    private TextMeshProUGUI speechText;

    /// <summary>
    /// Reserved Color for setting element colours.
    /// </summary>
    private Color newColourSetter;

    /// <summary>
    /// Reserved Vector3 for setting a new position to the cameo item.
    /// </summary>
    private Vector3 newPositionSetter = Vector3.zero;

    public void Initialise()
    {
        AssertInspectorInputs();

        CacheSubordinates();

        rectTransform.anchoredPosition = Vector3.zero;
        rectTransform.localScale = Vector3.one;
    }

    /// <summary>
    /// Assert that all Inspector fields have been set or filled.
    /// </summary>
    private void AssertInspectorInputs()
    {
        Debug.Assert(GO_CameoImage != null);
        Debug.Assert(GO_CharacterNameText != null);
        Debug.Assert(GO_SpeechText != null);
    }

    private void CacheSubordinates()
    {
        Debug.Assert(GO_CameoImage != null);
        Debug.Assert(GO_CharacterNameText != null);
        Debug.Assert(GO_SpeechText != null);

        rectTransform = GetComponent<RectTransform>();
        Debug.Assert(rectTransform != null); // Assert that the component was found.

        cameoImage = GO_CameoImage.GetComponent<Image>();
        Debug.Assert(cameoImage != null); // Assert that the component was found.

        characterNameText = GO_CharacterNameText.GetComponent<TextMeshProUGUI>();
        Debug.Assert(characterNameText != null); // Assert that the component was found.

        speechText = GO_SpeechText.GetComponent<TextMeshProUGUI>();
        Debug.Assert(speechText != null); // Assert that the component was found.
    }

    /// <summary>
    /// Sets the Y position of this cameo.
    /// </summary>
    /// <param name="newYPosition">The new Y position of the item.</param>
    public void SetPosition(float newYPosition)
    {
        Debug.Assert(rectTransform != null);

        newPositionSetter.y = newYPosition;

        rectTransform.anchoredPosition = newPositionSetter;
    }

    /// <summary>
    /// Returns the Y position of this cameo.
    /// </summary>
    public float GetPositionY()
    {
        Debug.Assert(rectTransform != null);

        return rectTransform.anchoredPosition.y;
    }

    /// <summary>
    /// Sets the character name & speech text of this cameo to the given values.
    /// </summary>
    /// <param name="inputCharacterName">The character who is speaking.</param>
    /// <param name="inputSpeech">The speech spoken by the character.</param>
    public void SetText(string inputCharacterName, string inputSpeech)
    {
        Debug.Assert(inputCharacterName != "");
        Debug.Assert(inputSpeech != "");

        Debug.Assert(characterNameText != null);
        Debug.Assert(speechText != null);

        characterNameText.text = inputCharacterName;
        speechText.text = inputSpeech;
    }

    /// <summary>
    /// Sets the alpha values of each component
    /// of this cameo item to the given alpha value.
    /// </summary>
    /// <param name="newAlphaValue">The alpha value to which the items should be set.</param>
    public void SetAlphaValues(float newAlphaValue)
    {
        Debug.Assert(newAlphaValue >= 0f);
        Debug.Assert(newAlphaValue <= 1f);

        SetImageAlpha(ref newAlphaValue);
        SetTextAlphas(ref newAlphaValue);
    }

    /// <summary>
    /// Sets the alpha value of the cameo image to the given alpha.
    /// </summary>
    /// <param name="newAlphaValue">The alpha value to which the image should be set.</param>
    private void SetImageAlpha(ref float newAlphaValue)
    {
        Debug.Assert(newAlphaValue >= 0f);
        Debug.Assert(newAlphaValue <= 1f);

        Debug.Assert(cameoImage != null);

        newColourSetter = cameoImage.color;
        newColourSetter.a = newAlphaValue;

        cameoImage.color = newColourSetter;
    }

    /// <summary>
    /// Sets the alpha values of each cameo text item to the given alpha.
    /// </summary>
    /// <param name="newAlphaValue">The alpha value to which the texts should be set.</param>
    private void SetTextAlphas(ref float newAlphaValue)
    {
        Debug.Assert(newAlphaValue >= 0f);
        Debug.Assert(newAlphaValue <= 1f);

        Debug.Assert(characterNameText != null);
        Debug.Assert(speechText != null);

        newColourSetter = characterNameText.color;
        newColourSetter.a = newAlphaValue;

        characterNameText.color = newColourSetter;
        speechText.color = newColourSetter;
    }
}