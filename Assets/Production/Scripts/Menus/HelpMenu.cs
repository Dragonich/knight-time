﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
// Author: Luke Saliba
namespace Help
{
    public class HelpMenu : MonoBehaviour
    {
        public GameObject helpMenuOpen;
        static public bool isHelpMenuOpen;

        void Start()
        {
            helpMenuOpen.SetActive(false);
        }


        void Update()
        {
            if (isHelpMenuOpen == false)
            {
                if (Input.GetKeyDown(KeyCode.F1))
                {
                    Debug.Log("Help Menu opened");
                    helpMenuOpen.SetActive(true);
                    isHelpMenuOpen = true;
                }
            }
            else if (isHelpMenuOpen == true)
            {
                if (Input.GetKeyDown(KeyCode.F1))
                {
                    Debug.Log("help menu closed");
                    helpMenuOpen.SetActive(false);
                    isHelpMenuOpen = false;
                }
            }
        }
    }
}
