﻿// AUTHOR: Benjamin Chaves

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using System.IO;
using SimpleJSON;

namespace SaveFile
{
    public class SaveFileManager : MonoBehaviour
    {
        /// <summary>
        /// The name of the save-file on-disk.
        /// </summary>
        private const string savefileName = "sf";

        /// <summary>
        /// The JSON item ID for the Battle 1 Status enum.
        /// </summary>
        private const string battle1StatusID = "b1s";

        /// <summary>
        /// The JSON item ID for the Battle 2 Status enum.
        /// </summary>
        private const string battle2StatusID = "b2s";

        /// <summary>
        /// The JSON item ID for the Battle 3 Status enum.
        /// </summary>
        private const string battle3StatusID = "b3s";

        /// <summary>
        /// The JSON item ID for the Player Maximum HP integer.
        /// </summary>
        private const string playerMaxHealthID = "pmh";

        /// <summary>
        /// The JSON item ID for the Has Potion boolean.
        /// </summary>
        private const string hasPotionID = "hp";

        /// <summary>
        /// The JSON item ID for the Has Fast Attack boolean.
        /// </summary>
        private const string hasFastAttackID = "hfa";

        /// <summary>
        /// The JSON item ID for the Has Drain Attack boolean.
        /// </summary>
        private const string hasDrainAttackID = "hdra";

        /// <summary>
        /// The JSON item ID for the Has Double Attack boolean.
        /// </summary>
        private const string hasDoubleAttackID = "hdoa";

        /// <summary>
        /// The JSON term for the user having not attempted a given battle.
        /// </summary>
        private const string battleStatusUnattempted = "bsu";

        /// <summary>
        /// The JSON term for the user having attempted a given battle.
        /// </summary>
        private const string battleStatusAttempted = "bsa";

        /// <summary>
        /// The JSON term for the user having completed a given battle.
        /// </summary>
        private const string battleStatusCompleted = "bsc";

        /// <summary>
        /// Represents whether the loaded save-file has complete data.
        /// </summary>
        private bool parsedDataComplete;

        /// <summary>
        /// Outcomes which can result from attempting to delete the save-file.
        /// </summary>
        public enum DeletionResults
        {
            Successful,
            FileNotFound,
            Failed,
        }

        /// <summary>
        /// Returns whether the save-file exists on-disk.
        /// </summary>
        public bool SaveFileExists()
        {
            string filePath = Path.Combine(Application.dataPath, savefileName); // Cache the file-path of the save-file.

            return File.Exists(filePath);
        }

        /// <summary>
        /// Compiles the given save-file data into JSON format, and saves it to disk.
        /// </summary>
        /// <param name="dataToSave">The data which should be saved to disk.</param>
        public void WriteSaveFile(SaveFileData dataToSave)
        {
            Debug.Assert(dataToSave != null);
            Debug.Assert(dataToSave.PlayerMaxHealth > 0, dataToSave.PlayerMaxHealth); // Assert that the player's max HP is at least 1.

            string battle1Completed = GetStringFromEnum(dataToSave.Battle1Status); // Convert Battle 1 Completed bool to string.
            string battle2Completed = GetStringFromEnum(dataToSave.Battle2Status); // Convert Battle 2 Completed bool to string.
            string battle3Completed = GetStringFromEnum(dataToSave.Battle3Status); // Convert Battle 3 completed bool to string.

            string playerMaxHealth = dataToSave.PlayerMaxHealth.ToString(); // Convert Player Maximum HP integer to string.

            string hasPotion = GetStringFromBool(dataToSave.HasPotion); // Convert Has Potion bool to string.
            string hasFastAttack = GetStringFromBool(dataToSave.HasFastAttack); // Convert Has Fast Attack bool to string.
            string hasDrainAttack = GetStringFromBool(dataToSave.HasDrainAttack); // Convert Has Drain Attack bool to string.
            string hasDoubleAttack = GetStringFromBool(dataToSave.HasDoubleAttack); // Convert Has Double Attack bool to string.

            string savefileData = "{"; // Open the JSON string.

            savefileData += GetJSONItem(battle1StatusID, ref battle1Completed); // Add Battle 1 Status to save-file content.
            savefileData += GetJSONItem(battle2StatusID, ref battle2Completed); // Add Battle 2 Status to save-file content.
            savefileData += GetJSONItem(battle3StatusID, ref battle3Completed); // Add Battle 3 Status to save-file content.

            savefileData += GetJSONItem(playerMaxHealthID, ref playerMaxHealth); // Add Player Maximum HP to save-file content.

            savefileData += GetJSONItem(hasPotionID, ref hasPotion); // Add Has Potion to save-file content.
            savefileData += GetJSONItem(hasFastAttackID, ref hasFastAttack); // Add Has Fast Attack to save-file content.
            savefileData += GetJSONItem(hasDrainAttackID, ref hasDrainAttack); // Add Has Drain Attack to save-file content.
            savefileData += GetJSONItem(hasDoubleAttackID, ref hasDoubleAttack); // Add Has Double Attack to save-file content.

            savefileData += "}"; // Close the JSON string.

            string filePath = Path.Combine(Application.dataPath, savefileName); // Cache the file-path of the save-file.

            using (StreamWriter streamWriter = File.CreateText(filePath))
            {
                streamWriter.Write(savefileData);
            }
        }

        /// <summary>
        /// Returns a string based on the given enum value.
        /// </summary>
        private string GetStringFromEnum(SaveFileData.BattleStatus enumToConvert)
        {
            switch(enumToConvert)
            {
                case SaveFileData.BattleStatus.Unattempted:

                    return battleStatusUnattempted;

                case SaveFileData.BattleStatus.Attempted:

                    return battleStatusAttempted;

                case SaveFileData.BattleStatus.Completed:

                    return battleStatusCompleted;

                default:

                    Debug.LogError(enumToConvert);
                    return "";
            }
        }

        /// <summary>
        /// Converts the given boolean to string,
        /// where true is 1 and false is 0.
        /// </summary>
        private string GetStringFromBool(bool boolToConvert)
        {
            return boolToConvert ? "1" : "0";
        }

        /// <summary>
        /// Compiles the given item ID and item content into a JSON item.
        /// </summary>
        /// <param name="itemID">THe ID term of the item.</param>
        /// <param name="itemContent">The content of the item.</param>
        private string GetJSONItem(string itemID, ref string itemContent)
        {
            Debug.Assert(itemID != "", itemID);
            Debug.Assert(itemContent != "", itemContent);

            return ",\"" + itemID + "\":\"" + itemContent + "\"";
        }

        /// <summary>
        /// Attempts to load the save-file from disk.
        /// </summary>
        public SaveFileDataLoadWrapper LoadSaveFile()
        {
            string filePath = Path.Combine(Application.dataPath, savefileName); // Cache the file-path of the save-file.
            
            if(!File.Exists(filePath)) // The save-file does not exist on-disk.
            {
                return new SaveFileDataLoadWrapper(SaveFileDataLoadWrapper.LoadResults.FileNotFound, null); // Return the wrapper with result File Not Found and null save-file data.
            }
            else // The save-file exists on-disk.
            {
                string fileData;

                using (StreamReader streamReader = File.OpenText(filePath))
                {
                    fileData = streamReader.ReadToEnd();
                }

                parsedDataComplete = true;

                JSONNode parsedFileData = JSON.Parse(fileData); // Parse the file-data string to JSON Node.

                SaveFileData.BattleStatus battle1Status = ParseBattleStatusEnum(ref parsedFileData, battle1StatusID); // Parse the Battle 1 Status enum from the save-file data.
                SaveFileData.BattleStatus battle2Status = ParseBattleStatusEnum(ref parsedFileData, battle2StatusID); // Parse the Battle 2 Status enum from the save-file data.
                SaveFileData.BattleStatus battle3Status = ParseBattleStatusEnum(ref parsedFileData, battle3StatusID); // Parse the Battle 3 Status enum from the save-file data.

                int playerMaxHealth = ParseInt(ref parsedFileData, playerMaxHealthID); // Parse the Player Maximum HP integer from the save-file data.

                if(playerMaxHealth <= 0)
                {
                    parsedDataComplete = false;
                }

                bool hasPotion = ParseBool(ref parsedFileData, hasPotionID); // Parse the Has Potion boolean from the save-file data.
                bool hasFastAttack = ParseBool(ref parsedFileData, hasFastAttackID); // Parse the Has Fast Attack boolean from the save-file data.
                bool hasDrainAttack = ParseBool(ref parsedFileData, hasDrainAttackID); // Parse the Has Drain Attack boolean from the save-file data.
                bool hasDoubleAttack = ParseBool(ref parsedFileData, hasDoubleAttackID); // Parse the Double Attack boolean from the save-file data.

                SaveFileData saveFileData = new SaveFileData(battle1Status, battle2Status, battle3Status, playerMaxHealth, hasDoubleAttack, hasFastAttack, hasPotion, hasDrainAttack);

                SaveFileDataLoadWrapper.LoadResults loadResult = parsedDataComplete ? SaveFileDataLoadWrapper.LoadResults.Successful : SaveFileDataLoadWrapper.LoadResults.FileIncomplete;

                return new SaveFileDataLoadWrapper(loadResult, saveFileData);
            }
        }

        /// <summary>
        /// Returns the Battle Status enum based on the given save-file data and the given item ID.
        /// </summary>
        private SaveFileData.BattleStatus ParseBattleStatusEnum(ref JSONNode saveFileData, string itemID)
        {
            Debug.Assert(saveFileData != null);
            Debug.Assert(itemID != "");

            string itemValue = saveFileData[itemID];

            switch(itemValue)
            {
                case battleStatusUnattempted:

                    return SaveFileData.BattleStatus.Unattempted;

                case battleStatusAttempted:

                    return SaveFileData.BattleStatus.Attempted;

                case battleStatusCompleted:

                    return SaveFileData.BattleStatus.Completed;

                default:

                    Debug.LogError(itemValue);
                    return SaveFileData.BattleStatus.Unattempted;
            }
        }

        /// <summary>
        /// Parses the given JSON boolean item from the given save-file data.
        /// If the given JSON item cannot be found, then the data is marked as incomplete.
        /// </summary>
        private bool ParseBool(ref JSONNode saveFileData, string itemID)
        {
            Debug.Assert(saveFileData != null);
            Debug.Assert(itemID != "");

            string itemValue = saveFileData[itemID]; // Cache the given JSON item from the save-file data.

            if(itemValue == null) // The given JSON item was not found in the save-file data.
            {
                parsedDataComplete = false;

                return false;
            }
            else // The given JSON item was found in the save-file data.
            {
                return itemValue == "1";
            }
        }

        /// <summary>
        /// Parses the given JSON integer item from the given save-file data.
        /// If the given JSON item cannot be found, then the data is marked as incomplete.
        /// </summary>
        private int ParseInt(ref JSONNode saveFileData, string itemID)
        {
            Debug.Assert(saveFileData != null);
            Debug.Assert(itemID != "");

            string itemStringValue = saveFileData[itemID]; // Cache the given JSON item from the save-file data.

            int itemIntegerValue = 0;

            if (itemStringValue == null) // The given JSON item was not found in the save-file data.
            {
                parsedDataComplete = false;
            }
            else // The given JSON item was found in the save-file data.
            {
                try // Attempt to convert the string value to integer.
                {
                    itemIntegerValue = int.Parse(itemStringValue);
                }
                catch // Failed to convert the string value to integer.
                {
                    parsedDataComplete = false;
                }
            }

            return itemIntegerValue;
        }

        /// <summary>
        /// Attempts to delete the save-file, and also its associated Unity meta file.
        /// Returns the result of attempting to delete the save-file.
        /// </summary>
        public DeletionResults DeleteSaveFile()
        {
            DeleteFile(Path.Combine(Application.dataPath, savefileName + ".meta")); // Attempt deletion of save-file Unity meta file.
            return DeleteFile(Path.Combine(Application.dataPath, savefileName)); // Attempt deletion of save-file, and return attempt result.
        }

        /// <summary>
        /// Attempts to delete the given file, and returns the attempt result.
        /// </summary>
        private DeletionResults DeleteFile(string filePath)
        {
            Debug.Assert(filePath != "");

            if(File.Exists(filePath)) // The given file exists.
            {
                File.Delete(filePath);

                if(File.Exists(filePath)) // The given file still exists.
                {
                    return DeletionResults.Failed;
                }
                else // The given file no longer exists.
                {
                    return DeletionResults.Successful;
                }
            }
            else // The given file does not exist.
            {
                return DeletionResults.FileNotFound;
            }
        }
    }
}