﻿// Author: Luke Saliba
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

using SaveFile;
using DisolveTransitionShader;

namespace LevelSelection
{
    public class LevelSelecting : MonoBehaviour
    {
        private SaveFileManager saveFileManager;

        public GameObject GO_SaveFileManager;
        public GameObject DisolveShaderScript;
        public GameObject openMap;

        public GameObject level1CompletedCross;
        public GameObject level2CompletedCross;
        public GameObject level3CompletedCross;

        public GameObject level1FadeIn;
        public GameObject level2FadeIn;
        public GameObject level3FadeIn;
        
        static public bool mapOpen;

        public string Battle1;
        public string Battle2;
        public string Battle3;

        bool battle1Completed;
        bool battle2Completed;
        bool battle3Completed;

        public float Delay;
 
        void Start()
        {
            openMap.SetActive(true);
  
            saveFileManager = GO_SaveFileManager.GetComponent<SaveFileManager>();

            LoadSaveFile();
            //Red Crosses showing previous battles are completed
            level1CompletedCross.SetActive(false);
            level2CompletedCross.SetActive(false);
            level3CompletedCross.SetActive(false);
            //setting the enemies shader panels
            level1FadeIn.SetActive(true);
            level2FadeIn.SetActive(false);
            level3FadeIn.SetActive(false);

            if (battle1Completed == true)
            {
                level1CompletedCross.SetActive(true);
                level2FadeIn.SetActive(true);
            }
            if (battle2Completed == true)
            {
                level2CompletedCross.SetActive(true);
                level3FadeIn.SetActive(true);

            }
            if (battle3Completed == true)
            {
                level3CompletedCross.SetActive(true);
            }
        }
        public void LoadSaveFile()
        {
            SaveFileDataLoadWrapper saveFileDataLoadWrapper = saveFileManager.LoadSaveFile();

            if (saveFileDataLoadWrapper.LoadResult == SaveFileDataLoadWrapper.LoadResults.Successful)
            {
                //called from save file
                battle1Completed = saveFileDataLoadWrapper.SaveFileData.Battle1Status == SaveFileData.BattleStatus.Completed;
                battle2Completed = saveFileDataLoadWrapper.SaveFileData.Battle2Status == SaveFileData.BattleStatus.Completed;
                battle3Completed = saveFileDataLoadWrapper.SaveFileData.Battle3Status == SaveFileData.BattleStatus.Completed;
            }
        }

        public void PauseGame()
        {
            // pausing game
            if (Pause.PauseMenu.isPaused == false)
            {
                openMap.SetActive(true);
                Time.timeScale = 0f;
                mapOpen = true;
            }
        }
        public void ResumeGame()
        {
            //resuming game
            if (Pause.PauseMenu.isPaused == false)
            {
                openMap.SetActive(false);
                Time.timeScale = 1f;
                mapOpen = false;
            }

        }
        public void Level1()
        {
            if (battle1Completed == false && DisolveShaderScript.GetComponent<DisolveShader>().FadeTime > 1.8)
            {
                SceneManager.LoadScene(Battle1);
                mapOpen = false;
                DisolveShaderScript.GetComponent<DisolveShader>().ResetFade();
            }
        }
        public void Level2()
        {
            if (battle1Completed == true && battle2Completed == false && DisolveShaderScript.GetComponent<DisolveShader>().FadeTime > 1.8)
            {
                SceneManager.LoadScene(Battle2);
                mapOpen = false;
                DisolveShaderScript.GetComponent<DisolveShader>().ResetFade();
            }
        }

        public void Level3()
        {
            if (battle1Completed == true && battle2Completed == true && battle3Completed == false && DisolveShaderScript.GetComponent<DisolveShader>().FadeTime > 1.8)
            {
                SceneManager.LoadScene(Battle3);
                mapOpen = false;
                DisolveShaderScript.GetComponent<DisolveShader>().ResetFade();
            }
        }

        public void LoadLevel(int battleID)
        {
            switch (battleID)
            {
                case 1:

                    SceneManager.LoadScene(Battle1);
                    break;

                case 2:

                    SceneManager.LoadScene(Battle2);
                    break;

                case 3:

                    SceneManager.LoadScene(Battle3);
                    break;

                default:

                    Debug.LogError(battleID);
                    break;
            }
        }
    }
}
