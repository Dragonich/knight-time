﻿// AUTHOR: Benjamin Chaves

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Battle;

public class CharacterSoundEffects : MonoBehaviour
{
    [Header("Prefab Items")]

    public AudioClip FlavoHurtClip;
    public AudioClip SnailderHurtClip;
    public AudioClip JohnHurtClip;
    public AudioClip BebekHurtClip;

    [Tooltip("Cached reference to the Audio Source for playing the character effects.")]
    private AudioSource audioSourceComponent;

    public void Initialise()
    {
        AssertInspectorInputs();

        CacheSubordinates();
    }

    /// <summary>
    /// Assert that all Inspector fields have been set or filled.
    /// </summary>
    private void AssertInspectorInputs()
    {
        Debug.Assert(FlavoHurtClip != null);
        Debug.Assert(SnailderHurtClip != null);
        Debug.Assert(JohnHurtClip != null);
        Debug.Assert(BebekHurtClip != null);
    }

    private void CacheSubordinates()
    {
        audioSourceComponent = GetComponent<AudioSource>();
        Debug.Assert(audioSourceComponent != null); // Assert that the component was found.
    }

    public void PlayCharacterHurtClip(CharactersDatabase.Characters characterToPlay)
    {
        Debug.Assert(JohnHurtClip != null);
        Debug.Assert(FlavoHurtClip != null);
        Debug.Assert(BebekHurtClip != null);
        Debug.Assert(SnailderHurtClip != null);
        Debug.Assert(audioSourceComponent != null);

        switch (characterToPlay)
        {
            case CharactersDatabase.Characters.Flavo:

                audioSourceComponent.PlayOneShot(FlavoHurtClip);
                break;

            case CharactersDatabase.Characters.Snailor:

                audioSourceComponent.PlayOneShot(SnailderHurtClip);
                break;

            case CharactersDatabase.Characters.John:

                audioSourceComponent.PlayOneShot(JohnHurtClip);
                break;

            case CharactersDatabase.Characters.DuckDragon:

                audioSourceComponent.PlayOneShot(BebekHurtClip);
                break;

            default:

                Debug.LogError(characterToPlay);
                break;
        }
    }
}