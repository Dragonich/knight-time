﻿// AUTHOR: Benjamin Chaves

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Battle
{
    /// <summary>
    /// Contains which card has been played to the table, and who the card is targeting.
    /// </summary>
    public class TabledCard
    {
        /// <summary>
        /// The card which has been played to the table.
        /// </summary>
        public Card Card { get; private set; }

        /// <summary>
        /// The character index of the character the card is targeting.
        /// </summary>
        public int TargetedCharacter { get; private set; }

        public void SetAttributes(Card newCard, int newTargetedCharacter)
        {
            Debug.Assert(newCard != null);
            Debug.Assert(newTargetedCharacter >= 0);

            Card = newCard;
            TargetedCharacter = newTargetedCharacter;
        }

        public void SetBlank()
        {
            Card = null;
            TargetedCharacter = -1;
        }
    }
}