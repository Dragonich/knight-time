﻿// AUTHOR: Benjamin Chaves

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using TMPro;
using Audio;

namespace Battle
{
    public class UI_BattleDebriefManager : MonoBehaviour
    {
        [Header("Override Items")]

        private CardsManager cardsManager;
        private BattleManager battleManager;
        private SoundEffectsManager soundEffectsManager;

        public GameObject MenuParent;

        public GameObject VictoryElements;
        public GameObject FailureElements;

        public GameObject CardName;
        public GameObject HPUpgradeText;

        public GameObject PotionCard;
        public GameObject FastAttackCard;

        public GameObject LossArtwork;

        [Tooltip("The GameObject which holds the Audio Manager as a component.")]
        public GameObject GO_AudioManager;
        private AudioManager audioManager;

        [Header("Prefab Items")]

        public string MenuSceneName;

        public void Initialise(CardsManager inputCardsManager, BattleManager inputBattleManager, SoundEffectsManager inputSoundEffectsManager)
        {
            Debug.Assert(inputCardsManager != null);
            Debug.Assert(inputBattleManager != null);
            Debug.Assert(inputSoundEffectsManager != null);

            AssertInspectorInputs();

            CacheSubordinates();

            cardsManager = inputCardsManager;
            battleManager = inputBattleManager;
            soundEffectsManager = inputSoundEffectsManager;

            MenuParent.SetActive(false);
        }

        /// <summary>
        /// Assert that all Inspector fields have been set or filled.
        /// </summary>
        private void AssertInspectorInputs()
        {
            Debug.Assert(MenuParent != null);

            Debug.Assert(VictoryElements != null);
            Debug.Assert(FailureElements != null);

            Debug.Assert(CardName != null);
            Debug.Assert(HPUpgradeText != null);

            Debug.Assert(PotionCard != null);
            Debug.Assert(FastAttackCard != null);

            Debug.Assert(LossArtwork != null);
            Debug.Assert(GO_AudioManager != null);

            Debug.Assert(MenuSceneName != "");
        }

        private void CacheSubordinates()
        {
            Debug.Assert(GO_AudioManager != null);

            audioManager = GO_AudioManager.GetComponent<AudioManager>();
            Debug.Assert(audioManager != null); // Assert that the component was found.
        }

        /// <summary>
        /// Activates the battle victory debrief menu.
        /// </summary>
        public void ActivateVictoryDebrief()
        {
            Debug.Assert(MenuParent != null);
            Debug.Assert(VictoryElements != null);
            Debug.Assert(FailureElements != null);
            Debug.Assert(soundEffectsManager != null);
            Debug.Assert(battleManager != null);
            Debug.Assert(CardName != null);
            Debug.Assert(PotionCard != null);
            Debug.Assert(FastAttackCard != null);
            Debug.Assert(HPUpgradeText != null);

            MenuParent.SetActive(true);

            VictoryElements.SetActive(true);
            FailureElements.SetActive(false);

            soundEffectsManager.MenuSoundEffects.PlayUpgradePanelClip();

            TextMeshProUGUI cardNameText = CardName.GetComponent<TextMeshProUGUI>();
            Debug.Assert(cardNameText != null); // Assert that the component was found.

            CardsDatabase.CardTypes cardToSteal = battleManager.GetCardToSteal();
            cardNameText.text = cardsManager.GetDatabaseCardByEnum(cardToSteal).CardName;

            switch(cardToSteal)
            {
                case CardsDatabase.CardTypes.Potion:

                    PotionCard.SetActive(true);
                    FastAttackCard.SetActive(false);
                    break;

                case CardsDatabase.CardTypes.FastAttack:

                    PotionCard.SetActive(false);
                    FastAttackCard.SetActive(true);
                    break;

                default:

                    Debug.LogError(cardToSteal);
                    break;
            }

            TextMeshProUGUI hpUpgradeTextComponent = HPUpgradeText.GetComponent<TextMeshProUGUI>();
            Debug.Assert(hpUpgradeTextComponent != null); // Assert that the component was found.

            hpUpgradeTextComponent.text = "+" + battleManager.GetHPUpgrade();
        }

        /// <summary>
        /// Activates the failure screen.
        /// </summary>
        public void ActivateFailureDebrief()
        {
            Debug.Assert(MenuParent != null);
            Debug.Assert(audioManager != null);
            Debug.Assert(VictoryElements != null);
            Debug.Assert(FailureElements != null);

            MenuParent.SetActive(true);

            audioManager.PlayLossMusic();

            VictoryElements.SetActive(false);
            FailureElements.SetActive(true);
        }

        /// <summary>
        /// Triggered when the user has chosen to steal
        /// the enemy's skill card at the end of the battle.
        /// </summary>
        public void ButtonClickedSkillCard()
        {
            Debug.Assert(battleManager != null);

            battleManager.BattleEndStealCard();
        }

        /// <summary>
        /// Triggered when the user has chosen to
        /// upgrade their HP at the end of the battle.
        /// </summary>
        public void ButtonClickedIncreaseHP()
        {
            Debug.Assert(battleManager != null);

            battleManager.BattleEndUpgradeHP();
        }

        /// <summary>
        /// Triggered when the user has chosen to retry the battle at the end of the battle.
        /// Marks the current battle as attempted, saves the save-file data to disk,
        /// and restarts the current battle.
        /// </summary>
        public void ButtonClickedRetry()
        {
            Debug.Assert(battleManager != null);

            battleManager.SaveCurrentBattleAttempted();

            battleManager.RestartBattle();
        }

        /// <summary>
        /// Triggered when the user has chosen to quit to the menu screen at the end of the battle.
        /// Marks the current battle as attempted, saves the save-file data to disk,
        /// and quits to the main menu.
        /// </summary>
        public void ButtonClickedQuit()
        {
            Debug.Assert(battleManager != null);

            battleManager.SaveCurrentBattleAttempted();

            UnityEngine.SceneManagement.SceneManager.LoadScene(MenuSceneName);
        }
    }
}