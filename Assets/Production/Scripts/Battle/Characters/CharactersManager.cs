﻿// AUTHOR: Benjamin Chaves

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Battle
{
    public class CharactersManager : MonoBehaviour
    {
        private AppManager appManager;

        public GameObject GO_CharactersDatabase;
        public GameObject GO_BattleCharactersManager;

        private CharactersDatabase charactersDatabase;
        private BattleCharactersManager battleCharactersManager;

        public void InitialiseStaticSubordinates(AppManager inputAppManager)
        {
            Debug.Assert(inputAppManager != null);
            appManager = inputAppManager;

            charactersDatabase = GO_CharactersDatabase.GetComponent<CharactersDatabase>();
            Debug.Assert(charactersDatabase != null); // Assert that the component was found.

            charactersDatabase.Initialise();
        }

        public void InitialiseDynamicSubordinates()
        {
            Debug.Assert(GO_BattleCharactersManager != null);

            battleCharactersManager = GO_BattleCharactersManager.GetComponent<BattleCharactersManager>();
            Debug.Assert(battleCharactersManager != null); // Assert that the component was found.

            battleCharactersManager.Initialise(appManager);
        }

        /// <summary>
        /// Modifies the given character's current health value by the given amount.
        /// </summary>
        /// <param name="battleCharacterID">The character whose health should be modified.</param>
        /// <param name="healthModifier">The value by which the character's health should be modified.</param>
        public void ModifyCharacterHealth(int battleCharacterID, float healthModifier)
        {
            Debug.Assert(battleCharacterID >= 0, battleCharacterID);

            Debug.Assert(battleCharactersManager != null);

            battleCharactersManager.ModifyCharacterHealth(battleCharacterID, healthModifier);
        }

        public bool GetPlayerHasCard(int cardIndex)
        {
            Debug.Assert(cardIndex >= 0, cardIndex);

            Debug.Assert(battleCharactersManager != null);

            return battleCharactersManager.GetPlayerHasCard(cardIndex);
        }

        /// <summary>
        /// Triggers each AI to select & play a card to the table.
        /// </summary>
        public void TableAICards()
        {
            Debug.Assert(battleCharactersManager != null);

            battleCharactersManager.TableAICards();
        }

        public void DebitPlayerCardFromSet(int handCardID)
        {
            Debug.Assert(handCardID >= 0, handCardID);

            Debug.Assert(battleCharactersManager != null);

            battleCharactersManager.DebitPlayerCardFromSet(handCardID);
        }

        /// <summary>
        /// Returns the current health value of the given character.
        /// </summary>
        /// <param name="battleCharacterID">The character whose health value should be returned.</param>
        public float GetCharacterHealth(int battleCharacterID)
        {
            Debug.Assert(battleCharacterID >= 0, battleCharacterID);

            Debug.Assert(battleCharactersManager != null);

            return battleCharactersManager.GetCharacterHealth(battleCharacterID);
        }

        /// <summary>
        /// Triggers the given animation to be played on the given character.
        /// </summary>
        /// <param name="characterID">The character who should play the animation.</param>
        /// <param name="animationToTrigger">The animation which the character should play.</param>
        public void TriggerCharacterAnimation(int characterID, RigAnimator.CharacterAnimations animationToTrigger)
        {
            Debug.Assert(characterID >= 0, characterID);

            Debug.Assert(battleCharactersManager != null);

            battleCharactersManager.TriggerCharacterAnimation(characterID, animationToTrigger);
        }

        /// <summary>
        /// Advances the Skill Card cooldowns for all characters,
        /// and updates the information on the user's hand cards UI.
        /// </summary>
        public void AdvanceSkillCardCooldowns()
        {
            Debug.Assert(appManager != null);
            Debug.Assert(appManager.UIManager != null);
            Debug.Assert(battleCharactersManager != null);

            battleCharactersManager.AdvanceSkillCardCooldowns();

            appManager.UIManager.UpdateHandCardsInformation();
        }

        /// <summary>
        /// Advances the turns for which the player is Duckified,
        /// and un-Duckifies the player if appropriate.
        /// </summary>
        public void AdvanceTurnsDuckified()
        {
            Debug.Assert(battleCharactersManager != null);

            battleCharactersManager.AdvanceTurnsDuckified();
        }

        /// <summary>
        /// Returns whether the player has at least 1 card available to play from their hand.
        /// </summary>
        public bool GetPlayerHasAnyCardsAvailable()
        {
            Debug.Assert(battleCharactersManager != null);

            return battleCharactersManager.GetPlayerHasAnyCardsAvailable();
        }

        /// <summary>
        /// Returns whether any enemies have at least 1 card available to play.
        /// </summary>
        public bool GetEnemiesHaveAnyCardsAvailable()
        {
            Debug.Assert(battleCharactersManager != null);

            return battleCharactersManager.GetEnemiesHaveAnyCardsAvailable();
        }

        /// <summary>
        /// Returns the quantity of cards the player has in the given card set.
        /// </summary>
        /// <param name="cardSetID">The card whose quantity should be returned.</param>
        public int GetPlayerCardsQuantity(int cardSetID)
        {
            Debug.Assert(cardSetID >= 0, cardSetID);

            Debug.Assert(battleCharactersManager != null);

            return battleCharactersManager.GetPlayerCardsQuantity(cardSetID);
        }

        /// <summary>
        /// Returns whether the player has the given card
        /// set available to be played to the table.
        /// </summary>
        /// <param name="cardID">The card whose availability should be returned.</param>
        public bool GetPlayerHasCardAvailable(int cardID)
        {
            Debug.Assert(cardID >= 0, cardID);

            Debug.Assert(battleCharactersManager != null);

            return battleCharactersManager.GetPlayerHasCardAvailable(cardID);
        }

        /// <summary>
        /// Returns the number of cooldown turns remaining on the player's given card.
        /// </summary>
        /// <param name="cardSetID">The card whose cooldown turns should be returned.</param>
        public int GetPlayerCardCooldowns(int cardSetID)
        {
            Debug.Assert(cardSetID >= 0, cardSetID);

            Debug.Assert(battleCharactersManager != null);

            return battleCharactersManager.GetPlayerCardCooldowns(cardSetID);
        }

        public CharacterAttributes GetCharacterAttributes(CharactersDatabase.Characters characterToGet)
        {
            Debug.Assert(charactersDatabase != null);

            return charactersDatabase.GetCharacterAttributes(characterToGet);
        }

        /// <summary>
        /// Returns the randomised damage roll by the given character.
        /// </summary>
        /// <param name="characterID">The character whose damage roll should be returned.</param>
        public uint GetRolledCharacterDamageValue(int characterID)
        {
            Debug.Assert(characterID >= 0, characterID);

            Debug.Assert(battleCharactersManager != null);

            return battleCharactersManager.GetRolledCharacterDamageValue(characterID);
        }

        /// <summary>
        /// Increases the max HP of the player character by the given amount.
        /// This is used in the context of upgrading the character's
        /// maximum HP after winning a battle.
        /// </summary>
        /// <param name="maxHPIncrease">The amount by which the player's maximum HP should be increased.</param>
        public void IncreaseFlavoMaxHP(uint maxHPIncrease)
        {
            Debug.Assert(maxHPIncrease > 0, maxHPIncrease);

            Debug.Assert(charactersDatabase != null);

            charactersDatabase.IncreaseFlavoMaxHP(maxHPIncrease);
        }

        /// <summary>
        /// Sets the max HP of the player character. This is used in
        /// the context of loading the character's maximum HP from the save-file.
        /// </summary>
        public void SetPlayerMaxHP(int newPlayerMaxHP)
        {
            Debug.Assert(newPlayerMaxHP > 0, newPlayerMaxHP);

            Debug.Assert(charactersDatabase != null);

            charactersDatabase.SetPlayerMaxHP(newPlayerMaxHP);
        }

        /// <summary>
        /// Sets whether the player has the given card.
        /// </summary>
        public void SetPlayerHasCard(CardsDatabase.CardTypes cardToSet, bool newHasCard)
        {
            Debug.Assert(cardToSet != CardsDatabase.CardTypes.NormalAttack); // Assert that the Normal Attack is not being set, as this is always enabled.

            Debug.Assert(charactersDatabase != null);

            charactersDatabase.SetPlayerHasCard(cardToSet, newHasCard);
        }

        /// <summary>
        /// Sets the turns for which the player is Duckified.
        /// </summary>
        /// <param name="newTurnsDuckified">The turns for which the player is Duckified.</param>
        public void SetPlayerDuckified(int newTurnsDuckified)
        {
            Debug.Assert(newTurnsDuckified > 0, newTurnsDuckified);

            Debug.Assert(battleCharactersManager != null);

            battleCharactersManager.SetPlayerDuckified(newTurnsDuckified);
        }

        /// <summary>
        /// Returns the remaining turns for which the player is Duckified.
        /// </summary>
        public int GetPlayerTurnsDuckified()
        {
            Debug.Assert(battleCharactersManager != null);

            return battleCharactersManager.GetPlayerTurnsDuckified();
        }

        /// <summary>
        /// Sets the active status of the Flavo and Duck-Flavo character objects,
        /// inverse to each other depending on the given boolean.
        /// </summary>
        /// <param name="newDuckActive">Whether the Duck-Flavo object should be active.</param>
        public void SetDuckFlavoActive(bool newDuckActive)
        {
            Debug.Assert(battleCharactersManager != null);

            battleCharactersManager.SetDuckFlavoActive(newDuckActive);
        }

        /// <summary>
        /// Sets the player character to have the given tint.
        /// </summary>
        public void SetPlayerTint(CharacterTinter.CharacterTints tintToSet)
        {
            Debug.Assert(battleCharactersManager != null);

            battleCharactersManager.SetPlayerTint(tintToSet);
        }

        /// <summary>
        /// Sets the enemy character to have the given tint.
        /// </summary>
        public void SetEnemyTint(CharacterTinter.CharacterTints tintToSet)
        {
            Debug.Assert(battleCharactersManager != null);

            battleCharactersManager.SetEnemyTint(tintToSet);
        }

        /// <summary>
        /// Unfreezes the character animations, after being frozen for the cameos.
        /// </summary>
        public void UnfreezeCharacters()
        {
            Debug.Assert(battleCharactersManager != null);

            battleCharactersManager.UnfreezeCharacters();
        }

        /// <summary>
        /// Plays the Duckify particle effect on the enemy character.
        /// </summary>
        public void PlayEnemyDuckifyEffect()
        {
            Debug.Assert(battleCharactersManager != null);

            battleCharactersManager.PlayEnemyDuckifyEffect();
        }
    }
}