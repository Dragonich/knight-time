﻿// AUTHOR: Benjamin Chaves

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Pause;

namespace Battle
{
    public class UI_TutorialsPromptManager : MonoBehaviour
    {
        [Header("Override Items")]

        public GameObject MenuParent;

        public GameObject ButtonObject;
        private RectTransform buttonObjectRectTransform;

        [Header("Prefab Items")]

        [Tooltip("Determines for how many seconds the prompt button should be highlighted to the user.")]
        public float ButtonHighlightingSeconds;
        private Coroutine buttonHighlightTimerCoroutine;
        private WaitForSeconds waitForHighlightingSeconds;

        private TutorialsManager tutorialsManager;

        /// <summary>
        /// Represents whether the user's mouse is currently over the prompt.
        /// </summary>
        public bool MouseIsOver { get; private set; }

        /// <summary>
        /// Represents whether the prompt is currently highlighted to the user.
        /// </summary>
        private bool isHighlighted;

        /// <summary>
        /// Allocated variable to store the calculated scale value.
        /// </summary>
        private float cachedScaleValue;

        /// <summary>
        /// Allocated variable to store the scale assigned to the prompt.
        /// </summary>
        private Vector2 newScaleSetter;

        /// <summary>
        /// Index used in the sine formula determing the prompt scale.
        /// </summary>
        private float scaleFormulaIndex;

        public void Initialise(TutorialsManager inputTutorialsManager)
        {
            Debug.Assert(inputTutorialsManager != null);
            tutorialsManager = inputTutorialsManager;

            AssertInspectorInputs();

            CacheSubordinates();

            SetMenuActive(false);

            waitForHighlightingSeconds = new WaitForSeconds(ButtonHighlightingSeconds);
        }

        /// <summary>
        /// Assert that all Inspector fields have been set or filled.
        /// </summary>
        private void AssertInspectorInputs()
        {
            Debug.Assert(MenuParent != null);
            Debug.Assert(ButtonObject != null);

            Debug.Assert(ButtonHighlightingSeconds > 0f);
        }

        private void CacheSubordinates()
        {
            buttonObjectRectTransform = ButtonObject.GetComponent<RectTransform>();
            Debug.Assert(buttonObjectRectTransform != null); // Assert that the component was found.
        }

        private void Update()
        {
            ScalePrompt();
        }

        private void ScalePrompt()
        {
            Debug.Assert(buttonObjectRectTransform != null);

            if (isHighlighted && !PauseMenu.isPaused) // The prompt button is highlighted, and the pause menu is not active.
            {
                cachedScaleValue = Mathf.Sin(scaleFormulaIndex * 0.125f + 5f) * 0.25f + 1.25f;

                newScaleSetter.x = cachedScaleValue;
                newScaleSetter.y = cachedScaleValue;

                buttonObjectRectTransform.localScale = newScaleSetter;

                scaleFormulaIndex++;
            }
        }

        /// <summary>
        /// Sets the active status of the GameObject to the given boolean.
        /// </summary>
        public void SetMenuActive(bool newActive)
        {
            Debug.Assert(MenuParent != null);

            if(!newActive)
            {
                SetMouseIsOver(false);
            }

            MenuParent.SetActive(newActive);
        }

        /// <summary>
        /// Triggered when the UI button is clicked.
        /// </summary>
        public void ButtonClicked()
        {
            Debug.Assert(tutorialsManager != null);

            if(buttonHighlightTimerCoroutine != null) // The highlight timer coroutine is running.
            {
                StopCoroutine(buttonHighlightTimerCoroutine);

                buttonHighlightTimerCoroutine = null;
            }

            SetPromptHighlighted(false);

            tutorialsManager.StopTutorialsPromptCoroutine();

            tutorialsManager.ActivateContextualTutorial();
        }

        /// <summary>
        /// Sets whether the manager will consider
        /// the user's mouse to be over the prompt button.
        /// </summary>
        public void SetMouseIsOver(bool newMouseIsOver)
        {
            MouseIsOver = newMouseIsOver;
        }

        /// <summary>
        /// Sets whether the button prompt should be highlighted
        /// to the user to the given boolean.
        /// </summary>
        public void SetPromptHighlighted(bool newIsHighlighted)
        {
            scaleFormulaIndex = 0f; // Reset the formula index to where it results in 1.
            isHighlighted = newIsHighlighted;

            if(isHighlighted)
            {
                buttonHighlightTimerCoroutine = StartCoroutine(ButtonHighlightingTimer()); // Start the timer to set the prompt as not highlighted.
            }
            else
            {
                StartCoroutine(ReturnToDefaultScale()); // Return the prompt button to its default scale.
            }
        }

        /// <summary>
        /// After waiting for the set number of seconds while the prompt
        /// button is highlighted, sets the prompt button to not highlighted.
        /// </summary>
        private IEnumerator ButtonHighlightingTimer()
        {
            yield return waitForHighlightingSeconds;

            SetPromptHighlighted(false);

            buttonHighlightTimerCoroutine = null;
        }

        /// <summary>
        /// Smoothly returns the prompt button's scale to 1.
        /// </summary>
        private IEnumerator ReturnToDefaultScale()
        {
            Debug.Assert(buttonObjectRectTransform != null);

            while(!Mathf.Approximately(buttonObjectRectTransform.localScale.x, 1f)) // The prompt button's scale is not approximately 1.
            {
                cachedScaleValue = Mathf.MoveTowards(buttonObjectRectTransform.localScale.x, 1f, Time.deltaTime);

                newScaleSetter.x = cachedScaleValue;
                newScaleSetter.y = cachedScaleValue;

                buttonObjectRectTransform.localScale = newScaleSetter;

                yield return null;
            }
        }
    }
}