﻿// AUTHOR: Benjamin Chaves

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Pause;

namespace Battle
{
    public class StrikeActionManager : MonoBehaviour
    {
        [Header("Prefab Items")]

        [Tooltip("Defines the the maximum number of seconds for which the user can draw their strike.")]
        public float StrikeTimerCap;

        [Tooltip("Defines the rate at which the strike fades out.")]
        public float StrikeFadeSpeed;

        [Tooltip("The GameObject which holds the strike streak Line Renderer as a component.")]
        public GameObject GO_StrikeStreak;
        private LineRenderer strikeStreak;

        private UIManager uiManager;
        private Camera cameraComponent;
        private TutorialsManager tutorialsManager;
        private MouseActionsManager mouseActionsManager;

        /// <summary>
        /// Stores the seconds for which the user has been drawing their current strike.
        /// </summary>
        private float strikeTimer;

        /// <summary>
        /// Represents whether the user has hit the enemy during the current strike.
        /// </summary>
        private bool hitThisStrike;

        /// <summary>
        /// Represents whether the strike action is available to the user.
        /// </summary>
        private bool strikeAvailable;

        /// <summary>
        /// Whether the user has initiated the strike action.
        /// </summary>
        private bool strikeInitiated;

        /// <summary>
        /// A Vector2 reserved for assigning a position to a new strike streak vertex.
        /// </summary>
        private Vector2 newStreakVertexPosition;

        private Coroutine fadeOutLineCoroutine;

        public void Initialise(MouseActionsManager inputMouseActionsManager, Camera inputCameraComponent, UIManager inputUIManager, TutorialsManager inputTutorialsManager)
        {
            Debug.Assert(inputMouseActionsManager != null);
            Debug.Assert(inputCameraComponent != null);
            Debug.Assert(inputUIManager != null);
            Debug.Assert(inputTutorialsManager != null);

            AssertInspectorInputs();

            CacheSubordinates();

            uiManager = inputUIManager;
            cameraComponent = inputCameraComponent;
            tutorialsManager = inputTutorialsManager;
            mouseActionsManager = inputMouseActionsManager;
        }

        /// <summary>
        /// Assert that all Inspector fields have been set or filled.
        /// </summary>
        private void AssertInspectorInputs()
        {
            Debug.Assert(StrikeFadeSpeed > 0f);
            Debug.Assert(StrikeTimerCap > 0f);

            Debug.Assert(GO_StrikeStreak != null);
        }

        private void CacheSubordinates()
        {
            strikeStreak = GO_StrikeStreak.GetComponent<LineRenderer>();
            Debug.Assert(strikeStreak != null); // Assert that the component was found.
        }

        /// <summary>
        /// Sets the action to be available to be used by the user.
        /// </summary>
        public void ActivateAction()
        {
            hitThisStrike = false;
            strikeAvailable = true;
            strikeInitiated = false;

            strikeTimer = 0f;
        }

        /// <summary>
        /// Sets the action to not be available to be used by the user.
        /// </summary>
        public void CancelAction()
        {
            strikeAvailable = false;
        }

        private void Update()
        {
            Debug.Assert(uiManager != null);
            Debug.Assert(tutorialsManager != null);

            if (strikeAvailable && !PauseMenu.isPaused) // The Strike action is available, and the game is not paused.
            {
                if(strikeInitiated) // The user has initiated the strike.
                {
                    if (Input.GetMouseButton(0)) // The user has left-clicked pressed.
                    {
                        strikeTimer += Time.deltaTime;

                        if(strikeTimer < StrikeTimerCap) // The timer is within the cap.
                        {
                            UpdateStrike();
                        }
                        else if(fadeOutLineCoroutine == null) // The timer has exceeded the cap, and the line is not currently fading out.
                        {
                            EndStrike();
                        }
                    }
                    else // The user does not have left-click pressed.
                    {
                        if (Input.GetMouseButtonUp(0) && !hitThisStrike && fadeOutLineCoroutine == null) // The user has released left-click, has not hit, and the line is not currently fading out.
                        {
                            EndStrike();
                        }
                    }
                }
                else if (Input.GetMouseButtonDown(0)) // The user has not initiated the strike, and has pressed left-click.
                {
                    if (!uiManager.GetPointerOverAnyHandCard() && !tutorialsManager.MouseIsOverTutorialPrompt()) // The user's cursor is not over a card, and is not over the tutorial prompt.
                    {
                        StartStrike();
                    }
                }
            }
        }

        /// <summary>
        /// Locks the user into the strike card, and starts the strike.
        /// </summary>
        private void StartStrike()
        {
            Debug.Assert(mouseActionsManager != null);

            strikeInitiated = true;
            mouseActionsManager.TriggerActionInitiated();

            mouseActionsManager.LockPlayerCard(); // Lock the player into the currently-selected card.
        }

        /// <summary>
        /// Updates the strike with the user's current mouse position,
        /// and uses a raycast to check if the user has hit the enemy.
        /// </summary>
        private void UpdateStrike()
        {
            Debug.Assert(strikeStreak != null);
            Debug.Assert(cameraComponent != null);

            strikeStreak.positionCount++;

            newStreakVertexPosition = cameraComponent.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, cameraComponent.nearClipPlane));
            newStreakVertexPosition.x -= cameraComponent.transform.position.x;
            newStreakVertexPosition.y -= cameraComponent.transform.position.y;
            newStreakVertexPosition *= 13.25f;

            strikeStreak.SetPosition(strikeStreak.positionCount - 1, newStreakVertexPosition);

            if (!hitThisStrike) // The user has not hit the enemy this strike.
            {
                Ray hitRay = cameraComponent.ScreenPointToRay(Input.mousePosition);

                if (Physics.Raycast(hitRay, out RaycastHit rayHit))
                {
                    if (rayHit.collider.name == BattleCharactersManager.EnemyName) // The user has hit the enemy's collider.
                    {
                        hitThisStrike = true;

                        EndStrike();
                    }
                }
            }
        }

        /// <summary>
        /// Ends the strike, resolves the action, and pushes the camera.
        /// </summary>
        private void EndStrike()
        {
            Debug.Assert(strikeStreak != null);
            Debug.Assert(mouseActionsManager != null);

            fadeOutLineCoroutine = StartCoroutine(FadeOutLine());

            mouseActionsManager.TriggerActionResolved(MouseActionsManager.Symbols.Sword, hitThisStrike);

            if(hitThisStrike) // The user has hit the enemy this strike.
            {
                Vector3 strikeVector;

                if(strikeStreak.positionCount >= 2) // The strike-streak has at least 2 points.
                {
                    strikeVector = strikeStreak.GetPosition(strikeStreak.positionCount - 1) - strikeStreak.GetPosition(strikeStreak.positionCount - 2);
                }
                else // The strike-streak has 1 point, and therefore the line's direction is unknown.
                {
                    strikeVector = Vector3.right; // Apply a guessed line direction.
                }
                
                mouseActionsManager.PushCamera(strikeVector); // Push the camera as per the user's strike vector.
            }
        }

        /// <summary>
        /// Fades out the strike streak line to be invisible.
        /// </summary>
        private IEnumerator FadeOutLine()
        {
            Debug.Assert(strikeStreak != null);
            Debug.Assert(fadeOutLineCoroutine == null); // Assert that this coroutine is not already running.

            Color newLineColour = Color.red;

            while(newLineColour.a > 0f) // While the line's opacity is above 0.
            {
                newLineColour.a -= StrikeFadeSpeed * Time.deltaTime; // Decrease the value of the line's opacity.
                strikeStreak.material.SetColor("_BaseColor", newLineColour); // Apply the new line colour to the line.

                yield return null; // Pause the coroutine until the next frame.
            }

            strikeStreak.positionCount = 0; // Clear all positions from the line renderer.

            newLineColour.a = 1f;
            strikeStreak.material.SetColor("_BaseColor", newLineColour); // Apply the new line colour to the line.

            strikeAvailable = false;

            fadeOutLineCoroutine = null;
        }
    }
}