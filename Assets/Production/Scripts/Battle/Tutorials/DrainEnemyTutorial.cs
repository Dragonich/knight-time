﻿// AUTHOR: Benjamin Chaves

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Battle
{
    public class DrainEnemyTutorial : MonoBehaviour
    {
        private TutorialsManager tutorialsManager;
        private TutorialCursorManager tutorialCursorManager;

        /// <summary>
        /// A Vector2 reserved for assigning a new position to the tutorial cursor.
        /// </summary>
        private Vector2 cursorPositionSetter;

        /// <summary>
        /// A Vector2 for the target position to which the tutorial cursor should move.
        /// </summary>
        private Vector2 cursorPositionTarget;

        /// <summary>
        /// Whether this tutorial is active.
        /// </summary>
        public bool TutorialActive { get; private set; }

        private WaitForEndOfFrame waitForEndOfFrame;
        private WaitForSeconds waitForTutorialSeconds;
        private WaitForSeconds waitForTutorialSecondsHalf;

        private Coroutine holdOnScreenCentreCoroutine;
        private Coroutine moveToEnemyCharacterCoroutine;
        private Coroutine holdOnEnemyCharacterCoroutine;
        private Coroutine moveToPlayerCharacterCoroutine;
        private Coroutine holdOnPlayerCharacterCoroutine;
        private Coroutine holdInvisibleCoroutine;

        public void Initialise(TutorialsManager inputTutorialsManager, GameObject inputTutorialCursorPrefab, GameObject inputTutorialCursorsParent)
        {
            Debug.Assert(inputTutorialsManager != null);

            tutorialsManager = inputTutorialsManager;

            InstantiateTutorialCursor(inputTutorialCursorPrefab, inputTutorialCursorsParent);

            waitForEndOfFrame = new WaitForEndOfFrame();
            waitForTutorialSeconds = new WaitForSeconds(tutorialsManager.CursorHoldSeconds);
            waitForTutorialSecondsHalf = new WaitForSeconds(tutorialsManager.CursorHoldSeconds / 2f);
        }

        /// <summary>
        /// Instantiates the tutorial cursor for this tutorial.
        /// </summary>
        /// <param name="inputTutorialCursorPrefab">The prefab of the tutorial cursor GameObject to be instantiated.</param>
        /// <param name="inputTutorialCursorsParent">The GameObject which should be the parent of the instantiated tutorial cursor.</param>
        private void InstantiateTutorialCursor(GameObject inputTutorialCursorPrefab, GameObject inputTutorialCursorsParent)
        {
            Debug.Assert(inputTutorialCursorPrefab != null);
            Debug.Assert(inputTutorialCursorsParent != null);

            GameObject newTutorialCursor = Instantiate(inputTutorialCursorPrefab);

            newTutorialCursor.SetActive(false);
            newTutorialCursor.name = "Drain Enemy Tutorial Cursor";
            newTutorialCursor.transform.SetParent(inputTutorialCursorsParent.transform);

            tutorialCursorManager = newTutorialCursor.GetComponent<TutorialCursorManager>();
            Debug.Assert(tutorialCursorManager != null); // Assert that the component was found.

            tutorialCursorManager.Initialise();
        }

        /// <summary>
        /// Starts this tutorial.
        /// </summary>
        public void StartTutorial()
        {
            Debug.Assert(tutorialCursorManager != null);
            Debug.Assert(!TutorialActive); // Assert that the tutorial is not already active.

            TutorialActive = true;
            tutorialCursorManager.SetMouseClicked(false);

            holdOnScreenCentreCoroutine = StartCoroutine(HoldOnScreenCentre());
        }

        /// <summary>
        /// Ends this tutorial.
        /// </summary>
        public void EndTutorial()
        {
            Debug.Assert(TutorialActive);
            Debug.Assert(tutorialCursorManager != null);

            TutorialActive = false;

            StopCoroutines();

            tutorialCursorManager.SetActive(false);
        }

        private void StopCoroutines()
        {
            StopActiveCoroutine(holdOnScreenCentreCoroutine);

            StopActiveCoroutine(moveToEnemyCharacterCoroutine);
            StopActiveCoroutine(holdOnEnemyCharacterCoroutine);

            StopActiveCoroutine(moveToPlayerCharacterCoroutine);
            StopActiveCoroutine(holdOnPlayerCharacterCoroutine);

            StopActiveCoroutine(holdInvisibleCoroutine);
        }

        private void StopActiveCoroutine(Coroutine coroutineToStop)
        {
            if (coroutineToStop != null)
            {
                StopCoroutine(coroutineToStop);
            }
        }

        private IEnumerator HoldOnScreenCentre()
        {
            Debug.Assert(tutorialCursorManager != null);

            tutorialCursorManager.ZeroPosition();
            tutorialCursorManager.SetActive(true);

            yield return waitForTutorialSeconds;

            moveToEnemyCharacterCoroutine = StartCoroutine(MoveToEnemyCharacter());
            holdOnScreenCentreCoroutine = null;
        }

        private IEnumerator MoveToEnemyCharacter()
        {
            Debug.Assert(tutorialCursorManager != null);

            cursorPositionTarget.x = Screen.width * 0.3f;

            while (!TutorialsManager.VectorsApproximatelyEqual(tutorialCursorManager.GetAnchoredPosition(), cursorPositionTarget)) // The cursor has not reached its target.
            {
                float movementSpeed = tutorialsManager.CursorMovementSpeed * Time.deltaTime;
                cursorPositionSetter = Vector2.MoveTowards(tutorialCursorManager.GetAnchoredPosition(), cursorPositionTarget, movementSpeed); // Move the cursor towards its target.

                tutorialCursorManager.SetPosition(cursorPositionSetter);

                yield return waitForEndOfFrame;
            }

            holdOnEnemyCharacterCoroutine = StartCoroutine(HoldOnEnemyCharacter());
            moveToEnemyCharacterCoroutine = null;
        }

        private IEnumerator HoldOnEnemyCharacter()
        {
            Debug.Assert(tutorialCursorManager != null);

            yield return waitForTutorialSecondsHalf;

            tutorialCursorManager.SetMouseClicked(true);

            yield return waitForTutorialSecondsHalf;

            moveToPlayerCharacterCoroutine = StartCoroutine(MoveToPlayerCharacter());
            holdOnEnemyCharacterCoroutine = null;
        }

        private IEnumerator MoveToPlayerCharacter()
        {
            Debug.Assert(tutorialCursorManager != null);

            cursorPositionTarget.x = Screen.width * -0.3f;

            while (!TutorialsManager.VectorsApproximatelyEqual(tutorialCursorManager.GetAnchoredPosition(), cursorPositionTarget)) // The cursor has not reached its target.
            {
                float movementSpeed = tutorialsManager.CursorMovementSpeed * Time.deltaTime;
                cursorPositionSetter = Vector2.MoveTowards(tutorialCursorManager.GetAnchoredPosition(), cursorPositionTarget, movementSpeed); // Move the cursor towards its target.

                tutorialCursorManager.SetPosition(cursorPositionSetter);

                yield return waitForEndOfFrame;
            }

            holdOnPlayerCharacterCoroutine = StartCoroutine(HoldOnPlayerCharacter());
            moveToPlayerCharacterCoroutine = null;
        }

        private IEnumerator HoldOnPlayerCharacter()
        {
            Debug.Assert(tutorialCursorManager != null);

            yield return waitForTutorialSecondsHalf;

            tutorialCursorManager.SetMouseClicked(false);

            yield return waitForTutorialSecondsHalf;

            holdInvisibleCoroutine = StartCoroutine(HoldInvisible());
            holdOnPlayerCharacterCoroutine = null;
        }

        private IEnumerator HoldInvisible()
        {
            Debug.Assert(tutorialCursorManager != null);

            tutorialCursorManager.SetActive(false);

            yield return waitForTutorialSeconds;

            holdOnScreenCentreCoroutine = StartCoroutine(HoldOnScreenCentre());
            holdInvisibleCoroutine = null;
        }
    }
}