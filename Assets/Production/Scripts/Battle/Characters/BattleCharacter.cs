﻿// AUTHOR: Benjamin Chaves

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Battle
{
    public class BattleCharacter
    {
        public int CharacterID { get; private set; }        
        public float CurrentHealth { get; private set; }

        private GameObject characterObject;
        private CharacterAttributes characterAttributes;

        private RigAnimator rigAnimator;
        private CharacterTinter characterTinter;
        private CharacterParticlesManager characterParticlesManager;
        private UI_HealthGraphicsAttributesManager healthGraphicsManager;

        /// <summary>
        /// The turns for which this character is currently Duckified.
        /// </summary>
        public int TurnsDuckified { get; private set; }

        /// <summary>
        /// The hand cards of this character.
        /// </summary>
        private CardSet[] handCards;

        public BattleCharacter(CharacterAttributes inputCharacterAttributes, ref GameObject[] inputCharacterObjects, UI_HealthGraphicsAttributesManager inputHealthGraphicsManager, int inputCharacterID, int handCardsQuantity, CanvasConversionManager inputCanvasConversionManager, GameObject inputDamageEffect, GameObject inputHealEffect, GameObject inputDuckifyEffect)
        {
            Debug.Assert(inputCharacterID >= 0);
            Debug.Assert(inputCharacterAttributes != null);
            Debug.Assert(inputCharacterObjects != null);
            Debug.Assert(inputHealthGraphicsManager != null);
            Debug.Assert(handCardsQuantity > 0);
            Debug.Assert(inputCanvasConversionManager != null);
            Debug.Assert(inputDamageEffect != null);
            Debug.Assert(inputHealEffect != null);

            characterAttributes = inputCharacterAttributes;
            CharacterID = inputCharacterID;

            CurrentHealth = characterAttributes.MaxHealth;

            characterObject = inputCharacterObjects[0];
            Debug.Assert(characterObject != null);

            characterParticlesManager = new CharacterParticlesManager(inputDamageEffect, inputHealEffect, inputDuckifyEffect, characterObject.transform.position, CharacterID == 0);

            rigAnimator = new RigAnimator(characterObject);
            characterTinter = new CharacterTinter(ref inputCharacterObjects);
            handCards = new CardSet[handCardsQuantity];

            InitialiseHealthGraphics(inputHealthGraphicsManager, inputCanvasConversionManager);
        }

        private void InitialiseHealthGraphics(UI_HealthGraphicsAttributesManager inputHealthGraphicsManager, CanvasConversionManager inputCanvasConversionManager)
        {
            Debug.Assert(inputHealthGraphicsManager != null);
            Debug.Assert(inputCanvasConversionManager != null);

            healthGraphicsManager = inputHealthGraphicsManager;
            healthGraphicsManager.Initialise(inputCanvasConversionManager, characterAttributes.MaxHealth);
        }

        public void InitialiseCardSet(Card cardToSet, int newCardsQuantity)
        {
            Debug.Assert(cardToSet != null);
            Debug.Assert(newCardsQuantity >= 0);

            Debug.Assert(characterAttributes != null);

            if (characterAttributes.GetHasCard(cardToSet.CardID)) // This character has been set to have this card type.
            {
                Debug.Assert(handCards[cardToSet.CardID] == null);

                CardSet newCardSet = new CardSet(cardToSet);
                handCards[cardToSet.CardID] = newCardSet;

                if (!newCardSet.Card.IsSkillCard) // The current card is not a skill card.
                {
                    newCardSet.SetQuantity(newCardsQuantity);
                }
            }
        }

        /// <summary>
        /// Modifies this character's health by the given value.
        /// </summary>
        /// <param name="healthModifier">The value by which the character's health should be modified.</param>
        public void ModifyHealth(float healthModifier)
        {
            Debug.Assert(rigAnimator != null);
            Debug.Assert(characterTinter != null);
            Debug.Assert(healthGraphicsManager != null);
            Debug.Assert(characterParticlesManager != null);

            CurrentHealth += healthModifier;
            CurrentHealth = Mathf.Clamp(CurrentHealth, 0f, characterAttributes.MaxHealth); // Clamp the character's health between 0 and its maximum health.

            healthGraphicsManager.UpdateHealthGraphics(CurrentHealth); // Update this character's health graphics with the given health value.

            if(!Mathf.Approximately(Mathf.Abs(healthModifier), 0f)) // The change to the character's health is non-zero.
            {
                healthGraphicsManager.CreateHitScore((int)healthModifier); // Create the given mod-score on this character's health graphics.

                if(healthModifier > 0f) // The character has been healed.
                {
                    characterParticlesManager.PlayHealEffect();
                }
                else // The character has been damaged.
                {
                    characterTinter.ActivateDamageTint(); // Activate this character's damage tint.
                    characterParticlesManager.PlayDamageEffect();

                    rigAnimator.TriggerAnimation(RigAnimator.CharacterAnimations.Recoil); // Trigger this character to play its recoil animation.
                }
            }
        }

        /// <summary>
        /// Returns this character's percentage health.
        /// </summary>
        public float GetHealthPercentage()
        {
            Debug.Assert(characterAttributes != null);

            return CurrentHealth / characterAttributes.MaxHealth;
        }

        /// <summary>
        /// Returns whether this character has the given card.
        /// </summary>
        /// <param name="cardIndex"></param>
        public bool GetHasCard(int cardIndex)
        {
            Debug.Assert(characterAttributes != null);

            return characterAttributes.GetHasCard(cardIndex);
        }

        /// <summary>
        /// Returns the number of playable cards this character has for the given card.
        /// </summary>
        /// <param name="handCardID">The card whose quantity should be returned.</param>
        public int GetCardQuantity(int handCardID)
        {
            Debug.Assert(handCardID >= 0, handCardID);
            Debug.Assert(handCardID < handCards.Length, handCardID);

            CardSet cardSet = handCards[handCardID];
            Debug.Assert(cardSet != null);
            Debug.Assert(!cardSet.Card.IsSkillCard); // Assert that the given card is not a skill card.

            Debug.Assert(cardSet.Quantity >= 0);

            return cardSet.Quantity;
        }

        /// <summary>
        /// Returns whether the given card set is available to be played to the table.
        /// </summary>
        /// <param name="handCardID">The card whose availability should be returned.</param>
        public bool GetHasCardsAvailable(int handCardID)
        {
            Debug.Assert(handCardID >= 0, handCardID);
            Debug.Assert(handCardID < handCards.Length, handCardID);

            CardSet cardSet = handCards[handCardID];

            if (cardSet == null) // The character does not have the given card in their hand.
            {
                return false;
            }
            else // The character has the given card in their hand.
            {
                return cardSet.GetIsSetAvailable();
            }
        }

        /// <summary>
        /// Returns the number of cooldowns this character has for the given card.
        /// </summary>
        /// <param name="cardID">The card whose cooldown turns should be returned.</param>
        public int GetCardCooldowns(int cardID)
        {
            Debug.Assert(cardID >= 0, cardID);
            Debug.Assert(cardID < handCards.Length, cardID);

            CardSet cardSet = handCards[cardID];
            Debug.Assert(cardSet != null);
            Debug.Assert(cardSet.Card.IsSkillCard); // Assert that the given card is a skill card.

            return cardSet.CooldownsRemaining;
        }

        /// <summary>
        /// Debits the given card from the character's hand.
        /// If the card is a skill card, it will be put into cooldown.
        /// If the card is a combat card, 1 use will be removed from the set.
        /// </summary>
        /// <param name="cardID"></param>
        public void DebitCardFromSet(int cardID)
        {
            Debug.Assert(cardID >= 0);
            Debug.Assert(cardID < handCards.Length);

            CardSet cardSet = handCards[cardID];
            Debug.Assert(cardSet != null);

            cardSet.DebitCardFromSet();
        }

        /// <summary>
        /// Returns whether the character has at least 1 card
        /// available to play from their hand.
        /// </summary>
        public bool GetHasAnyCardsAvailable()
        {
            CardSet currentCardSet;

            for (int cardSetIndex = 0; cardSetIndex < handCards.Length; cardSetIndex++) // Iterate through each card in the hand.
            {
                currentCardSet = handCards[cardSetIndex];

                if (currentCardSet != null) // The player has the current card in their hand.
                {
                    if (currentCardSet.Card.IsSkillCard)
                    {
                        if (currentCardSet.CooldownsRemaining == 0) // The skill card is not cooling, and is available for use.
                        {
                            return true; // Return true, as the skill card is available.
                        }
                    }
                    else
                    {
                        if (currentCardSet.Quantity > 0) // The combat card has at least 1 use remaining.
                        {
                            return true; // Return true, as the combat card is available.
                        }
                    }
                }
            }

            return false; // Return false, as no skill cards or combat cards were found to be available.
        }

        /// <summary>
        /// Advances the cooldowns of all skill cards in the player's hand.
        /// </summary>
        public void AdvanceSkillCardCooldowns()
        {
            CardSet currentCardSet;

            for (int index = 0; index < handCards.Length; index++) // Iterate through each hand-card.
            {
                currentCardSet = handCards[index];

                if (currentCardSet != null && currentCardSet.Card.IsSkillCard) // The current card is in the player's hand, and is a skill card.
                {
                    currentCardSet.AdvanceTurnCooldowns(); // Advance the cooldown of the current hand-card.
                }
            }
        }

        /// <summary>
        /// Sets the turns for which this character is Duckified.
        /// </summary>
        /// <param name="newTurnsDuckified"></param>
        public void SetTurnsDuckified(int newTurnsDuckified)
        {
            Debug.Assert(newTurnsDuckified > 0, newTurnsDuckified);

            TurnsDuckified = newTurnsDuckified;
        }

        /// <summary>
        /// Advances the turns for which this character is Duckified.
        /// </summary>
        public void AdvanceTurnsDuckified()
        {
            TurnsDuckified--;
            Debug.Assert(TurnsDuckified >= 0);
        }

        /// <summary>
        /// Returns the randomised damage roll for this character.
        /// </summary>
        /// <param name="randomGenerator"></param>
        /// <returns></returns>
        public uint GetRolledCharacterDamageValue(System.Random randomGenerator)
        {
            Debug.Assert(randomGenerator != null);

            Debug.Assert(characterAttributes != null);

            return characterAttributes.RollDamageValue(randomGenerator);
        }

        /// <summary>
        /// Triggers the given animation to be played on the character.
        /// </summary>
        /// <param name="animationToTrigger"></param>
        public void TriggerAnimation(RigAnimator.CharacterAnimations animationToTrigger)
        {
            Debug.Assert(animationToTrigger != RigAnimator.CharacterAnimations.None);

            Debug.Assert(rigAnimator != null);

            rigAnimator.TriggerAnimation(animationToTrigger);
        }

        /// <summary>
        /// Compiles and returns a printout of the character's hand information.
        /// </summary>
        public string CompileHandCardsString()
        {
            string handCardsString = "";

            for (int cardIndex = 0; cardIndex < handCards.Length; cardIndex++) // Iterate through each card in the hand.
            {
                if (handCards[cardIndex] == null)
                {
                    handCardsString += cardIndex + " :: null\n";
                }
                else
                {
                    handCardsString += cardIndex + " :: " + handCards[cardIndex].ToString() + "\n";
                }
            }

            return handCardsString;
        }

        /// <summary>
        /// Returns the position of the character object in the gameworld.
        /// </summary>
        public Vector3 GetCharacterWorldPosition()
        {
            Debug.Assert(characterObject != null);

            return characterObject.transform.position;
        }

        /// <summary>
        /// Sets the character's GameObject active status to the given boolean.
        /// </summary>
        /// <param name="newActive"></param>
        public void SetCharacterObjectActive(bool newActive)
        {
            Debug.Assert(characterObject != null);

            characterObject.SetActive(newActive);
        }

        /// <summary>
        /// Sets the character to have the given tint.
        /// </summary>
        /// <param name="tintToSet"></param>
        public void SetCharacterTint(CharacterTinter.CharacterTints tintToSet)
        {
            Debug.Assert(characterTinter != null);

            characterTinter.SetCharacterTint(tintToSet);
        }

        /// <summary>
        /// Advances the damage tint timer by the given delta-time value.
        /// </summary>
        /// <param name="deltaTime"></param>
        public void AdvanceDamageTintTimer(float deltaTime)
        {
            Debug.Assert(characterTinter != null);

            characterTinter.AdvanceDamageTintTimer(deltaTime);
        }

        /// <summary>
        /// Returns the lower inclusive value in the character's damage roll.
        /// </summary>
        public int GetCharacterDamageRangeLower()
        {
            Debug.Assert(characterAttributes != null);

            return characterAttributes.DamageRangeLower;
        }

        /// <summary>
        /// Unfreezes the character, and transitions it into its idle animation.
        /// </summary>
        public void UnfreezeCharacter()
        {
            Debug.Assert(rigAnimator != null);

            rigAnimator.UnfreezeCharacter();
        }

        /// <summary>
        /// Triggers the Duckify particle effect to be played on this character.
        /// </summary>
        public void PlayDuckifyParticleEffect()
        {
            Debug.Assert(characterParticlesManager != null);

            characterParticlesManager.PlayDuckifyEffect();
        }
    }
}