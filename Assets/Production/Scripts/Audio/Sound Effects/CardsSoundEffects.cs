﻿// AUTHOR: Benjamin Chaves

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Battle;

public class CardsSoundEffects : MonoBehaviour
{
    [Header("Flavo Prefab Items")]

    public AudioClip FlavoPotion;
    public AudioClip FlavoFastAttack;
    public AudioClip FlavoDoubleAttack;

    [Header("Snailder Prefab Items")]

    public AudioClip SnailderFastAttack;

    [Header("John Prefab Items")]

    public AudioClip JohnPotion;
    public AudioClip JohnDrainAttack;

    [Header("Bebek Prefab Items")]

    public AudioClip BebekPotion;
    public AudioClip BebekDoubleAttack;
    public AudioClip BebekDragonAttack;

    [Tooltip("Cached reference to the Audio Source for playing the card sound effects.")]
    private AudioSource audioSourceComponent;

    public void Initialise()
    {
        AssertInspectorInputs();

        CacheSubordinates();
    }

    /// <summary>
    /// Assert that all Inspector fields have been set or filled.
    /// </summary>
    private void AssertInspectorInputs()
    {
        Debug.Assert(FlavoPotion != null);
        Debug.Assert(FlavoFastAttack != null);
        Debug.Assert(FlavoDoubleAttack != null);

        Debug.Assert(SnailderFastAttack != null);

        Debug.Assert(JohnPotion != null);
        Debug.Assert(JohnDrainAttack != null);

        Debug.Assert(BebekPotion != null);
        Debug.Assert(BebekDoubleAttack != null);
        Debug.Assert(BebekDragonAttack != null);
    }

    private void CacheSubordinates()
    {
        audioSourceComponent = GetComponent<AudioSource>();
        Debug.Assert(audioSourceComponent != null); // Assert that the component was found.
    }

    /// <summary>
    /// Plays the given character's sound effect for the given card.
    /// </summary>
    /// <param name="character">The character whose sound effect should be played.</param>
    /// <param name="cardPlayed">The card whose sound effect should be played.</param>
    public void PlayCardSoundEffect(CharactersDatabase.Characters character, CardsDatabase.CardTypes cardPlayed)
    {
        switch(character)
        {
            case CharactersDatabase.Characters.Flavo:

                PlayFlavoClip(cardPlayed);
                break;

            case CharactersDatabase.Characters.Snailor:

                PlaySnailderClip(cardPlayed);
                break;

            case CharactersDatabase.Characters.John:

                PlayJohnClip(cardPlayed);
                break;

            case CharactersDatabase.Characters.DuckDragon:

                PlayBebekClip(cardPlayed);
                break;

            default:

                Debug.LogError(character);
                break;
        }
    }

    /// <summary>
    /// Plays Flavo's sound effect for the given card.
    /// </summary>
    /// <param name="cardPlayed">The card whose sound effect should be played.</param>
    private void PlayFlavoClip(CardsDatabase.CardTypes cardPlayed)
    {
        Debug.Assert(FlavoPotion != null);
        Debug.Assert(FlavoFastAttack != null);
        Debug.Assert(FlavoDoubleAttack != null);
        Debug.Assert(audioSourceComponent != null);

        switch(cardPlayed)
        {
            case CardsDatabase.CardTypes.Potion:

                audioSourceComponent.PlayOneShot(FlavoPotion);
                break;

            case CardsDatabase.CardTypes.FastAttack:

                audioSourceComponent.PlayOneShot(FlavoFastAttack);
                break;

            case CardsDatabase.CardTypes.DoubleAttack:

                audioSourceComponent.PlayOneShot(FlavoDoubleAttack);
                break;

            default: break; // Empty default because this character has no other sound effects other than the above cases.
        }
    }

    /// <summary>
    /// Plays Snailder's sound effect for the given card.
    /// </summary>
    /// <param name="cardPlayed">The card whose sound effect should be played.</param>
    private void PlaySnailderClip(CardsDatabase.CardTypes cardPlayed)
    {
        Debug.Assert(SnailderFastAttack != null);
        Debug.Assert(audioSourceComponent != null);

        switch (cardPlayed)
        {
            case CardsDatabase.CardTypes.FastAttack:

                audioSourceComponent.PlayOneShot(SnailderFastAttack);
                break;

            default: break; // Empty default because this character has no other sound effects other than the above cases.
        }
    }

    /// <summary>
    /// Plays John's sound effect for the given card.
    /// </summary>
    /// <param name="cardPlayed">The card whose sound effect should be played.</param>
    private void PlayJohnClip(CardsDatabase.CardTypes cardPlayed)
    {
        Debug.Assert(JohnPotion != null);
        Debug.Assert(JohnDrainAttack != null);
        Debug.Assert(audioSourceComponent != null);

        switch (cardPlayed)
        {
            case CardsDatabase.CardTypes.Potion:

                audioSourceComponent.PlayOneShot(JohnPotion);
                break;

            case CardsDatabase.CardTypes.DrainAttack:

                audioSourceComponent.PlayOneShot(JohnDrainAttack);
                break;

            default: break; // Empty default because this character has no other sound effects other than the above cases.
        }
    }

    /// <summary>
    /// Plays Bebek''s sound effect for the given card.
    /// </summary>
    /// <param name="cardPlayed">The card whose sound effect should be played.</param>
    private void PlayBebekClip(CardsDatabase.CardTypes cardPlayed)
    {
        Debug.Assert(BebekPotion != null);
        Debug.Assert(BebekDoubleAttack != null);
        Debug.Assert(BebekDragonAttack != null);
        Debug.Assert(audioSourceComponent != null);

        switch (cardPlayed)
        {
            case CardsDatabase.CardTypes.Potion:

                audioSourceComponent.PlayOneShot(BebekPotion);
                break;

            case CardsDatabase.CardTypes.DoubleAttack:

                audioSourceComponent.PlayOneShot(BebekDoubleAttack);
                break;

            case CardsDatabase.CardTypes.DragonAttack:

                audioSourceComponent.PlayOneShot(BebekDragonAttack);
                break;

            default: break; // Empty default because this character has no other sound effects other than the above cases.
        }
    }
}