﻿// AUTHOR: Benjamin Chaves

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Pause;

namespace Battle
{
    public class MouseActionsManager : MonoBehaviour
    {
        [Tooltip("The camera GameObject in the gameworld.")]
        public GameObject Camera;

        private AppManager appManager;

        [Tooltip("The GameObject which holds the Drain Action Manager as a component.")]
        public GameObject GO_DrainActionManager;

        [Tooltip("The GameObject which holds the Strike Action Manager as a component.")]
        public GameObject GO_StrikeActionManager;

        [Tooltip("The GameObject which holds the Potiob Action Manager as a component.")]
        public GameObject GO_PotionActionManager;

        [Tooltip("The GameObject which holds the Action Failed Manager as a component.")]
        public GameObject GO_ActionFailedManager;

        private DrainActionManager drainActionManager;
        private StrikeActionManager strikeActionManager;
        private PotionActionManager potionActionManager;
        private UI_ActionFailedManager actionFailedManager;

        [Tooltip("The parent GameObject of the symbols' graphics.")]
        public GameObject GO_SymbolsParent;

        /// <summary>
        /// The RectTransform component of the symbols' parent GameObject.
        /// </summary>
        private RectTransform symbolsParentRectTrans;

        [Tooltip("The GameObject which contains the Sword graphic.")]
        public GameObject SwordSymbol;

        [Tooltip("The GameObject which contains the Potion graphic.")]
        public GameObject PotionSymbol;

        [Tooltip("The GameObject which contains the Drain graphic.")]
        public GameObject DrainSymbol;

        /// <summary>
        /// The ability symbol which is currently active.
        /// </summary>
        public Symbols ActiveSymbol { get; private set; }

        /// <summary>
        /// The ability symbols which are available in the game, including None.
        /// </summary>
        public enum Symbols
        {
            Sword,
            Potion,
            Drain,
            None,
        }

        public void Initialise(AppManager inputAppManager)
        {
            Debug.Assert(inputAppManager != null);
            appManager = inputAppManager;

            AssertInspectorInputs();

            DeactivateAllSymbols();

            CacheSubordinates();
            InitialiseSubordinates();
        }

        /// <summary>
        /// Assert that all Inspector fields have been set or filled.
        /// </summary>
        private void AssertInspectorInputs()
        {
            Debug.Assert(Camera != null);

            Debug.Assert(SwordSymbol != null);
            Debug.Assert(PotionSymbol != null);
            Debug.Assert(DrainSymbol != null);

            Debug.Assert(GO_SymbolsParent != null);
            Debug.Assert(GO_ActionFailedManager != null);
        }

        private void CacheSubordinates()
        {
            Debug.Assert(GO_SymbolsParent != null);
            Debug.Assert(GO_StrikeActionManager != null);
            Debug.Assert(GO_PotionActionManager != null);
            Debug.Assert(GO_DrainActionManager != null);
            Debug.Assert(GO_ActionFailedManager != null);

            strikeActionManager = GO_StrikeActionManager.GetComponent<StrikeActionManager>();
            Debug.Assert(strikeActionManager != null); // Assert that the component was found.

            potionActionManager = GO_PotionActionManager.GetComponent<PotionActionManager>();
            Debug.Assert(potionActionManager != null); // Assert that the component was found.

            drainActionManager = GO_DrainActionManager.GetComponent<DrainActionManager>();
            Debug.Assert(drainActionManager != null); // Assert that the component was found.

            actionFailedManager = GO_ActionFailedManager.GetComponent<UI_ActionFailedManager>();
            Debug.Assert(actionFailedManager != null); // Assert that the component was found.

            symbolsParentRectTrans = GO_SymbolsParent.GetComponent<RectTransform>();
            Debug.Assert(symbolsParentRectTrans != null); // Assert that the component was found.
        }

        private void InitialiseSubordinates()
        {
            Debug.Assert(Camera != null);
            Debug.Assert(strikeActionManager != null);
            Debug.Assert(potionActionManager != null);
            Debug.Assert(drainActionManager != null);
            Debug.Assert(actionFailedManager != null);

            Camera cameraComponent = Camera.GetComponent<Camera>();
            Debug.Assert(cameraComponent != null); // Assert that the component was found.

            strikeActionManager.Initialise(this, cameraComponent, appManager.UIManager, appManager.TutorialsManager);
            potionActionManager.Initialise(this, cameraComponent, appManager.UIManager, appManager.TutorialsManager);
            drainActionManager.Initialise(this, cameraComponent, appManager.UIManager, appManager.TutorialsManager);

            actionFailedManager.Initialise();
        }

        private void Update()
        {
            Debug.Assert(appManager != null);
            Debug.Assert(appManager.CardsManager != null);
            Debug.Assert(appManager.TutorialsManager != null);

            if(ActiveSymbol != Symbols.None && !PauseMenu.isPaused) // A symbol is active, and the pause menu is not active.
            {
                SnapSymbolsToMouse();

                if(Input.GetMouseButtonDown(1)) // The user has pressed the right-mouse button this frame.
                {
                    DeactivateAllSymbols();
                    CancelExistingActions();

                    appManager.CardsManager.ClearTabledCards();
                    appManager.TutorialsManager.EndAllTutorials();
                }
            }
        }

        /// <summary>
        /// Activates the given symbol & action.
        /// </summary>
        /// <param name="symbolToActivate">The ability symbol which should be activated.</param>
        public void ActivateSymbol(Symbols symbolToActivate)
        {
            Debug.Assert(SwordSymbol != null);
            Debug.Assert(PotionSymbol != null);
            Debug.Assert(DrainSymbol != null);
            Debug.Assert(strikeActionManager != null);
            Debug.Assert(potionActionManager != null);
            Debug.Assert(drainActionManager != null);

            DeactivateAllSymbols();
            CancelExistingActions();

            ActiveSymbol = symbolToActivate;

            SnapSymbolsToMouse();

            switch (symbolToActivate)
            {
                case Symbols.Sword:

                    SwordSymbol.SetActive(true);

                    strikeActionManager.ActivateAction();
                    break;

                case Symbols.Potion:

                    PotionSymbol.SetActive(true);

                    potionActionManager.ActivateAction();
                    break;

                case Symbols.Drain:

                    DrainSymbol.SetActive(true);

                    drainActionManager.ActivateAction();
                    break;

                default:

                    Debug.LogError(symbolToActivate);
                    break;
            }
        }

        /// <summary>
        /// Snaps all symbols to match the mouse position.
        /// </summary>
        private void SnapSymbolsToMouse()
        {
            Debug.Assert(appManager != null);
            Debug.Assert(appManager.CanvasConversionManager != null);
            Debug.Assert(symbolsParentRectTrans != null);

            symbolsParentRectTrans.anchoredPosition = appManager.CanvasConversionManager.GetConvertedMousePosition();
        }

        /// <summary>
        /// Deactivates any symbols which may be active.
        /// </summary>
        private void DeactivateAllSymbols()
        {
            ActiveSymbol = Symbols.None;

            SwordSymbol.SetActive(false);
            DrainSymbol.SetActive(false);
            PotionSymbol.SetActive(false);
        }

        /// <summary>
        /// Cancels any actions which may be active.
        /// </summary>
        private void CancelExistingActions()
        {
            Debug.Assert(drainActionManager != null);
            Debug.Assert(drainActionManager != null);
            Debug.Assert(potionActionManager != null);

            drainActionManager.CancelAction();
            strikeActionManager.CancelAction();
            potionActionManager.CancelAction();
        }

        /// <summary>
        /// Locks the player into their currently-selected card,
        /// preventing them from selecting a different card instead.
        /// </summary>
        public void LockPlayerCard()
        {
            Debug.Assert(appManager != null);
            Debug.Assert(appManager.BattleManager != null);

            appManager.BattleManager.SetAcceptingPlayerCardSelection(false);
        }

        /// <summary>
        /// Notifies the Tutorials Manager that the user has initiated a mouse action.
        /// </summary>
        public void TriggerActionInitiated()
        {
            Debug.Assert(appManager != null);
            Debug.Assert(appManager.TutorialsManager != null);

            appManager.TutorialsManager.NotifyMouseActionInitiated();
        }

        /// <summary>
        /// Finishes the given action with the given success result.
        /// </summary>
        /// <param name="actionResolved">Which action is being finished.</param>
        /// <param name="actionSuccessful">Whether the user completed the action successfully.</param>
        public void TriggerActionResolved(Symbols actionResolved, bool actionSuccessful)
        {
            Debug.Assert(appManager != null);
            Debug.Assert(appManager.TutorialsManager != null);
            Debug.Assert(actionFailedManager != null);

            appManager.TutorialsManager.NotifyActionResolved(actionResolved, actionSuccessful);

            DeactivateAllSymbols();

            if(!actionSuccessful)
            {
                actionFailedManager.ActivateText(actionResolved);
            }

            appManager.BattleManager.ResolvePlayerTurnComponent(actionSuccessful);
        }

        /// <summary>
        /// Pushes the camera's position with the given Vector2.
        /// </summary>
        /// <param name="strikeVector">The Vector2 by which the camera's position should be pushed.</param>
        public void PushCamera(Vector2 strikeVector)
        {
            Debug.Assert(appManager != null);
            Debug.Assert(appManager.CameraAnimationsManager != null);

            appManager.CameraAnimationsManager.PushCamera(strikeVector);
        }

        /// <summary>
        /// Sets the player character to have the given tint.
        /// </summary>
        public void SetPlayerTint(CharacterTinter.CharacterTints tintToSet)
        {
            Debug.Assert(appManager != null);
            Debug.Assert(appManager.CharactersManager != null);

            appManager.CharactersManager.SetPlayerTint(tintToSet);
        }

        /// <summary>
        /// Sets the enemy character to have the given tint.
        /// </summary>
        public void SetEnemyTint(CharacterTinter.CharacterTints tintToSet)
        {
            Debug.Assert(appManager != null);
            Debug.Assert(appManager.CharactersManager != null);

            appManager.CharactersManager.SetEnemyTint(tintToSet);
        }
    }
}