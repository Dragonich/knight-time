﻿// AUTHOR: Benjamin Chaves

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Battle
{
    public class RigAnimator
    {
        /// <summary>
        /// The Animator component on the character GameObject.
        /// </summary>
        private Animator characterAnimator;

        /// <summary>
        /// The animations which are available to be played.
        /// </summary>
        public enum CharacterAnimations
        {
            None,
            Attack,
            Heal,
            Recoil,
            Win,
            Death,
            Duckify
        }

        /// <summary>
        /// The hash-value for the attack trigger name.
        /// </summary>
        private static int attackTriggerHash;

        /// <summary>
        /// The hash-value for the heal trigger name.
        /// </summary>
        private static int healTriggerHash;

        /// <summary>
        /// The hash-value for the recoil trigger name.
        /// </summary>
        private static int recoilTriggerHash;

        /// <summary>
        /// The hash-value for the unfreeze trigger name.
        /// </summary>
        private static int unfreezeTriggerHash;

        /// <summary>
        /// The hash-value for the win trigger name.
        /// </summary>
        private static int winTriggerHash;

        /// <summary>
        /// The hash-value for the death trigger name.
        /// </summary>
        private static int deathTriggerHash;

        /// <summary>
        /// The hash-value for the duckify trigger name.
        /// </summary>
        private static int duckifyTriggerHash;

        /// <summary>
        /// Applies the given trigger hashes to the class's static variables.
        /// </summary>
        /// <param name="inputAttackTriggerHash">The input hash-value for the attack trigger name.</param>
        /// <param name="inputHealTriggerHash">The input hash-value for the heal trigger name.</param>
        /// <param name="inputRecoilTriggerHash">The input hash-value for the recoil trigger name.</param>
        /// <param name="inputDuckifyTriggerHash">The input hash-value for the duckify trigger name.</param>
        /// <param name="inputUnfreezeTriggerHash">The input hash-value for the unfreeze trigger name.</param>
        /// <param name="inputWinTriggerHash">The input hash-value for the win trigger name.</param>
        /// <param name="inputDeathTriggerHash">The input hash-value for the death trigger name.</param>
        public static void SetTriggerHashes(int inputAttackTriggerHash, int inputHealTriggerHash, int inputRecoilTriggerHash, int inputDuckifyTriggerHash, int inputUnfreezeTriggerHash, int inputWinTriggerHash, int inputDeathTriggerHash)
        {
            attackTriggerHash = inputAttackTriggerHash;
            healTriggerHash = inputHealTriggerHash;
            recoilTriggerHash = inputRecoilTriggerHash;
            duckifyTriggerHash = inputDuckifyTriggerHash;

            unfreezeTriggerHash = inputUnfreezeTriggerHash;

            winTriggerHash = inputWinTriggerHash;
            deathTriggerHash = inputDeathTriggerHash;
        }

        public RigAnimator(GameObject characterRig)
        {
            Debug.Assert(characterRig != null);

            characterAnimator = characterRig.GetComponent<Animator>(); // Cache the Animator component on the character GameObject.
            Debug.Assert(characterAnimator != null); // Assert that the component was found.
        }

        /// <summary>
        /// Triggers the character to be unfrozen after the character cameos.
        /// </summary>
        public void UnfreezeCharacter()
        {
            Debug.Assert(characterAnimator != null);

            characterAnimator.SetTrigger(unfreezeTriggerHash); // Trigger the character to transition to its Idle animation.
        }

        /// <summary>
        /// Triggers the given animation to play on the character.
        /// </summary>
        /// <param name="animationToTrigger">The animation which should be played on the character.</param>
        public void TriggerAnimation(CharacterAnimations animationToTrigger)
        {
            Debug.Assert(animationToTrigger != CharacterAnimations.None); // Assert that the None enum item has not been passed-in.

            Debug.Assert(characterAnimator != null);

            switch (animationToTrigger)
            {
                case CharacterAnimations.Attack:

                    characterAnimator.SetTrigger(attackTriggerHash); // Trigger the Attack animation to be played.
                    break;

                case CharacterAnimations.Heal:

                    characterAnimator.SetTrigger(healTriggerHash); // Trigger the Heal animation to be played.
                    break;

                case CharacterAnimations.Recoil:

                    characterAnimator.SetTrigger(recoilTriggerHash); // Trigger the Recoil animation to be played.
                    break;

                case CharacterAnimations.Win:

                    characterAnimator.SetTrigger(winTriggerHash); // Trigger the Win animation to be played.
                    break;

                case CharacterAnimations.Death:

                    characterAnimator.SetTrigger(deathTriggerHash); // Trigger the Death animation to be played.
                    break;

                case CharacterAnimations.Duckify:

                    characterAnimator.SetTrigger(duckifyTriggerHash); // Trigger the Duckify animation to be played.
                    break;

                default:

                    Debug.LogError(animationToTrigger);
                    break;
            }
        }
    }
}