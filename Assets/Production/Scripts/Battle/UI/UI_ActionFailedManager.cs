﻿// AUTHOR: Benjamin Chaves

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using TMPro;

namespace Battle
{
    public class UI_ActionFailedManager : MonoBehaviour
    {
        [Header("Override Items")]

        public GameObject ActionFailedParent;
        private RectTransform actionFailedParentRectTransform;

        public GameObject ActionFailedText;
        private TextMeshProUGUI actionFailedTextComponent;

        [Header("Prefab Items")]

        [Tooltip("The seconds for which the banner holds on-screen between animations.")]
        public float HoldSeconds;

        [Tooltip("The speed with which the banner animates up & down the screen.")]
        public float MovementSpeed;

        [Tooltip("The text displayed on the banner when the user has failed a strike action.")]
        public string StrikeFailedText;

        [Tooltip("The text displayed on the banner when the user has failed a potion action.")]
        public string PotionFailedText;

        /// <summary>
        /// Reserved Vector2 for assigning a new position.
        /// </summary>
        private Vector2 newPositionSetter;

        private Coroutine holdOnScreenCoroutine;
        private WaitForSeconds waitForDisplaySeconds;

        /// <summary>
        /// The current animation state of the action failed graphics.
        /// </summary>
        private AnimationStates animationState;

        /// <summary>
        /// The animation states in which the action failed graphics can be.
        /// </summary>
        private enum AnimationStates
        {
            None,
            AnimatingIn,
            Holding,
            AnimatingOut
        }

        public void Initialise()
        {
            AssertInspectorInputs();

            CacheSubordinates();

            SetParentOffscreen();

            waitForDisplaySeconds = new WaitForSeconds(HoldSeconds);
        }

        /// <summary>
        /// Assert that all Inspector fields have been set or filled.
        /// </summary>
        private void AssertInspectorInputs()
        {
            Debug.Assert(StrikeFailedText != "");
            Debug.Assert(PotionFailedText != "");

            Debug.Assert(MovementSpeed > 0f);
            Debug.Assert(HoldSeconds > 0f);

            Debug.Assert(ActionFailedText != null);
            Debug.Assert(ActionFailedParent != null);
        }

        private void CacheSubordinates()
        {
            Debug.Assert(ActionFailedText != null);
            Debug.Assert(ActionFailedParent != null);

            actionFailedParentRectTransform = ActionFailedParent.GetComponent<RectTransform>();
            Debug.Assert(actionFailedParentRectTransform != null); // Assert that the component was found.

            actionFailedTextComponent = ActionFailedText.GetComponent<TextMeshProUGUI>();
            Debug.Assert(actionFailedTextComponent != null); // Assert that the component was found.
        }

        private void Update()
        {
            if(animationState != AnimationStates.None)
            {
                if (animationState == AnimationStates.AnimatingIn)
                {
                    MoveParentOnscreen();
                }
                else if (animationState == AnimationStates.AnimatingOut)
                {
                    MoveParentOffscreen();
                }
            }
        }

        /// <summary>
        /// Sets the graphics parent to be positioned off-screen.
        /// </summary>
        private void SetParentOffscreen()
        {
            Debug.Assert(actionFailedParentRectTransform != null);

            newPositionSetter.y = actionFailedParentRectTransform.sizeDelta.y;

            actionFailedParentRectTransform.anchoredPosition = newPositionSetter;
        }

        /// <summary>
        /// Activates the failure text with the appropriate text,
        /// depending on the given action which has been failed.
        /// </summary>
        /// <param name="actionFailed">The action which has been failed.</param>
        public void ActivateText(MouseActionsManager.Symbols actionFailed)
        {
            Debug.Assert(actionFailed != MouseActionsManager.Symbols.None);

            SetActionText(actionFailed);

            if(holdOnScreenCoroutine != null)
            {
                StopCoroutine(holdOnScreenCoroutine);
            }

            animationState = AnimationStates.AnimatingIn;
        }

        /// <summary>
        /// Sets the banner text based on the given action which was failed.
        /// </summary>
        /// <param name="actionFailed">The action which has was failed.</param>
        private void SetActionText(MouseActionsManager.Symbols actionFailed)
        {
            Debug.Assert(actionFailed != MouseActionsManager.Symbols.None);

            Debug.Assert(StrikeFailedText != "");
            Debug.Assert(PotionFailedText != "");
            Debug.Assert(actionFailedTextComponent != null);

            switch (actionFailed)
            {
                case MouseActionsManager.Symbols.Sword:

                    actionFailedTextComponent.text = StrikeFailedText;
                    break;

                case MouseActionsManager.Symbols.Potion:

                    actionFailedTextComponent.text = PotionFailedText;
                    break;

                default:

                    Debug.LogError(actionFailed);
                    break;
            }
        }

        /// <summary>
        /// Moves the graphics parent towards its on-screen position.
        /// </summary>
        private void MoveParentOnscreen()
        {
            Debug.Assert(actionFailedParentRectTransform != null);

            float positionY = actionFailedParentRectTransform.anchoredPosition.y;
            newPositionSetter.y = positionY;

            if (Mathf.Approximately(positionY, 0f))
            {
                animationState = AnimationStates.Holding;

                holdOnScreenCoroutine = StartCoroutine(HoldOnScreen());
            }
            else
            {
                newPositionSetter.y = Mathf.MoveTowards(positionY, 0f, MovementSpeed * Time.deltaTime);

                actionFailedParentRectTransform.anchoredPosition = newPositionSetter;
            }
        }

        /// <summary>
        /// Holds the banner on the screen for the set number of seconds.
        /// </summary>
        /// <returns></returns>
        private IEnumerator HoldOnScreen()
        {
            yield return waitForDisplaySeconds;

            animationState = AnimationStates.AnimatingOut;

            holdOnScreenCoroutine = null;
        }

        /// <summary>
        /// Moves the graphics parent towards its off-screen position.
        /// </summary>
        private void MoveParentOffscreen()
        {
            Debug.Assert(actionFailedParentRectTransform != null);

            float positionY = actionFailedParentRectTransform.anchoredPosition.y;
            float sizeDeltaY = actionFailedParentRectTransform.sizeDelta.y;

            newPositionSetter.y = positionY;

            if (Mathf.Approximately(positionY, sizeDeltaY))
            {
                animationState = AnimationStates.None;
            }
            else
            {
                newPositionSetter.y = Mathf.MoveTowards(positionY, sizeDeltaY, MovementSpeed * Time.deltaTime);

                actionFailedParentRectTransform.anchoredPosition = newPositionSetter;
            }
        }
    }
}