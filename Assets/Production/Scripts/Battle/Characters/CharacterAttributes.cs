﻿// AUTHOR: Benjamin Chaves

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Battle
{
    [System.Serializable]
    public class CharacterAttributes
    {
        [Header("Rig")]

        [Tooltip("The rigged prefab for the character.")]
        public GameObject CharacterPrefab;

        [Tooltip("The character's object scale.")]
        public float CharacterObjectScale;

        [Tooltip("The character's Y-position offset.")]
        public float CharacterObjectOffset;

        [Header("Attributes")]
        
        [Tooltip("The name of the character.")]
        public string Name;

        [Tooltip("The maximum health of the character in battle.")]
        public uint MaxHealth;

        [Tooltip("The lower inclusive value in the character's damage roll.")]
        public int DamageRangeLower;

        [Tooltip("The upper inclusive value in the character's damage roll.")]
        public int DamageRangeUpper;

        [Header("Skill Cards")]
        
        [Tooltip("Defines whether the character has this Skill Card.")]
        public bool HasDoubleAttack;

        [Tooltip("Defines whether the character has this Skill Card.")]
        public bool HasFastAttack;

        [Tooltip("Defines whether the character has this Skill Card.")]
        public bool HasPotion;

        [Tooltip("Defines whether the character has this Skill Card.")]
        public bool HasDrainAttack;

        [Tooltip("Defines whether the character has this Skill Card.")]
        public bool HasDragonAttack;

        /// <summary>
        /// Assert that all Inspector fields have been set or filled.
        /// </summary>
        public void AssertInspectorInputs()
        {
            Debug.Assert(Name != "");

            Debug.Assert(CharacterPrefab != null);
            Debug.Assert(CharacterObjectScale > 0f);

            Debug.Assert(DamageRangeLower >= 0);
            Debug.Assert(DamageRangeUpper > 0);

            Debug.Assert(DamageRangeLower <= DamageRangeUpper);
        }

        /// <summary>
        /// Returns the randomised damage roll for this character.
        /// </summary>
        /// <param name="randomGenerator"></param>
        public uint RollDamageValue(System.Random randomGenerator)
        {
            Debug.Assert(randomGenerator != null);

            return (uint)randomGenerator.Next(DamageRangeLower, DamageRangeUpper + 1);
        }

        /// <summary>
        /// Returns whether the character has the given card.
        /// </summary>
        public bool GetHasCard(int cardID)
        {
            Debug.Assert(cardID >= 0, cardID);

            switch(cardID)
            {
                case 0:

                    return true; // Return true, as every character has the standard attack card by default.

                case 1:

                    return HasDoubleAttack;

                case 2:

                    return HasFastAttack;

                case 3:

                    return HasPotion;

                case 4:

                    return HasDrainAttack;

                case 5:

                    return HasDragonAttack;

                default:

                    Debug.LogError(cardID);
                    return false;
            }
        }
    }
}