﻿// AUTHOR: Benjamin Chaves

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SaveFile
{
    public class SaveFileData
    {
        public BattleStatus Battle1Status { get; private set; }
        public BattleStatus Battle2Status { get; private set; }
        public BattleStatus Battle3Status { get; private set; }

        public int PlayerMaxHealth { get; private set; }

        /// <summary>
        /// Whether the user has the Double Attack ability.
        /// </summary>
        public bool HasDoubleAttack { get; private set; }

        /// <summary>
        /// Whether the user has the Fast Attack ability.
        /// </summary>
        public bool HasFastAttack { get; private set; }

        /// <summary>
        /// Whether the user has the Potion ability.
        /// </summary>
        public bool HasPotion { get; private set; }

        /// <summary>
        /// Whether the user has the Drain Attack ability.
        /// </summary>
        public bool HasDrainAttack { get; private set; }

        public enum BattleStatus
        {
            Unattempted,
            Attempted,
            Completed,
        }

        public SaveFileData(BattleStatus inputBattle1Status, BattleStatus inputBattle2Status, BattleStatus inputBattle3Status, int inputPlayerMaxHealth, bool inputHasDoubleAttack, bool inputHasFastAttack, bool inputHasPotion, bool inputHasDrainAttack)
        {
            Debug.Assert(inputPlayerMaxHealth > 0, inputPlayerMaxHealth);

            Battle1Status = inputBattle1Status;
            Battle2Status = inputBattle2Status;
            Battle3Status = inputBattle3Status;

            PlayerMaxHealth = inputPlayerMaxHealth;

            HasDoubleAttack = inputHasDoubleAttack;
            HasFastAttack = inputHasFastAttack;
            HasPotion = inputHasPotion;
            HasDrainAttack = inputHasDrainAttack;
        }
    }
}