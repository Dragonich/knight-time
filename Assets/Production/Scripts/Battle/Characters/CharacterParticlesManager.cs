﻿// AUTHOR: Benjamin Chaves

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Battle
{
    public class CharacterParticlesManager
    {
        private ParticleSystem healParticleSystem;
        private ParticleSystem damageParticleSystem;
        private ParticleSystem duckifyParticleSystem;

        public CharacterParticlesManager(GameObject inputDamageEffect, GameObject inputHealEffect, GameObject inputDuckifyEffect, Vector3 characterPosition, bool isPlayedCharacter)
        {
            Debug.Assert(inputHealEffect != null);
            Debug.Assert(inputDamageEffect != null);
            Debug.Assert(inputDuckifyEffect != null);

            characterPosition.y += 0.2f;
            inputHealEffect.transform.position = characterPosition;

            characterPosition.y += 1.4f;
            inputDamageEffect.transform.position = characterPosition;

            damageParticleSystem = inputDamageEffect.GetComponent<ParticleSystem>();
            Debug.Assert(damageParticleSystem != null); // Assert that the component was found.

            healParticleSystem = inputHealEffect.GetComponent<ParticleSystem>();
            Debug.Assert(healParticleSystem != null); // Assert that the component was found.

            characterPosition.y -= 0.1f;
            inputDuckifyEffect.transform.position = characterPosition;

            duckifyParticleSystem = inputDuckifyEffect.GetComponent<ParticleSystem>();
            Debug.Assert(duckifyParticleSystem != null); // Assert that the component was found.

            ApplyHealRotationCorrection(isPlayedCharacter);
        }

        /// <summary>
        /// Applies a rotation correction to the healing particle effect
        /// depending on whether this is the player character.
        /// </summary>
        /// <param name="isPlayerCharacter">Whether this is the player character.</param>
        private void ApplyHealRotationCorrection(bool isPlayerCharacter)
        {
            Debug.Assert(healParticleSystem != null);

            GameObject particleSystemParent = healParticleSystem.gameObject;

            Vector3 parentRotation = particleSystemParent.transform.eulerAngles;
            parentRotation.z += isPlayerCharacter ? 4f : -4f;

            particleSystemParent.transform.eulerAngles = parentRotation;
        }

        public void PlayDamageEffect()
        {
            Debug.Assert(damageParticleSystem != null);

            damageParticleSystem.Play();
        }

        public void PlayHealEffect()
        {
            Debug.Assert(healParticleSystem != null);

            healParticleSystem.Play();
        }

        public void PlayDuckifyEffect()
        {
            Debug.Assert(duckifyParticleSystem != null);

            duckifyParticleSystem.Play();
        }
    }
}