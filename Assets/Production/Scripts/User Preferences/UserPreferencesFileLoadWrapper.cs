﻿// AUTHOR: Benjamin Chaves

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UserPreferences
{
    public class UserPreferencesFileLoadWrapper
    {
        public LoadResults LoadResult { get; private set; }
        public UserPreferencesFileData UserPreferenceFileData { get; private set; }

        /// <summary>
        /// The possible results from attempting to load the data from disk.
        /// </summary>
        public enum LoadResults
        {
            Successful,
            FileIncomplete,
            FileNotFound,
        }

        public UserPreferencesFileLoadWrapper(LoadResults inputLoadResult, UserPreferencesFileData inputUserPreferencesFileData)
        {
            Debug.Assert(inputUserPreferencesFileData != null);

            LoadResult = inputLoadResult;

            UserPreferenceFileData = inputUserPreferencesFileData;
        }
    }
}