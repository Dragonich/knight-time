﻿// AUTHOR: Benjamin Chaves

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WinSceneManager : MonoBehaviour
{
    public string CreditsSceneName;

    public float SceneChangeDelaySeconds;

    private WaitForSeconds waitForSceneChangeSeconds;

    private void Start()
    {
        AssertInspectorInputs();

        waitForSceneChangeSeconds = new WaitForSeconds(SceneChangeDelaySeconds);

        StartCoroutine(SceneChangeDelay());
    }

    private void AssertInspectorInputs()
    {
        Debug.Assert(CreditsSceneName != "");

        Debug.Assert(SceneChangeDelaySeconds > 0f);
    }

    private IEnumerator SceneChangeDelay()
    {
        yield return waitForSceneChangeSeconds;

        UnityEngine.SceneManagement.SceneManager.LoadScene(CreditsSceneName);
    }
}