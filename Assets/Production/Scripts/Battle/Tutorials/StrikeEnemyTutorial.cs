﻿// AUTHOR: Benjamin Chaves

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Battle
{
    public class StrikeEnemyTutorial : MonoBehaviour
    {
        [Header("Override Items")]

        public GameObject GO_Camera;

        /// <summary>
        /// Cached reference to the Camera component of the camera GameObject.
        /// </summary>
        private Camera cameraComponent;

        [Header("Prefab Items")]

        [Tooltip("The speed at which the strike streak should fade.")]
        public float StrikeFadeSpeed;

        [Tooltip("The GameObject which holds the strike streak Line Renderer as a component.")]
        public GameObject GO_StrikeStreak;
        private LineRenderer strikeStreak;

        private TutorialsManager tutorialsManager;
        private TutorialCursorManager tutorialCursorManager;

        /// <summary>
        /// Whether this tutorial is active.
        /// </summary>
        public bool TutorialActive { get; private set; }
        
        /// <summary>
        /// A Vector2 reserved for assigning a new vertex to the strike streak.
        /// </summary>
        private Vector2 newStreakVertexPosition;

        /// <summary>
        /// A Vector2 reserved for assigning a new position to the cursor.
        /// </summary>
        private Vector2 cursorPositionSetter;

        /// <summary>
        /// A Vector2 reserved for assigning a new position to the cursor target.
        /// </summary>
        private Vector2 cursorPositionTarget;

        private WaitForEndOfFrame waitForEndOfFrame;
        private WaitForSeconds waitForTutorialSeconds;
        private WaitForSeconds waitForTutorialSecondsHalf;

        private Coroutine holdOnScreenCentreCoroutine;
        private Coroutine moveToStrikeStartCoroutine;
        private Coroutine holdOnStrikeStartCoroutine;
        private Coroutine moveToStrikeEndCoroutine;
        private Coroutine holdOnStrikeEndCoroutine;
        private Coroutine holdInvisibleCoroutine;

        public void Initialise(TutorialsManager inputTutorialsManager, GameObject inputTutorialCursorPrefab, GameObject inputTutorialCursorParent)
        {
            Debug.Assert(inputTutorialsManager != null);
            Debug.Assert(inputTutorialCursorPrefab != null);
            Debug.Assert(inputTutorialCursorParent != null);

            AssertInspectorInputs();

            tutorialsManager = inputTutorialsManager;

            InstantiateTutorialCursor(inputTutorialCursorPrefab, inputTutorialCursorParent);

            CacheSubordinates();

            waitForEndOfFrame = new WaitForEndOfFrame();
            waitForTutorialSeconds = new WaitForSeconds(tutorialsManager.CursorHoldSeconds);
            waitForTutorialSecondsHalf = new WaitForSeconds(tutorialsManager.CursorHoldSeconds / 2f);
        }

        /// <summary>
        /// Assert that all Inspector fields have been set or filled.
        /// </summary>
        private void AssertInspectorInputs()
        {
            Debug.Assert(GO_Camera != null);
            Debug.Assert(GO_StrikeStreak != null);

            Debug.Assert(StrikeFadeSpeed > 0f);
        }

        private void CacheSubordinates()
        {
            cameraComponent = GO_Camera.GetComponent<Camera>();
            Debug.Assert(cameraComponent != null); // Assert that the component was found.

            strikeStreak = GO_StrikeStreak.GetComponent<LineRenderer>();
            Debug.Assert(strikeStreak != null); // Assert that the component was found.
        }

        /// <summary>
        /// Instantiates the tutorial cursor for this tutorial.
        /// </summary>
        /// <param name="inputTutorialCursorPrefab">The prefab of the tutorial cursor GameObject to be instantiated.</param>
        /// <param name="inputTutorialCursorsParent">The GameObject which should be the parent of the instantiated tutorial cursor.</param>
        private void InstantiateTutorialCursor(GameObject inputTutorialCursorPrefab, GameObject inputTutorialCursorsParent)
        {
            Debug.Assert(inputTutorialCursorPrefab != null);
            Debug.Assert(inputTutorialCursorsParent != null);

            GameObject newTutorialCursor = Instantiate(inputTutorialCursorPrefab);

            newTutorialCursor.SetActive(false);
            newTutorialCursor.name = "Strike Enemy Tutorial Cursor";
            newTutorialCursor.transform.SetParent(inputTutorialCursorsParent.transform);

            tutorialCursorManager = newTutorialCursor.GetComponent<TutorialCursorManager>();
            Debug.Assert(tutorialCursorManager != null); // Assert that the component was found.

            tutorialCursorManager.Initialise();
        }

        /// <summary>
        /// Starts this tutorial.
        /// </summary>
        public void StartTutorial()
        {
            Debug.Assert(tutorialCursorManager != null);
            Debug.Assert(!TutorialActive); // Assert that the tutorial is not already active.

            TutorialActive = true;
            tutorialCursorManager.SetMouseClicked(false);

            holdOnScreenCentreCoroutine = StartCoroutine(HoldOnScreenCentre());
        }

        /// <summary>
        /// Ends this tutorial.
        /// </summary>
        public void EndTutorial()
        {
            Debug.Assert(TutorialActive);
            Debug.Assert(strikeStreak != null);
            Debug.Assert(tutorialCursorManager != null);

            TutorialActive = false;

            StopCoroutines();

            strikeStreak.positionCount = 0;
            tutorialCursorManager.SetActive(false);
        }

        private void StopCoroutines()
        {
            StopActiveCoroutine(holdOnScreenCentreCoroutine);

            StopActiveCoroutine(moveToStrikeStartCoroutine);
            StopActiveCoroutine(holdOnStrikeStartCoroutine);

            StopActiveCoroutine(moveToStrikeEndCoroutine);
            StopActiveCoroutine(holdOnStrikeEndCoroutine);

            StopActiveCoroutine(holdInvisibleCoroutine);
        }

        private void StopActiveCoroutine(Coroutine coroutineToStop)
        {
            if(coroutineToStop != null)
            {
                StopCoroutine(coroutineToStop);
            }
        }

        private IEnumerator HoldOnScreenCentre()
        {
            Debug.Assert(tutorialCursorManager != null);

            tutorialCursorManager.ZeroPosition();
            tutorialCursorManager.SetActive(true);

            yield return waitForTutorialSeconds;

            moveToStrikeStartCoroutine = StartCoroutine(MoveToStrikeStart());
            holdOnScreenCentreCoroutine = null;
        }

        private IEnumerator MoveToStrikeStart()
        {
            Debug.Assert(tutorialsManager != null);
            Debug.Assert(tutorialCursorManager != null);

            cursorPositionTarget.x = 1920f * 0.1f;
            cursorPositionTarget.y = 1080f * 0.2f;

            while (!TutorialsManager.VectorsApproximatelyEqual(tutorialCursorManager.GetAnchoredPosition(), cursorPositionTarget)) // The cursor has not reached its target.
            {
                float movementSpeed = tutorialsManager.CursorMovementSpeed * Time.deltaTime;
                cursorPositionSetter = Vector2.MoveTowards(tutorialCursorManager.GetAnchoredPosition(), cursorPositionTarget, movementSpeed); // Move the cursor towards its target.

                tutorialCursorManager.SetPosition(cursorPositionSetter);

                yield return waitForEndOfFrame;
            }

            holdOnStrikeStartCoroutine = StartCoroutine(HoldOnStrikeStart());
            moveToStrikeEndCoroutine = null;
        }

        private IEnumerator HoldOnStrikeStart()
        {
            Debug.Assert(tutorialCursorManager != null);

            yield return waitForTutorialSecondsHalf;

            tutorialCursorManager.SetMouseClicked(true);

            yield return waitForTutorialSecondsHalf;

            moveToStrikeEndCoroutine = StartCoroutine(MoveToStrikeEnd());
            holdOnStrikeStartCoroutine = null;
        }

        private IEnumerator MoveToStrikeEnd()
        {
            Debug.Assert(tutorialsManager != null);
            Debug.Assert(tutorialCursorManager != null);

            cursorPositionTarget.x = 1920f * 0.35f;
            cursorPositionTarget.y = 1080f * -0.2f;

            while (!TutorialsManager.VectorsApproximatelyEqual(tutorialCursorManager.GetAnchoredPosition(), cursorPositionTarget)) // The cursor has not reached its target.
            {
                float movementSpeed = tutorialsManager.CursorMovementSpeed * Time.deltaTime;
                cursorPositionSetter = Vector2.MoveTowards(tutorialCursorManager.GetAnchoredPosition(), cursorPositionTarget, movementSpeed); // Move the cursor towards its target.

                tutorialCursorManager.SetPosition(cursorPositionSetter);

                AddStreakPoint(tutorialCursorManager.GetPosition());

                yield return waitForEndOfFrame;
            }

            holdOnStrikeEndCoroutine = StartCoroutine(HoldOnStrikeEnd());
            moveToStrikeEndCoroutine = null;
        }

        /// <summary>
        /// Adds a vertex to the streak based on the given tutorial cursor position.
        /// </summary>
        /// <param name="tutorialCursorPosition">The position of the tutorial cursor.</param>
        private void AddStreakPoint(Vector2 tutorialCursorPosition)
        {
            Debug.Assert(strikeStreak != null);
            Debug.Assert(cameraComponent != null);

            strikeStreak.positionCount++;

            newStreakVertexPosition = cameraComponent.ScreenToWorldPoint(new Vector3(tutorialCursorPosition.x, tutorialCursorPosition.y, cameraComponent.nearClipPlane));
            newStreakVertexPosition *= 13f;

            strikeStreak.SetPosition(strikeStreak.positionCount - 1, newStreakVertexPosition);
        }

        private IEnumerator HoldOnStrikeEnd()
        {
            Debug.Assert(tutorialCursorManager != null);

            yield return waitForTutorialSecondsHalf;

            tutorialCursorManager.SetMouseClicked(false);
            
            yield return waitForTutorialSecondsHalf;

            holdInvisibleCoroutine = StartCoroutine(HoldInvisible());
            holdOnStrikeEndCoroutine = null;
        }

        private IEnumerator HoldInvisible()
        {
            Debug.Assert(tutorialCursorManager != null);

            StartCoroutine(FadeOutLine());
            tutorialCursorManager.SetActive(false);

            yield return waitForTutorialSeconds;

            holdOnScreenCentreCoroutine = StartCoroutine(HoldOnScreenCentre());
            holdInvisibleCoroutine = null;
        }

        /// <summary>
        /// Fades out the strike streak line to be invisible.
        /// </summary>
        private IEnumerator FadeOutLine()
        {
            Debug.Assert(strikeStreak != null);

            Color newLineColour = Color.red;

            while (newLineColour.a > 0f) // While the line's opacity is above 0.
            {
                newLineColour.a -= StrikeFadeSpeed * Time.deltaTime; // Decrease the value of the line's opacity.
                strikeStreak.material.SetColor("_BaseColor", newLineColour); // Apply the new line colour to the line.

                yield return null; // Pause the coroutine until the next frame.
            }

            strikeStreak.positionCount = 0; // Clear all positions from the line renderer.

            newLineColour.a = 1f;
            strikeStreak.material.SetColor("_BaseColor", newLineColour); // Apply the new line colour to the line.
        }
    }
}