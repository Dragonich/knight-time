﻿// AUTHOR: Benjamin Chaves

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.UI;

namespace Battle
{
    public class CardsManager : MonoBehaviour
    {
        private AppManager appManager;

        [Tooltip("The GameObject which holds the Cards Database as a component.")]
        public GameObject GO_CardsDatabase;

        [Tooltip("The GameObject which holds the Tabled Cards Manager as a component.")]
        public GameObject GO_TabledCardsManager;

        private CardsDatabase cardsDatabase;
        private TabledCardsManager tabledCardsManager;

        public void InitialiseStaticSubordinates(AppManager inputAppManager)
        {
            Debug.Assert(inputAppManager != null);
            appManager = inputAppManager;

            cardsDatabase = GO_CardsDatabase.GetComponent<CardsDatabase>();
            Debug.Assert(cardsDatabase != null); // Assert that the component was found.

            cardsDatabase.Initialise();
        }

        public void InitialiseDynamicSubordinates()
        {
            Debug.Assert(GO_TabledCardsManager != null);

            tabledCardsManager = GO_TabledCardsManager.GetComponent<TabledCardsManager>();
            Debug.Assert(tabledCardsManager != null); // Assert that the component was found.

            tabledCardsManager.Initialise(appManager.BattleManager.GetBattleCharactersQuantity());
        }

        /// <summary>
        /// Returns the number of cards in the game.
        /// </summary>
        public int GetCardsQuantity()
        {
            Debug.Assert(cardsDatabase != null);

            return cardsDatabase.GetCardsQuantity();
        }

        /// <summary>
        /// Returns the Card for the given card ID.
        /// </summary>
        /// <param name="cardIndex">The ID of the Card which should be returned.</param>
        public Card GetDatabaseCardByID(int cardID)
        {
            Debug.Assert(cardID >= 0, cardID);

            Debug.Assert(cardsDatabase != null);

            return cardsDatabase.GetCardByID(cardID);
        }

        /// <summary>
        /// Returns the Card for the given card enum.
        /// </summary>
        /// <param name="cardToGet">The enum of the Card which should be returned.</param>
        public Card GetDatabaseCardByEnum(CardsDatabase.CardTypes cardToGet)
        {
            Debug.Assert(cardsDatabase != null);

            return cardsDatabase.GetCardByEnum(cardToGet);
        }

        /// <summary>
        /// Clears any cards currently played to the table.
        /// </summary>
        public void ClearTabledCards()
        {
            Debug.Assert(tabledCardsManager != null);

            tabledCardsManager.ClearTabledCards();
        }

        /// <summary>
        /// Plays a card to the table, being cast from a given character, and targeting another given character.
        /// </summary>
        /// <param name="battleCharacterID">The character playing the card to the table.</param>
        /// <param name="cardToTable">The card which should be played to the table.</param>
        /// <param name="characterTarget">The character who is being targeted by the played card.</param>
        public void PlayCardToTable(int battleCharacterID, Card cardToTable, int characterTarget)
        {
            Debug.Assert(battleCharacterID >= 0, battleCharacterID);
            Debug.Assert(cardToTable != null);
            Debug.Assert(characterTarget >= 0, characterTarget);

            Debug.Assert(appManager != null);
            Debug.Assert(appManager.UIManager != null);
            Debug.Assert(tabledCardsManager != null);

            tabledCardsManager.SetCharacterTabledCard(battleCharacterID, cardToTable, characterTarget);

            if(battleCharacterID == 0) // The player is playing a card to the table.
            {
                appManager.UIManager.UpdateHandCardsInformation();
            }
        }

        /// <summary>
        /// Returns the card which the player character has tabled.
        /// </summary>
        public Card GetPlayerTabledCard()
        {
            Debug.Assert(tabledCardsManager != null);

            TabledCard playerTabledCard = tabledCardsManager.GetTabledCard(0);
            Debug.Assert(playerTabledCard != null);

            return playerTabledCard.Card;
        }

        /// <summary>
        /// Activates the mouse symbol for the player's tabled card.
        /// </summary>
        public void ActivatePlayerCardMouseSymbol()
        {
            Debug.Assert(appManager != null);
            Debug.Assert(appManager.MouseActionsManager != null);
            Debug.Assert(tabledCardsManager != null);

            MouseActionsManager.Symbols playerTabledCardSymbol = tabledCardsManager.GetPlayerCardSymbol();

            appManager.MouseActionsManager.ActivateSymbol(playerTabledCardSymbol);
        }

        /// <summary>
        /// Resolves the player's tabled card.
        /// </summary>
        /// <param name="characterHealthModifiers">The referenced array of each character's health modifier.</param>
        public void ResolvePlayerCard(ref float[] characterHealthModifiers)
        {
            TabledCard playerTabledCard = tabledCardsManager.GetTabledCard(0); // Cache the player's card.

            characterHealthModifiers[0] += playerTabledCard.Card.HealingAdder; // Add the healing value of the player's card to the player's health.

            float damageValue = appManager.CharactersManager.GetRolledCharacterDamageValue(0); // Cache the player's damage roll.
            damageValue *= playerTabledCard.Card.OffensiveMultiplier; // Multiply the player's damage roll by the multiplier of the player's card.

            damageValue += playerTabledCard.Card.OffensiveAdder; // Add the additional damage of the player's card to their damage.

            characterHealthModifiers[playerTabledCard.TargetedCharacter] -= damageValue; // Deduct the player's damage from the enemy's health.

            if (playerTabledCard.Card.HealsDamageCaused) // The player's card heals the player for the damage it causes.
            {
                characterHealthModifiers[0] += Mathf.Abs(characterHealthModifiers[playerTabledCard.TargetedCharacter]);
            }

            if(appManager.BattleManager.WriteTurnDetailsToLog) // The details of this turn should be written to the debug log.
            {
                Debug.Log("TURN " + appManager.BattleManager.GetTurnID() + ": Player plays \"" + playerTabledCard.Card.CardName + "\" for " + damageValue + " damage. Player heals " + characterHealthModifiers[0] + ".");
            }

            if (playerTabledCard.Card.CharacterAnimation != RigAnimator.CharacterAnimations.None) // The played card has an associated character animation.
            {
                appManager.CharactersManager.TriggerCharacterAnimation(0, playerTabledCard.Card.CharacterAnimation); // Trigger the card's animation to play on the player.
            }

            appManager.SoundEffectsManager.CardsSoundEffects.PlayCardSoundEffect(CharactersDatabase.Characters.Flavo, playerTabledCard.Card.CardType); // Trigger Flavo's sound effect for the card to be played.
        }

        /// <summary>
        /// Resolves the enemy's tabled card.
        /// </summary>
        /// <param name="characterHealthModifiers">The referenced array of each character's health modifier.</param>
        public void ResolveEnemyCards(ref float[] characterHealthModifiers)
        {
            for(int enemyCharacterIndex = 1; enemyCharacterIndex < appManager.BattleManager.GetBattleCharactersQuantity(); enemyCharacterIndex++) // Iterate through each enemy character.
            {
                TabledCard currentEnemyTabledCard = tabledCardsManager.GetTabledCard(enemyCharacterIndex); // Cache the enemy's card.

                characterHealthModifiers[enemyCharacterIndex] += currentEnemyTabledCard.Card.HealingAdder; // Add the healing value of the enemy's card to the enemy's health.

                float damageValue = appManager.CharactersManager.GetRolledCharacterDamageValue(enemyCharacterIndex); // Cache the enemy's damage roll.
                damageValue *= currentEnemyTabledCard.Card.OffensiveMultiplier; // Multiply the enemy's damage roll by the multiplier of the enemy's card.

                damageValue += currentEnemyTabledCard.Card.OffensiveAdder; // Add the additional damage of the enemy's card to their damage.

                characterHealthModifiers[0] -= damageValue; // Deduct the enemy's damage from the players's health.

                if (currentEnemyTabledCard.Card.HealsDamageCaused) // The enemy's card heals the enemy for the damage it causes.
                {
                    characterHealthModifiers[enemyCharacterIndex] += Mathf.Abs(characterHealthModifiers[0]);
                }

                if (appManager.BattleManager.WriteTurnDetailsToLog) // The details of this turn should be written to the debug log.
                {
                    Debug.Log("TURN " + appManager.BattleManager.GetTurnID() + ": Enemy plays \"" + currentEnemyTabledCard.Card.CardName + "\" for " + damageValue + " damage. Enemy heals " + characterHealthModifiers[enemyCharacterIndex] + ".");
                }

                if (currentEnemyTabledCard.Card.DuckifiesTarget) // The played card will Duckify the player.
                {
                    appManager.CharactersManager.SetDuckFlavoActive(true);
                    appManager.CharactersManager.SetPlayerDuckified(appManager.CardsManager.cardsDatabase.PlayerTurnsDuckified);

                    if (appManager.BattleManager.WriteTurnDetailsToLog) // The details of this turn should be written to the debug log.
                    {
                        Debug.Log("TURN " + appManager.BattleManager.GetTurnID() + ": Player has been Duckified.");
                    }
                }

                if (currentEnemyTabledCard.Card.CharacterAnimation != RigAnimator.CharacterAnimations.None) // The played card has an associated character animation.
                {
                    appManager.CharactersManager.TriggerCharacterAnimation(enemyCharacterIndex, currentEnemyTabledCard.Card.CharacterAnimation); // Trigger the card's animation to play on the enemy.
                }

                if(currentEnemyTabledCard.Card == cardsDatabase.GetCardByEnum(CardsDatabase.CardTypes.DragonAttack)) // The enemy's card is the Dragon Attack card.
                {
                    appManager.CharactersManager.PlayEnemyDuckifyEffect();
                }

                CharactersDatabase.Characters enemyCharacter = appManager.BattleManager.GetEnemyCharacter(enemyCharacterIndex - 1);
                appManager.SoundEffectsManager.CardsSoundEffects.PlayCardSoundEffect(enemyCharacter, currentEnemyTabledCard.Card.CardType); // Trigger the enemy's sound effect for the card to be played.
            }
        }
    }
}