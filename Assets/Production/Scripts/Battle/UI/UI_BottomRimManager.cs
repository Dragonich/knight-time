﻿// AUTHOR: Benjamin Chaves

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Battle
{
    public class UI_BottomRimManager : MonoBehaviour
    {
        [Header("Override Items")]

        public GameObject BottomRim;

        public void Initialise()
        {
            AssertInspectorInputs();
        }

        /// <summary>
        /// Assert that all Inspector fields have been set or filled.
        /// </summary>
        private void AssertInspectorInputs()
        {
            Debug.Assert(BottomRim != null);
        }

        /// <summary>
        /// Sets whether the bottom rim graphic is active to the given boolean.
        /// </summary>
        public void SetBottomRimActive(bool newIsActive)
        {
            Debug.Assert(BottomRim != null);

            BottomRim.SetActive(newIsActive);
        }
    }
}