﻿// AUTHOR: Benjamin Chaves

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Pause;
using Battle;

public class CameosManager : MonoBehaviour
{
    private AppManager appManager;

    [Header("Prefab Items")]

    [Tooltip("The speed at which cameos should scroll.")]
    public float CameoSpeed;

    [Tooltip("The distance between each cameo item.")]
    public float CameoItemSpacing;

    [Tooltip("The prefab for John's cameo items.")]
    public GameObject JohnCameoPrefab;

    [Tooltip("The prefab for Flavo's cameo items.")]
    public GameObject FlavoCameoPrefab;

    [Tooltip("The prefab for Snailder's cameo items.")]
    public GameObject SnailderCameoPrefab;

    [Tooltip("The prefab for Bebek's cameo items.")]
    public GameObject DuckDragonCameoPrefab;

    [Header("Override Items")]

    [Tooltip("Whether the cameos should be skipped for this battle.")]
    public bool SkipCameos;

    public GameObject SkipCameosButton;

    [Tooltip("The GameObject which is the parent of this battle's cameo.")]
    public GameObject SeriesParent;

    /// <summary>
    /// Cached RectTransform component on the series parent GameObject.
    /// </summary>
    private RectTransform SeriesParentRectTrans;

    [Tooltip("The cameo items in this battle's cameo.")]
    public CameoItem[] Series;

    private UI_CameoAttributesManager[] seriesObjects;

    /// <summary>
    /// The index of the cameo item which should be focused.
    /// </summary>
    private int seriesTargetIndex = -1;

    /// <summary>
    /// Pre-allocated Vector3 for setting the series parent's new position.
    /// </summary>
    private Vector3 seriesParentPositionSetter;

    /// <summary>
    /// Whether the current battle has been attempted by the player.
    /// </summary>
    private bool currentBattleAttempted;

    /// <summary>
    /// The state the cameo series is currently in.
    /// </summary>
    private SeriesStates seriesState;

    /// <summary>
    /// The states in which the cameo series can be.
    /// </summary>
    private enum SeriesStates
    {
        /// <summary>
        /// The cameo series is holding on the current item.
        /// </summary>
        Holding,

        /// <summary>
        /// The cameo series is moving to focus on the current item.
        /// </summary>
        Moving,
    }

    public enum Characters
    {
        Flavo,
        Snailder,
        John,
        Bebek,
    }

    public void Initialise(AppManager inputAppManager)
    {
        Debug.Assert(inputAppManager != null);
        appManager = inputAppManager;

        AssertInspectorInputs();

        CacheSubordinates();

        InstantiateSeries();
        SeriesParent.SetActive(false);
        SkipCameosButton.SetActive(false);
    }

    private void AssertInspectorInputs()
    {
        Debug.Assert(SeriesParent != null);
        Debug.Assert(SkipCameosButton != null);

        Debug.Assert(CameoItemSpacing > 0f);
        Debug.Assert(CameoSpeed > 0f);

        Debug.Assert(FlavoCameoPrefab != null);
        Debug.Assert(SnailderCameoPrefab != null);
        Debug.Assert(JohnCameoPrefab != null);
        Debug.Assert(DuckDragonCameoPrefab != null);
    }

    private void CacheSubordinates()
    {
        Debug.Assert(SeriesParent != null);

        SeriesParentRectTrans = SeriesParent.GetComponent<RectTransform>();
        Debug.Assert(SeriesParentRectTrans != null); // Assert that the component was found.
    }

    /// <summary>
    /// Activates this battle's cameo series.
    /// </summary>
    public void ActivateCameoSeries()
    {
        Debug.Assert(SeriesParent != null);
        Debug.Assert(SkipCameosButton != null);

        if (Series.Length > 0) // This series has cameo items.
        {
            SeriesParent.SetActive(true);
            SkipCameosButton.SetActive(currentBattleAttempted);

            seriesTargetIndex++;
            seriesState = SeriesStates.Moving;
        }
        else // This series does not have any cameo items.
        {
            EndSeries();
        }
    }

    /// <summary>
    /// Ends the cameo series, and triggers the battle to start.
    /// </summary>
    private void EndSeries()
    {
        Debug.Assert(SeriesParent != null);
        Debug.Assert(SkipCameosButton != null);
        Debug.Assert(appManager != null);
        Debug.Assert(appManager.BattleManager != null);

        SeriesParent.SetActive(false);
        SkipCameosButton.SetActive(false);

        appManager.BattleManager.StartBattle();
    }

    /// <summary>
    /// Instantiates the objects for this battle's cameo series.
    /// </summary>
    private void InstantiateSeries()
    {
        Debug.Assert(Series.Length > 0);
        Debug.Assert(SeriesParent != null);

        float yPosition = Screen.height * -0.2f - CameoItemSpacing;

        seriesObjects = new UI_CameoAttributesManager[Series.Length];

        GameObject newSeriesItem;
        GameObject prefabToInstantiate;

        CameoItem currentCameoItem;
        UI_CameoAttributesManager currentCameoAttributesManager;

        for (int index = 0; index < Series.Length; index++) // Iterate through each cameo item.
        {
            currentCameoItem = Series[index];
            Debug.Assert(currentCameoItem != null);

            prefabToInstantiate = GetCharacterPrefab(currentCameoItem.Character); // Get the prefab for the current cameo item.
            Debug.Assert(prefabToInstantiate != null); // Assert that the current cameo item has a prefab assigned.

            newSeriesItem = Instantiate(prefabToInstantiate);
            newSeriesItem.transform.SetParent(SeriesParent.transform);

            currentCameoAttributesManager = newSeriesItem.GetComponent<UI_CameoAttributesManager>();
            Debug.Assert(currentCameoAttributesManager != null); // Assert that the component was found.

            currentCameoAttributesManager.Initialise();
            currentCameoAttributesManager.SetText(currentCameoItem.Character.ToString(), currentCameoItem.SpeechText);

            seriesObjects[index] = currentCameoAttributesManager;

            seriesObjects[index].SetPosition(yPosition);

            yPosition -= CameoItemSpacing;
        }
    }

    private void Update()
    {
        if(SeriesParent.activeSelf && !PauseMenu.isPaused) // The cameo series is active, and the pause menu is not active.
        {
            ApplyUserInput();

            AnimateCameos();
        }
    }

    /// <summary>
    /// Checks for & applies valid user input.
    /// </summary>
    private void ApplyUserInput()
    {
        if (Input.GetMouseButtonUp(0)) // Left Click
        {
            MoveSeriesTarget(true);
        }
        else if (Input.GetMouseButtonDown(1)) // Right Click
        {
            MoveSeriesTarget(false);
        }
        else if(Input.GetKeyDown(KeyCode.LeftArrow)) // Left Arrow
        {
            MoveSeriesTarget(false);
        }
        else if(Input.GetKeyDown(KeyCode.RightArrow)) // Right Arrow
        {
            MoveSeriesTarget(true);
        }
        else if(Input.GetKeyDown(KeyCode.UpArrow)) // Up Arrow
        {
            MoveSeriesTarget(false);
        }
        else if(Input.GetKeyDown(KeyCode.DownArrow)) // Down Arrow
        {
            MoveSeriesTarget(true);
        }
        else if(Input.GetKeyDown(KeyCode.Space)) // Space
        {
            MoveSeriesTarget(true);
        }
    }

    /// <summary>
    /// Animates the cameos to the current cameo target.
    /// </summary>
    private void AnimateCameos()
    {
        Debug.Assert(SeriesParentRectTrans != null);

        if (seriesState == SeriesStates.Moving)
        {
            float currentPositionY = SeriesParentRectTrans.anchoredPosition.y; // Cache the Y position of the series parent.
            float targetPositionY = CameoItemSpacing * seriesTargetIndex + CameoItemSpacing; // Cache the calculated Y position of the target cameo item.

            if (Mathf.Approximately(currentPositionY, targetPositionY)) // The series parent has reached the target position.
            {
                seriesState = SeriesStates.Holding;
            }
            else // The series parent has not reached the target position.
            {
                float movementThisTick = CameoSpeed * Time.deltaTime;

                if (Mathf.Abs(currentPositionY - targetPositionY) < movementThisTick) // If the movement were applied, the current position would pass over the target.
                {
                    seriesParentPositionSetter.y = targetPositionY; // Set the position to the target position.
                }
                else // The current position will not pass over the target.
                {
                    if (currentPositionY < targetPositionY) // The position is lower than the target.
                    {
                        seriesParentPositionSetter.y = currentPositionY + movementThisTick;
                    }
                    else // The position is higher than the target.
                    {
                        seriesParentPositionSetter.y = currentPositionY - movementThisTick;
                    }
                }

                SeriesParentRectTrans.anchoredPosition = seriesParentPositionSetter;

                UpdateCameosAlphaValues(); // Update the alpha values of each cameo item.
            }
        }
    }

    /// <summary>
    /// Updates the alpha values of each cameo item, depending on their position on the screen.
    /// </summary>
    private void UpdateCameosAlphaValues()
    {
        Debug.Assert(appManager != null);
        Debug.Assert(appManager.CanvasConversionManager != null);

        float alphaValueToSet;
        float currentCameoPositionY;
        UI_CameoAttributesManager currentCameoAttributesManager;

        Vector2 canvasReferenceResolution = appManager.CanvasConversionManager.GetCanvasReferenceResolution();

        for (int index = 0; index < Series.Length; index++) // Iterate through each cameo item.
        {
            currentCameoAttributesManager = seriesObjects[index];

            currentCameoPositionY = currentCameoAttributesManager.GetPositionY() + SeriesParentRectTrans.anchoredPosition.y;

            if (currentCameoPositionY > 0f) // The cameo is in the top half of the screen.
            {
                alphaValueToSet = 1f - currentCameoPositionY / (canvasReferenceResolution.y / 4f);
            }
            else // The cameo is in the bottom half of the screen.
            {
                alphaValueToSet = 1f - (Mathf.Abs(currentCameoPositionY) - canvasReferenceResolution.y / 4f) / (canvasReferenceResolution.y / 5f);
            }

            alphaValueToSet = Mathf.Clamp(alphaValueToSet, 0f, 1f);
            currentCameoAttributesManager.SetAlphaValues(alphaValueToSet); // Apply the new alpha value to the current cameo.
        }
    }

    /// <summary>
    /// Moves the cameo series target in the given direction.
    /// </summary>
    /// <param name="moveForward">Whether the target index should increment.</param>
    private void MoveSeriesTarget(bool moveForward)
    {
        bool backwardsOnFirstCameo = seriesTargetIndex == 0 && !moveForward; // Caches whether the user is trying to move backwards from the first cameo.

        if (!backwardsOnFirstCameo) // The user is not trying to move backwards from the first cameo.
        {
            seriesTargetIndex += moveForward ? 1 : -1; // Advances or retreats the target index.

            if (seriesTargetIndex >= Series.Length) // The cameo series has been finished.
            {
                EndSeries();
            }
            else // The cameo series has not been finished.
            {
                seriesState = SeriesStates.Moving;
            }
        }
    }

    /// <summary>
    /// Returns the GameObject prefab for the given character.
    /// </summary>
    /// <param name="characterToGet">The character whose cameo item prefab should be returned.</param>
    private GameObject GetCharacterPrefab(Characters characterToGet)
    {
        switch (characterToGet)
        {
            case Characters.Flavo:

                return FlavoCameoPrefab;

            case Characters.Snailder:

                return SnailderCameoPrefab;

            case Characters.John:

                return JohnCameoPrefab;

            case Characters.Bebek:

                return DuckDragonCameoPrefab;

            default:

                Debug.LogError(characterToGet);
                return null;
        }
    }

    /// <summary>
    /// Sets the current battle attempted to the given boolean.
    /// </summary>
    /// <param name="newIsAttempted">Whether the current battle has been attempted.</param>
    public void SetCurrentBattleAttempted(bool newIsAttempted)
    {
        currentBattleAttempted = newIsAttempted;
    }

    /// <summary>
    /// Triggered when the user clicks the Skip Cameos UI button.
    /// </summary>
    public void ButtonClickedSkipCameos()
    {
        EndSeries();
    }
}