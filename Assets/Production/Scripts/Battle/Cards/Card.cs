﻿// AUTHOR: Benjamin Chaves

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Battle
{
    [System.Serializable]
    public class Card
    {
        [Header("Basic Attributes")]

        [Tooltip("The name of the card.")]
        public string CardName;
        public int CardID { get; private set; }

        [Tooltip("The animation which should play on the character playing the card.")]
        public RigAnimator.CharacterAnimations CharacterAnimation;

        [Tooltip("The mouse action which this card will trigger.")]
        public MouseActionsManager.Symbols MouseSymbol;

        public CardsDatabase.CardTypes CardType;

        [Header("Gameplay Attributes")]

        [Tooltip("Defines whether this card is a Skill Card or Combat Card.")]
        public bool IsSkillCard;

        [Tooltip("The number of turns for which this card must cooldown after playing it. Includes the turn in which the card is played.")]
        public int CooldownTurns;

        [Tooltip("The additional damage applied to the character's damage roll.")]
        public uint OffensiveAdder;

        [Tooltip("The multiplier applied to the character's damage roll.")]
        public uint OffensiveMultiplier;

        [Tooltip("The value by which the casting character is healed.")]
        public uint HealingAdder;

        [Tooltip("Defines whether this card will heal the character playing the card for the same value of damage it deals.")]
        public bool HealsDamageCaused;

        [Tooltip("Defines whether this card will Duckify the target.")]
        public bool DuckifiesTarget;

        public void SetCardID(int inputCardID)
        {
            Debug.Assert(inputCardID >= 0);

            CardID = inputCardID;
        }

        /// <summary>
        /// Assert that all Inspector fields have been set or filled.
        /// </summary>
        public void AssertInspectorInputs()
        {
            Debug.Assert(CardName != "");
        }
    }
}