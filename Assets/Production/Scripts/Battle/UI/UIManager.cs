﻿// AUTHOR: Benjamin Chaves

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Battle
{
    public class UIManager : MonoBehaviour
    {
        public GameObject BattleUIParent;

        [Tooltip("The GameObject which has the Hand Cards Manager as a component.")]
        public GameObject GO_UI_HandCardsManager;

        [Tooltip("The GameObject which has the Bottom Rim Manager as a component.")]
        public GameObject GO_UI_BottomRimManager;

        [Tooltip("The GameObject which has the Turn Counter Manager as a component.")]
        public GameObject GO_UI_TurnCounterManager;

        private UI_HandCardsManager UI_HandCardsManager;
        private UI_BottomRimManager UI_BottomRimManager;
        private UI_TurnCounterManager UI_TurnCounterManager;

        public void Initialise(AppManager inputAppManager)
        {
            Debug.Assert(inputAppManager != null);

            AssertInspectorInputs();

            CacheSubordinates();

            InitialiseSubordinates(inputAppManager);
        }

        /// <summary>
        /// Assert that all Inspector fields have been set or filled.
        /// </summary>
        private void AssertInspectorInputs()
        {
            Debug.Assert(BattleUIParent != null);

            Debug.Assert(GO_UI_HandCardsManager != null);
            Debug.Assert(GO_UI_BottomRimManager != null);
            Debug.Assert(GO_UI_TurnCounterManager != null);
        }

        private void CacheSubordinates()
        {
            Debug.Assert(GO_UI_HandCardsManager != null);
            Debug.Assert(GO_UI_BottomRimManager != null);
            Debug.Assert(GO_UI_TurnCounterManager != null);

            UI_HandCardsManager = GO_UI_HandCardsManager.GetComponent<UI_HandCardsManager>();
            Debug.Assert(UI_HandCardsManager != null); // Assert that the component was found.

            UI_BottomRimManager = GO_UI_BottomRimManager.GetComponent<UI_BottomRimManager>();
            Debug.Assert(UI_BottomRimManager != null); // Assert that the component was found.

            UI_TurnCounterManager = GO_UI_TurnCounterManager.GetComponent<UI_TurnCounterManager>();
            Debug.Assert(UI_TurnCounterManager != null); // Assert that the component was found.
        }

        private void InitialiseSubordinates(AppManager inputAppManager)
        {
            Debug.Assert(inputAppManager != null);

            Debug.Assert(UI_BottomRimManager != null);
            Debug.Assert(UI_HandCardsManager != null);
            Debug.Assert(UI_TurnCounterManager != null);

            UI_BottomRimManager.Initialise();
            UI_TurnCounterManager.Initialise();
            UI_HandCardsManager.Initialise(inputAppManager.CardsManager, inputAppManager.BattleManager, inputAppManager.TutorialsManager, inputAppManager.CharactersManager, inputAppManager.CanvasConversionManager);
        }

        /// <summary>
        /// Sets the turn counter text to represent the current turn number.
        /// </summary>
        public void SetTurnCounterTurn(int newTurnCount)
        {
            Debug.Assert(newTurnCount >= 0);

            Debug.Assert(UI_TurnCounterManager != null);

            UI_TurnCounterManager.SetTurnCounterText(newTurnCount);
        }

        /// <summary>
        /// Triggers the visual information on each hand-card to be updated.
        /// </summary>
        public void UpdateHandCardsInformation()
        {
            Debug.Assert(UI_HandCardsManager != null);

            Debug.Assert(UI_HandCardsManager != null);

            UI_HandCardsManager.UpdateHandCardsInformation();
        }

        /// <summary>
        /// Returns whether the user's mouse is over at least 1 hand-card.
        /// </summary>
        public bool GetPointerOverAnyHandCard()
        {
            Debug.Assert(UI_HandCardsManager != null);

            return UI_HandCardsManager.PointerOverAnyCard();
        }

        /// <summary>
        /// Returns the anchoredPosition of the given hand card.
        /// </summary>
        /// <param name="handCardIndex">The card whose position should be returned.</param>
        public Vector2 GetHandCardPosition(int handCardIndex)
        {
            Debug.Assert(handCardIndex >= 0);

            Debug.Assert(UI_HandCardsManager != null);

            return UI_HandCardsManager.GetHandCardPosition(handCardIndex);
        }

        /// <summary>
        /// Sets whether the hand-cards UI is active to the given boolean.
        /// </summary>
        public void SetHandCardsActive(bool newIsActive)
        {
            Debug.Assert(UI_HandCardsManager != null);

            UI_HandCardsManager.SetHandCardsActive(newIsActive);
        }

        /// <summary>
        /// Sets whether the bottom rim, hand-cards, and turn-counter
        /// are active to the given boolean.
        /// </summary>
        public void SetBattleUIActive(bool newIsActive)
        {
            Debug.Assert(BattleUIParent != null);
            Debug.Assert(UI_BottomRimManager != null);
            Debug.Assert(UI_HandCardsManager != null);
            Debug.Assert(UI_TurnCounterManager != null);

            BattleUIParent.SetActive(newIsActive);

            if(newIsActive)
            {
                UI_BottomRimManager.SetBottomRimActive(true);
                UI_HandCardsManager.SetHandCardsActive(true);
                UI_TurnCounterManager.SetTurnCounterActive(true);
            }
        }
    }
}