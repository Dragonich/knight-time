﻿// Author: Luke Saliba
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace SceneJump
{
    public class SceneToss : MonoBehaviour
    {
        
        public string SceneDestination;
        // Start is called before the first frame update
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }
        public void SceneJump()
        {
            
            
            SceneManager.LoadScene(SceneDestination);
        }

    }
}
