﻿// AUTHOR: Benjamin Chaves

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Battle
{
    public class CardSet
    {
        public Card Card { get; private set; }
        public int Quantity { get; private set; }
        public int CooldownsRemaining { get; private set; }

        public CardSet(Card inputCard)
        {
            Debug.Assert(inputCard != null);

            Card = inputCard;
        }

        /// <summary>
        /// Sets the cards quantity of this set.
        /// </summary>
        /// <param name="newQuantity">The new cards quantity of this set.</param>
        public void SetQuantity(int newQuantity)
        {
            Debug.Assert(newQuantity >= 0);
            Debug.Assert(!Card.IsSkillCard);

            Quantity = newQuantity;
        }

        /// <summary>
        /// Returns whether this card set is available to be played to the table.
        /// </summary>
        public bool GetIsSetAvailable()
        {
            if (Card.IsSkillCard) // This set's card is a Skill Card.
            {
                return CooldownsRemaining == 0; // Return true if the Skill Card is not in cooldown.
            }
            else // This set's card is not a Skill Card.
            {
                return Quantity > 0; // Return true if the set has at least 1 card available to be played.
            }
        }

        /// <summary>
        /// Registers a card as being used from this set.
        /// </summary>
        public void DebitCardFromSet()
        {
            if(Card.IsSkillCard) // This set's card is a Skill Card.
            {
                Debug.Assert(CooldownsRemaining == 0); // Assert that the card was available for use.
                CooldownsRemaining = Card.CooldownTurns; // Set the card set's cooldown turns to the number of turns before it can be used again.
            }
            else // This set's card is not a Skill Card.
            {
                Quantity--; // Decrement the quantity of cards in this card set.
                Debug.Assert(Quantity >= 0); // Assert that the card set's quantity is not negative.
            }
        }

        /// <summary>
        /// Advances the cooldown of this card.
        /// </summary>
        public void AdvanceTurnCooldowns()
        {
            Debug.Assert(Card.IsSkillCard);

            if(CooldownsRemaining > 0)
            {
                CooldownsRemaining--;
            }
        }

        public override string ToString()
        {
            return "Card: " + Card.CardName + ", Quantity: " + Quantity + ", Cooldowns Remaining: " + CooldownsRemaining;
        }
    }
}