﻿// AUTHOR: Benjamin Chaves

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Battle
{
    public class TurnManager : MonoBehaviour
    {
        private UIManager uiManager;
        private CardsManager cardsManager;
        private BattleManager battleManager;
        private TutorialsManager tutorialsManager;
        private CharactersManager charactersManager;
        private SoundEffectsManager soundEffectsManager;

        /// <summary>
        /// Represents whether the current component of the current turn is the player's.
        /// </summary>
        private bool isPlayerComponent = true;

        /// <summary>
        /// Array of values applied to each corresponding character's health.
        /// </summary>
        private float[] characterHealthModifiers;

        /// <summary>
        /// Represents the current turn index, where the first turn is 1.
        /// </summary>
        public int TurnID { get; private set; } = 0;

        /// <summary>
        /// Represents whether the manager is accepting player selection of a hand-card.
        /// </summary>
        public bool AcceptingPlayerCardSelection { get; private set; }

        public void Initialise(UIManager inputUIManager, CardsManager inputCardsManager, BattleManager inputBattleManager, TutorialsManager inputTutorialsManager, CharactersManager inputCharactersManager, SoundEffectsManager inputSoundEffectsManager)
        {
            Debug.Assert(inputUIManager != null);
            Debug.Assert(inputCardsManager != null);
            Debug.Assert(inputBattleManager != null);
            Debug.Assert(inputTutorialsManager != null);
            Debug.Assert(inputCharactersManager != null);
            Debug.Assert(inputSoundEffectsManager != null);

            uiManager = inputUIManager;
            cardsManager = inputCardsManager;
            battleManager = inputBattleManager;
            tutorialsManager = inputTutorialsManager;
            charactersManager = inputCharactersManager;
            soundEffectsManager = inputSoundEffectsManager;

            int battleCharactersQuantiy = battleManager.GetBattleCharactersQuantity();
            Debug.Assert(battleCharactersQuantiy >= 2, battleCharactersQuantiy);

            characterHealthModifiers = new float[battleCharactersQuantiy];
        }

        /// <summary>
        /// Waits for a few seconds before resolving
        /// the card the enemy played to the table.
        /// </summary>
        private IEnumerator WaitToFinaliseEnemyComponent()
        {
            Debug.Assert(battleManager != null);

            yield return new WaitForSeconds(1.5f);

            ResolveTabledCards();

            ApplyHealthModifiers(); // Appliy each character health modifier to its respective character.

            cardsManager.ClearTabledCards(); // Clear any cards currently played to the table.

            if (PlayerIsAlive()) // The player has at least 1 HP.
            {
                EndTurn();
            }
            else
            {
                battleManager.FailBattle(BattleManager.FailConditions.PlayerKilled); // Trigger the battle to be failed with a result of the player being killed.
            }
        }

        /// <summary>
        /// Starts a new turn.
        /// </summary>
        public void StartTurn()
        {
            Debug.Assert(uiManager != null);
            Debug.Assert(tutorialsManager != null);
            Debug.Assert(charactersManager != null);

            TurnID++;
            uiManager.SetTurnCounterTurn(TurnID); // Updates the turn number on the UI element.

            charactersManager.AdvanceSkillCardCooldowns();
            charactersManager.AdvanceTurnsDuckified();

            if (ValidateCharactersHaveCards()) // The player or the enemy has cards available to play.
            {
                if (charactersManager.GetPlayerTurnsDuckified() <= 0) // The player is not currently Duckified.
                {
                    isPlayerComponent = true;
                    AcceptingPlayerCardSelection = true;

                    uiManager.SetHandCardsActive(true); // Set the player's cards UI to be active.

                    tutorialsManager.NotifyPlayerTurnStarted();
                }
                else // The player is currently Duckified.
                {
                    if(battleManager.WriteTurnDetailsToLog)
                    {
                        Debug.Log("TURN " + TurnID + ": Player is a duck and misses this turn.");
                    }

                    isPlayerComponent = false;
                    AcceptingPlayerCardSelection = false;

                    charactersManager.TableAICards();
                    StartCoroutine(WaitToFinaliseEnemyComponent());
                }
            }
        }

        /// <summary>
        /// Ends the current turn, and starts a new turn.
        /// </summary>
        private void EndTurn()
        {
            StartTurn();
        }

        /// <summary>
        /// Returns whether the player character has at least 1 HP.
        /// </summary>
        private bool PlayerIsAlive()
        {
            Debug.Assert(charactersManager != null);

            return charactersManager.GetCharacterHealth(0) >= 1f;
        }

        /// <summary>
        /// Returns whether the enemy character has at least 1 HP.
        /// </summary>
        private bool EnemyIsAlive()
        {
            Debug.Assert(charactersManager != null);

            return charactersManager.GetCharacterHealth(1) >= 1f;
        }

        /// <summary>
        /// Returns true if the player has cards available to play,
        /// and the enemy has cards available to play.
        /// </summary>
        private bool ValidateCharactersHaveCards()
        {
            Debug.Assert(battleManager != null);
            Debug.Assert(charactersManager != null);

            bool playerHasCards = charactersManager.GetPlayerHasAnyCardsAvailable();
            bool enemyHasCards = charactersManager.GetEnemiesHaveAnyCardsAvailable();

            if(!playerHasCards && !enemyHasCards) // The player has no cards available, and the enemy has no cards available.
            {
                battleManager.FailBattle(BattleManager.FailConditions.AllCardsUnavailableDraw); // Trigger the battle to be failed with a result of a draw.
            }
            else if (!enemyHasCards) // The enemy has no cards available.
            {
                battleManager.WinBattle(BattleManager.WinConditions.EnemiesCardsUnavailable); // Trigger the battle to be won with a result of enemies having no cards available.
            }
            else if(!playerHasCards) // The player has no cards available.
            {
                battleManager.FailBattle(BattleManager.FailConditions.PlayerCardsUnavailable); // Trigger the battle to be failed with a result of the player having no cards available.
            }

            return playerHasCards && enemyHasCards; // Return true if the player has cards available, and the enemy has cards available.
        }

        /// <summary>
        /// Resolves any cards currently played to the table.
        /// </summary>
        public void ResolveTabledCards()
        {
            Debug.Assert(cardsManager != null);

            ZeroHealthModifiers();

            if(isPlayerComponent) // The current component of the current turn is the player's component.
            {
                cardsManager.ActivatePlayerCardMouseSymbol();
            }
            else // The current component of the current turn is the enemy's component.
            {
                cardsManager.ResolveEnemyCards(ref characterHealthModifiers);
            }
        }

        /// <summary>
        /// Resolves the player component of the current turn,
        /// with the given action result boolean.
        /// </summary>
        /// <param name="actionSuccessful">Whether the player's action was successful.</param>
        public void ResolvePlayerComponent(bool actionSuccessful)
        {
            Debug.Assert(uiManager != null);
            Debug.Assert(cardsManager != null);
            Debug.Assert(battleManager != null);
            Debug.Assert(charactersManager != null);

            Card playerTabledCard = cardsManager.GetPlayerTabledCard();
            charactersManager.DebitPlayerCardFromSet(playerTabledCard.CardID);

            uiManager.UpdateHandCardsInformation();

            if (actionSuccessful) // The player's action was successful.
            {
                cardsManager.ResolvePlayerCard(ref characterHealthModifiers);

                ApplyHealthModifiers();
            }
            else if(battleManager.WriteTurnDetailsToLog) // The player's action was successful, and the details should be written to the debug log.
            {
                Debug.Log("TURN " + TurnID + ": Player failed mouse action.");
            }

            cardsManager.ClearTabledCards();

            if(EnemyIsAlive()) // The enemy is still alive after the player's action.
            {
                isPlayerComponent = false;
                uiManager.SetHandCardsActive(false);

                charactersManager.TableAICards();
                StartCoroutine(WaitToFinaliseEnemyComponent());
            }
            else // The enemy is no longer alive after the player's action.
            {
                battleManager.WinBattle(BattleManager.WinConditions.EnemiesKilled);
            }
        }

        /// <summary>
        /// Zeroes each item in the character health modifiers array.
        /// </summary>
        private void ZeroHealthModifiers()
        {
            Debug.Assert(characterHealthModifiers.Length >= 1, characterHealthModifiers.Length);

            for(int index = 0; index < characterHealthModifiers.Length; index++) // Iterate through each character's health modifier.
            {
                characterHealthModifiers[index] = 0f;
            }
        }

        /// <summary>
        /// Applies each character health modifier to its respective character.
        /// </summary>
        private void ApplyHealthModifiers()
        {
            Debug.Assert(battleManager != null);
            Debug.Assert(charactersManager != null);
            Debug.Assert(soundEffectsManager != null);
            Debug.Assert(soundEffectsManager.CharacterSoundEffects != null);
            Debug.Assert(characterHealthModifiers.Length >= 1, characterHealthModifiers.Length);

            for (int index = 0; index < characterHealthModifiers.Length; index++) // Iterate through each character in the battle.
            {
                charactersManager.ModifyCharacterHealth(index, characterHealthModifiers[index]);

                if(characterHealthModifiers[index] < 0f) // The current character is taking damage.
                {
                    if(index == 0) // The current character is the player character.
                    {
                        soundEffectsManager.CharacterSoundEffects.PlayCharacterHurtClip(CharactersDatabase.Characters.Flavo);
                    }
                    else // The current character is not the player character.
                    {
                        CharactersDatabase.Characters enemyCharacter = battleManager.GetEnemyCharacter(index - 1);
                        Debug.Assert(enemyCharacter != CharactersDatabase.Characters.Flavo);

                        soundEffectsManager.CharacterSoundEffects.PlayCharacterHurtClip(enemyCharacter);
                    }
                }
            }

            if(battleManager.WriteTurnDetailsToLog)
            {
                if (isPlayerComponent) // It is currently the player's component of the current turn.
                {
                    Debug.Log("TURN " + TurnID + ": Player HP is " + charactersManager.GetCharacterHealth(0) + ".");
                }
                else // It is currently the enemy's component of the current turn.
                {
                    Debug.Log("TURN " + TurnID + ": Enemy HP is " + charactersManager.GetCharacterHealth(1) + ".");
                }
            }
        }

        /// <summary>
        /// Sets whether the manager is accepting player selection of a hand-card.
        /// </summary>
        /// <param name="newAccepting">Whether the system is accepting the player's card selection.</param>
        public void SetAcceptingPlayerCardSelection(bool newAccepting)
        {
            AcceptingPlayerCardSelection = newAccepting;
        }
    }
}