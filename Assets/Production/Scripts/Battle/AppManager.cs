﻿// AUTHOR: Benjamin Chaves

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using SaveFile;

namespace Battle
{
    public class AppManager : MonoBehaviour
    {
        public GameObject GO_UIManager;
        public GameObject GO_CardsManager;
        public GameObject GO_BattleManager;
        public GameObject GO_CameosManager;
        public GameObject GO_SaveFileManager;
        public GameObject GO_TutorialsManager;
        public GameObject GO_CharactersManager;
        public GameObject GO_MouseActionsManager;
        public GameObject GO_SoundEffectsManager;
        public GameObject GO_CanvasPositionConvert;
        public GameObject GO_ParticleEffectsManager;
        public GameObject GO_CameraAnimationsManager;

        public UIManager UIManager { get; private set; }
        public CardsManager CardsManager { get; private set; }
        public BattleManager BattleManager { get; private set; }
        public CameosManager CameosManager { get; private set; }
        public SaveFileManager SaveFileManager { get; private set; }
        public TutorialsManager TutorialsManager { get; private set; }
        public CharactersManager CharactersManager { get; private set; }
        public MouseActionsManager MouseActionsManager { get; private set; }
        public SoundEffectsManager SoundEffectsManager { get; private set; }
        public ParticleEffectsManager ParticleEffectsManager { get; private set; }
        public CanvasConversionManager CanvasConversionManager { get; private set; }
        public CameraAnimationsManager CameraAnimationsManager { get; private set; }

        private void Awake()
        {
            AssertInspectorInputs();

            CacheSubordinates();

            InitialiseSubordinates();

            if(CameosManager.SkipCameos)
            {
                BattleManager.StartBattle();
            }
            else
            {
                BattleManager.StartCameo();
            }
        }

        /// <summary>
        /// Assert that all Inspector fields have been set or filled.
        /// </summary>
        private void AssertInspectorInputs()
        {
            Debug.Assert(GO_UIManager);
            Debug.Assert(GO_CardsManager);
            Debug.Assert(GO_BattleManager);
            Debug.Assert(GO_CameosManager);
            Debug.Assert(GO_SaveFileManager);
            Debug.Assert(GO_TutorialsManager);
            Debug.Assert(GO_CharactersManager);
            Debug.Assert(GO_MouseActionsManager);
            Debug.Assert(GO_SoundEffectsManager);
            Debug.Assert(GO_CanvasPositionConvert);
            Debug.Assert(GO_ParticleEffectsManager);
            Debug.Assert(GO_CameraAnimationsManager);
        }

        private void CacheSubordinates()
        {
            UIManager = GO_UIManager.GetComponent<UIManager>();
            Debug.Assert(UIManager != null); // Assert that the component was found.

            CardsManager = GO_CardsManager.GetComponent<CardsManager>();
            Debug.Assert(CardsManager != null); // Assert that the component was found.

            BattleManager = GO_BattleManager.GetComponent<BattleManager>();
            Debug.Assert(BattleManager != null); // Assert that the component was found.

            CameosManager = GO_CameosManager.GetComponent<CameosManager>();
            Debug.Assert(CameosManager != null); // Assert that the component was found.

            SaveFileManager = GO_SaveFileManager.GetComponent<SaveFileManager>();
            Debug.Assert(SaveFileManager != null); // Assert that the component was found.

            TutorialsManager = GO_TutorialsManager.GetComponent<TutorialsManager>();
            Debug.Assert(TutorialsManager != null); // Assert that the component was found.

            CharactersManager = GO_CharactersManager.GetComponent<CharactersManager>();
            Debug.Assert(CharactersManager != null); // Assert that the component was found.

            MouseActionsManager = GO_MouseActionsManager.GetComponent<MouseActionsManager>();
            Debug.Assert(MouseActionsManager != null); // Assert that the component was found.

            SoundEffectsManager = GO_SoundEffectsManager.GetComponent<SoundEffectsManager>();
            Debug.Assert(SoundEffectsManager != null); // Assert that the component was found.

            ParticleEffectsManager = GO_ParticleEffectsManager.GetComponent<ParticleEffectsManager>();
            Debug.Assert(ParticleEffectsManager != null); // Assert that the component was found.

            CanvasConversionManager = GO_CanvasPositionConvert.GetComponent<CanvasConversionManager>();
            Debug.Assert(CanvasConversionManager != null); // Assert that the component was found.

            CameraAnimationsManager = GO_CameraAnimationsManager.GetComponent<CameraAnimationsManager>();
            Debug.Assert(CameraAnimationsManager != null); // Assert that the component was found.
        }

        private void InitialiseSubordinates()
        {
            CanvasConversionManager.Initialise();
            CardsManager.InitialiseStaticSubordinates(this);
            BattleManager.InitialiseStaticSubordinates(this);
            CharactersManager.InitialiseStaticSubordinates(this);

            CardsManager.InitialiseDynamicSubordinates();
            BattleManager.InitialiseDynamicSubordinates();
            CharactersManager.InitialiseDynamicSubordinates();

            UIManager.Initialise(this);
            CameosManager.Initialise(this);
            SoundEffectsManager.Initialise();
            TutorialsManager.Initialise(this);
            MouseActionsManager.Initialise(this);            
            CameraAnimationsManager.Initialise();
            ParticleEffectsManager.Initialise(BattleManager.GetBattleIndex());
        }
    }
}