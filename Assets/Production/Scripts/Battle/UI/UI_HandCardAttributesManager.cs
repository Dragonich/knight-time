﻿// AUTHOR: Benjamin Chaves

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using TMPro;
using UnityEngine.UI;

namespace Battle
{
    public class UI_HandCardAttributesManager : MonoBehaviour
    {
        public Card HandCard { get; private set; }

        private UI_HandCardsManager uiHandCardsManager;

        [Header("Prefab Items")]

        public GameObject CardIcon;
        public GameObject CardUnavailableIcon;

        public GameObject TextCardQuantity;
        public GameObject CooldownTurnsText;

        public Sprite NormalAttackImage;
        public Sprite DoubleAttackImage;
        public Sprite FastAttackImage;
        public Sprite PotionImage;
        public Sprite DrainImage;

        /// <summary>
        /// Represents whether the user's mouse is currently over this card's graphics.
        /// </summary>
        public bool MouseIsOver { get; private set; }

        public void Initialise(Card inputHandCard, UI_HandCardsManager inputUIHandCardsManager)
        {
            Debug.Assert(inputHandCard != null);
            Debug.Assert(inputUIHandCardsManager != null);

            AssertInspectorInputs();

            HandCard = inputHandCard;
            uiHandCardsManager = inputUIHandCardsManager;

            SetCardImageType();

            CooldownTurnsText.SetActive(false);
            TextCardQuantity.SetActive(!HandCard.IsSkillCard);
        }

        /// <summary>
        /// Assert that all Inspector fields have been set or filled.
        /// </summary>
        private void AssertInspectorInputs()
        {
            Debug.Assert(CardIcon != null);
            Debug.Assert(CardUnavailableIcon != null);

            Debug.Assert(TextCardQuantity != null);
            Debug.Assert(CooldownTurnsText != null);

            Debug.Assert(NormalAttackImage != null);
            Debug.Assert(DoubleAttackImage != null);
            Debug.Assert(FastAttackImage != null);
            Debug.Assert(PotionImage != null);
            Debug.Assert(DrainImage != null);
        }

        /// <summary>
        /// Sets the card sprites depending on this card's type.
        /// </summary>
        private void SetCardImageType()
        {
            Debug.Assert(NormalAttackImage != null);
            Debug.Assert(DoubleAttackImage != null);
            Debug.Assert(FastAttackImage != null);
            Debug.Assert(PotionImage != null);
            Debug.Assert(DrainImage != null);

            switch (HandCard.CardType)
            {
                case CardsDatabase.CardTypes.NormalAttack:

                    SetCardSprites(NormalAttackImage);
                    break;

                case CardsDatabase.CardTypes.DoubleAttack:

                    SetCardSprites(DoubleAttackImage);
                    break;

                case CardsDatabase.CardTypes.FastAttack:

                    SetCardSprites(FastAttackImage);
                    break;

                case CardsDatabase.CardTypes.Potion:

                    SetCardSprites(PotionImage);
                    break;

                case CardsDatabase.CardTypes.DrainAttack:

                    SetCardSprites(DrainImage);
                    break;

                default:

                    Debug.LogError(HandCard.CardType);
                    break;
            }
        }

        /// <summary>
        /// Sets the card sprites to the given sprite.
        /// </summary>
        private void SetCardSprites(Sprite spriteToSet)
        {
            Debug.Assert(spriteToSet != null);

            Debug.Assert(CardIcon != null);
            Debug.Assert(CardUnavailableIcon != null);

            Image cardImage = CardIcon.GetComponent<Image>();
            Debug.Assert(cardImage != null); // Assert that the component was found.

            Image cardUnavailableImage = CardUnavailableIcon.GetComponent<Image>();
            Debug.Assert(cardUnavailableImage != null); // Assert that the component was found.

            cardImage.sprite = spriteToSet;
            cardUnavailableImage.sprite = spriteToSet;
        }

        /// <summary>
        /// Sets the graphics depending on the given quantity of cards in this set.
        /// </summary>
        /// <param name="newCardQuantity">The new quantity of cards in this set.</param>
        public void SetCardQuantity(int newCardQuantity)
        {
            Debug.Assert(newCardQuantity >= 0);

            Debug.Assert(CardIcon != null);

            TextMeshProUGUI cardQuantityTextComponent = TextCardQuantity.GetComponent<TextMeshProUGUI>();
            Debug.Assert(cardQuantityTextComponent != null); // Assert that the component was found.

            cardQuantityTextComponent.text = "x" + newCardQuantity.ToString();

            CardIcon.SetActive(newCardQuantity > 0);
            SetCardAvailable(newCardQuantity > 0);
        }

        /// <summary>
        /// Sets the graphics depending on the given number of cooldowns on this set.
        /// </summary>
        /// <param name="newCardCooldowns">The new number of cooldowns on this set.</param>
        public void SetCardCooldowns(int newCardCooldowns)
        {
            Debug.Assert(newCardCooldowns >= 0);

            Debug.Assert(CooldownTurnsText != null);

            CooldownTurnsText.SetActive(newCardCooldowns != 0); // Set the Cooldown Turns text active when this card's cooldown turns is not 0.

            if(CooldownTurnsText.activeSelf) // The Cooldown Turns text is active.
            {
                TextMeshProUGUI cooldownTurnsTextComponent = CooldownTurnsText.GetComponent<TextMeshProUGUI>();
                Debug.Assert(cooldownTurnsTextComponent != null); // Assert that the component was found.

                cooldownTurnsTextComponent.text = newCardCooldowns.ToString(); // Update the cooldown turns on the text element.
            }

            SetCardAvailable(newCardCooldowns == 0); // Set this card available when this card's cooldown turns is 0.
        }

        /// <summary>
        /// Sets the unavailability graphic active
        /// depending on whether the card set is available.
        /// </summary>
        /// <param name="newIsAvailable">Whether the card set is available.</param>
        private void SetCardAvailable(bool newIsAvailable)
        {
            Debug.Assert(CardUnavailableIcon != null);

            CardUnavailableIcon.SetActive(!newIsAvailable); // Set the Unavailable Icon to be inactive when the card is available.
        }

        /// <summary>
        /// Sets the manager's tracker of whether the user's mouse is currently over this card's graphics.
        /// </summary>
        /// <param name="newMouseIsOver">The new status of whether the user's mouse is over this card's graphics.</param>
        public void SetMouseIsOver(bool newMouseIsOver)
        {
            MouseIsOver = newMouseIsOver;
        }

        /// <summary>
        /// Triggers the functionality related to the user clicking on this card's graphics.
        /// This function is called from a UI button.
        /// </summary>
        public void ButtonClickedCard()
        {
            Debug.Assert(uiHandCardsManager != null);

            uiHandCardsManager.ButtonClickedHandCard(this);
        }
    }
}